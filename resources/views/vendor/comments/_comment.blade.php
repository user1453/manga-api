@inject('markdown', 'Parsedown')
@php($markdown->setSafeMode(true))

@if(isset($reply) && $reply === true)
    <div id="comment-{{ $comment->getKey() }}" class="media">
        @else

            <div class="comm m-t-10px" data-comm-id="{{ $comment->getKey() }}" id="comment-{{ $comment->getKey() }}">
                @endif
                <div class="main-info">
                    <div class="img"
                         style="background-image: url({{asset('assets/img/coder-ava.jpg')}})"></div>
                    <div class="title">
                        <p class="font-16px">
                            {{$comment->commenter->login}}
                            @foreach($comment->commenter->roles as $role)
                            <span class="role role-{{$role->id}}">{{$role->name}}</span>
                            @endforeach
                        </p>
                        <span class="font-13px">{{ $comment->created_at->diffForHumans() }}</span>
                    </div>
                </div>
                <div class="text">
                    <span>{!! \App\Models\Comment::stip_tags($comment->comment) !!}</span>
                </div>
                @auth
                    <div class="under-control m-t-10px" data-comm-reply-id="{{ $comment->getKey() }}">
                        <a href="javascript: void(0)" class="font-14px c-fd reply"
                           data-comm-reply-id="{{ $comment->getKey() }}" data-comm-reply-action="{{ route('comments.reply', $comment->getKey()) }}">Ответить</a>
                        <a href="javascript: void(0)" class="like m-l-10px">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#like"></use>
                            </svg>
                            <span>1</span>
                        </a>
                        <a href="javascript: void(0)" class="dislike">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#dislike"></use>
                            </svg>
                            <span>0</span>
                        </a>
                    </div>
                    {{--            <button data-toggle="modal" data-target="#reply-modal-{{ $comment->getKey() }}" class="btn btn-sm btn-link text-uppercase">Reply</button>--}}
                @endauth
                {{--    <img class="mr-3" src="https://www.gravatar.com/avatar/{{ md5($comment->commenter->email ?? $comment->guest_email) }}.jpg?s=64" alt="{{ $comment->commenter->name ?? $comment->guest_name }} Avatar">--}}

                @if(false)
                    @can('edit-comment', $comment)
                        <button data-toggle="modal" data-target="#comment-modal-{{ $comment->getKey() }}"
                                class="btn btn-sm btn-link text-uppercase">Edit
                        </button>
                    @endif
                    @can('delete-comment', $comment)
                        <a href="{{ route('comments.destroy', $comment->getKey()) }}"
                           onclick="event.preventDefault();document.getElementById('comment-delete-form-{{ $comment->getKey() }}').submit();"
                           class="btn btn-sm btn-link text-danger text-uppercase">Delete</a>
                        <form id="comment-delete-form-{{ $comment->getKey() }}"
                              action="{{ route('comments.destroy', $comment->getKey()) }}" method="POST"
                              style="display: none;">
                            @method('DELETE')
                            @csrf
                        </form>
                    @endcan
                @endif
                {{--        <br />--}}{{-- Margin bottom --}}

                {{-- Recursion for children --}}
                @if($grouped_comments->has($comment->getKey()))
                    @foreach($grouped_comments[$comment->getKey()] as $child)
                        @include('comments::_comment', [
                            'comment' => $child,
                            'reply' => true,
                            'grouped_comments' => $grouped_comments
                        ])
                    @endforeach
                @endif


            @if(isset($reply) && $reply === true)
    </div>
    @else
    </div>
@endif
