<?php

namespace App\Http\Controllers;

use App\Events\Message;
use App\Http\Resources\StatusResponse;
use App\Models\ChatMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function message(Request $request)
    {
        $name = Auth::check() ? Auth::user()->login : 'Guest';
        $message = ChatMessage::create(['message' => $request->get('message'), 'name' => $name, 'image' => '/assets/img/demo-ava.jpg']);
        Message::dispatch($message);
        return response(new StatusResponse(1), 200);
    }

    public function clear($name)
    {
        ChatMessage::where('name', $name)->delete();
        return 'ok';
    }
}
