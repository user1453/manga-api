<?php

namespace App\Http\Controllers;

use App\Http\Resources\ErrorResponse;
use App\Http\Resources\StatusResponse;
use App\Http\Resources\TeamMangaCollection;
use App\Http\Resources\TopTeamCollection;
use App\Models\Delegate;
use App\Models\Part;
use App\Models\Team;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    public function create(Request $request)
    {
        $input = [
            'name' => strip_tags($request->get('name')),
            'date_start' => $request->get('dateStart'),
            'head_link' => $request->get('headLink'),
            'discord' => $request->get('discordLink'),
            'vk' => $request->get('vkLink'),
            'description' => strip_tags($request->get('description')),
            'certified' => true, // TODO: удалить
        ];

        $delegates = $request->get('deligates');

        $validator = Validator::make($input, [
            'name' => 'required|string|max:255|unique:teams',
            'date_start' => 'required|string',
            'head_link' => 'required|string',
            'discord' => 'nullable|string',
            'vk' => 'nullable|string',
            'description' => 'string',
        ]);

        if ($validator->fails()) {
            return response(new ErrorResponse($validator->errors()), 200);
        }

//        $input['date_start'] = Carbon::createFromFormat('d.m.Y', $input['date_start'])->format('d-m-Y');
        $team = new Team;
        $team->fill($input);
        $team->certified = false;

        $leader = User::where('login', $input['head_link'])->first();
        if ($leader == null) {
            return response(new StatusResponse(0), 200);
        }

        $team->save();

        Delegate::create(['user_id' => $leader->id, 'position' => 'Глава', 'team_id' => $team->id]);

        if ($delegates != null && is_array($delegates)) {
            try {
                foreach ($delegates as $delegate) {
                    $delegate = (object)$delegate;
                    $user = User::where('login', $delegate->log)->first();
                    if ($user == null)
                        continue;
                    Delegate::create(['user_id' => $user->id, 'position' => $delegate->deligate, 'team_id' => $team->id]);
                }
            } catch (\Exception $ex) {
                return response(new StatusResponse($ex->getMessage()), 200);
            }
        }
        return response(new StatusResponse(1), 200);
    }

    public function checkName(Request $request)
    {
        $name = $request->get('name');
        if (Team::where('name', $name)->first() != null)
            return response(new StatusResponse($name), 200);
        else
            return response(new StatusResponse('empty'), 200);
    }

    public function top()
    {
        return response(new TopTeamCollection(Team::certified()->take(4)->get()), 200); // TODO: Дописать топ
    }

    public function rate(Request $request)
    {
        $team = Team::where('id', $request->get('id'))->first();
        $rate = $request->get('rate');
        if ($rate > 5)
            $rate = 5;
        if ($rate < 1)
            $rate = 1;

        $team->rateOnce($rate);
        return response(new StatusResponse(1), 200);
    }

    public function getIndex(Request $request, $id)
    {
        $team = Team::with('delegates')->find($id);
        if ($team == null)
            abort(404);
        return view('team.index', ['team' => $team, 'user']);
    }

    public function getCreate(Request $request)
    {
        return view('team.create');
    }

    public function teamRedirect()
    {

        $delegate = Delegate::where('user_id', Auth::user()->id)->first();
        if ($delegate != null)
            return redirect(route('team.page', ['id' => $delegate->team_id]), 302);
        return redirect(route('team.create'), 302);
    }

    public function manga($id)
    {
        $userIds = Delegate::where('team_id', $id)->pluck('id');

        $parts = Part::whereHas('delegate', function (Builder $query) use ($id) {
            $query->where('team_id', $id);
        })->distinct('manga_id')->with(['manga', 'user'])->get();

        return response(new TeamMangaCollection($parts), 200);
    }
}
