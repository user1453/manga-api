@extends('layouts.page')

@section('title', 'teikei.me')

@section('main')

    <main class="_spa-view">
        <section class="home">
            <span class="home__progress-bar"></span>
            <div class="home__slider" id="homeSlider">
                <div class="home__slider__nav" id="homeSliderNav"></div>
            </div>
        </section>
        <div class="home-content">
            <div class="content-area">
                <div class="left-column">
                    <section class="update-popular-manga margin-10px shadow-dark">
                        <h2 class="font-19px m-b-10px uppercase">Обновление популярной манги</h2>
                        <div class="manga-update-container"></div>
                    </section>
                    <section class="new-manga-list-box margin-10px shadow-dark">
                        <h2 class="font-19px m-b-10px uppercase">Новая манга</h2>
                        <div class="new-manga-list"></div>
                    </section>
                    <section class="new-part-manga margin-10px shadow-dark">
                        <div class="parts m-b-10px">
                            <h2 class="font-19px uppercase">Новые главы</h2>
                            <div class="filters">
                                <span class="font-14px active" data-sort="all">Все</span>
                                <span class="font-14px" data-sort="my">Мои</span>
                            </div>
                        </div>
                        <div class="manga-new-container"></div>
                    </section>
                </div>
                <div class="right-column">
                    <div class="chat margin-10px shadow-dark" id="homeChat">
                        <table class="message">
                            <tbody>
                            @foreach($messages as $message)
                                <tr>
                                    <td class="ava" style="background-image: url({{$message->image}})"></td>
                                    <td class="text">
                                        <p class="bold font-15px">{{$message->name}}</p>
                                        <span class="font-14px">{{$message->message}}</span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @auth
                            <div class="input-box">
                                <span class="error font-14px">Не удалось отправить</span>
                                <input type="text" placeholder="Введите сообщение">
                                <!-- <div class="btn">
                                    <svg>
                                        <use xlink:href="/assets/img/sprite.svg#sendMessage"></use>
                                    </svg>
                                </div> -->
                            </div>
                        @endauth
                    </div>
                    <div class="list-manga margin-10px shadow-dark">
                        <h2 class="font-14px uppercase">Самое популярное</h2>
                        <div class="top-manga-list"></div>
                    </div>
                    <div class="list-teams margin-10px shadow-dark">
                        <h2 class="font-14px uppercase">Топ команд переводчиков</h2>
                        <div class="top-teams"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@stop

@push('css')

    <link rel="stylesheet" href="/assets/css/main.css">
@endpush

@push('scripts')

    <script src="/assets/js/main.js" charset="utf-8"></script>
@endpush
