<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Genre;
use App\Models\Manga;
use App\Models\Part;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function getIndex(Request $request)
    {
        $query = Manga::certified();
        $year = $request->get('year');
        $cat = explode('_', $request->get('cat'));
        $cat = $this->clearArray($cat);

        $format = explode('_', $request->get('form'));
        $format = $this->clearArray($format);

        $status = explode('_', $request->get('statTranslate'));
        $status = $this->clearArray($status);


        $genres = explode('_', $request->get('genres'));
        $genres = $this->clearArray($genres);

        $hide = explode('_', $request->get('hide'));
        $hide = $this->clearArray($hide);

        $translator = $request->get('translator');

        if ($year != null)
            $query = $query->where('date_release', $year);

        if ($genres != null && is_array($genres))
            $query = $query->whereHas('genres', function ($query) use ($genres) {
                $query->whereIn('genres.id', $genres);
            });

        if ($cat != null && is_array($cat))
            $query = $query->whereHas('categoryManga', function (Builder $que) use ($cat) {
                $que->whereIn('category_manga.category_id', $cat);
            });

        if ($status != null && is_array($status)) {
            $names = [
                'продолжается',
                'завершен',
                'заморожен'
            ];
            $selected = [];
            foreach ($status as $id) {
                $selected[] = $names[$id];
            }
            $query = $query->whereIn('translate_status', $selected);
        }

        if ($hide != null && is_array($hide)) {
            if (Auth::check()) {

                if (in_array(1, $hide)) {
                    $id = Auth::user()->id;
                    $query = $query->whereHas('boockmarks', function ($q) use ($id) {
                        $q->where('user_id', $id);
                    });
                } else {
                    $id = Auth::user()->id;
                    $query = $query->whereDoesntHave('boockmarks', function ($q) use ($id) {
                        $q->where('user_id', $id);
                    });
                }
            }
        }

        if ($format != null && is_array($format))
            $query = $query->whereIn('format', $format);

        if ($translator != null) {
            $user = User::findByLogin($translator);
            if ($user != null) {
                $ids = Part::certified()->with('user')->whereHas('user', function (Builder $query) use ($user) {
                    $query->where('user_id', $user->id);
                })->distinct('manga_id')->pluck('manga_id');

                $query = $query->whereIn('id', $ids);
            }
        }

        $order = $request->get('order', 'desc');
        $sort = $request->get('sort', 'rate');
        switch ($sort) {
            case 'rate':
                $query->select('mangas.*')
                    ->leftJoin('ratings', function (\Illuminate\Database\Query\Builder $query) {
                        $query->on('mangas.id', '=', 'ratings.rateable_id')
                            ->where('ratings.rateable_type', Manga::class);
                    })
                    ->addSelect(DB::raw('AVG(ratings.rating) as average_rating'))
                    ->groupBy('mangas.id')
                    ->orderBy('average_rating', $order);
                break;
            case 'updated_at':
                $query->select('mangas.*')
                    ->leftJoin('parts', function (\Illuminate\Database\Query\Builder $query) {
                        $query->on('mangas.id', '=', 'parts.manga_id')
                            ->where('parts.certified', true);
                    })
                    ->addSelect(DB::raw('MAX(parts.id) as last_id'))
                    ->groupBy('mangas.id')
                    ->orderBy('last_id', $order);
                break;
            case 'created_at':
                $query->orderBy('created_at', $order);
                break;
            case 'views':
                $query->orderBy('views', $order);
                break;
            case 'count':
                $query->withCount('parts')
                    ->orderBy('parts_count', $order);
                break;
            case 'name':
                $query->orderBy('en_name', $order);
                break;
            default:
                $query->orderBy('id', $order);
        }

        $genres = Cache::rememberForever('genres:all', function () {
            return Genre::all();
        });
        $categories = Cache::rememberForever('categories:all', function () {
            return Category::all();
        });

        return view('catalog.index', [
            'mangas' => $query->get(),
            'genres' => $genres,
            'categories' => $categories,
            'input' => $request->except('sort') + ['order' => $order],
            'inputExOrder' => $request->except('order') + ['sort' => $sort],
            'sort' => $sort,
            'order' => $order,
            'year' => $year
        ]);
    }

    public function search(Request $request)
    {
        $query = Manga::certified();
        $val = $request->get('val');
        $type = $request->get('type');
        switch ($type) {
            case 'author':
                $query = $query->where('author', 'ILIKE', "%$val%");
                break;
            case 'manga':
                $query = $query->where('en_name', 'ILIKE', "%$val%")
                    ->orWhere('rus_name', 'ILIKE', "%$val%")
                    ->orWhere('origin_name', 'ILIKE', "%$val%");
                break;
            case 'artist':
                $query = $query->where('painter', 'ILIKE', "%$val%");
                break;
            case 'user':
                $users = User::where('login', 'ILIKE', "%$val%")->get();
                if ($users->count() > 0)
                    $view = view('catalog.search.user', ['users' => User::where('login', 'ILIKE', "%$val%")->get()]);
                else
                    $view = view('catalog.search.wops');

                return $view->toHtml();
                break;
            case 'translater':
                $parts = Part::certified()->whereHas('user', function (Builder $query) use ($val) {
                    $query->where('login', 'ILIKE', "%$val%");
                })->with('manga')->distinct('manga_id')->get();
                if ($parts->count() > 0)
                    $view = view('catalog.search.translator', ['parts' => $parts]);
                else
                    $view = view('catalog.search.wops');

                return $view->toHtml();
                break;
            default:
                abort(404);
                break;
        }
        $mangas = $query->get();
        if ($mangas->count() > 0)
            $view = view('catalog.search.manga', ['items' => $mangas]);
        else
            $view = view('catalog.search.wops');
        return $view->toHtml();

        $user = Auth::user();
        $team = Team::where('user_id', $user->id)->first();
        if ($team == null) {
            $team = new Team();
            $team->user_id = $user->id;
        }
        $team->fill($request->get('team'));
        $team->save();
    }

    private function clearArray($linksArray)
    {
        foreach ($linksArray as $key => $link) {
            if ($link === '') {
                unset($linksArray[$key]);
            }
        }
        return $linksArray;
    }
}
