<p class="font-16px c-fd">Комментарии</p>
<div>
    <button type="button" class="font-13px active">Новые</button>
    <button type="button" class="font-13px">Старые</button>
</div>
<div class="textarea m-t-10px b-r-5px" data-t-textarea-id="main">
    <div class="input font-14px" contenteditable="true" data-textarea-id="main"></div>
    <div class="controls">
        <div>
                            <span data-func="bold">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#bold"></use>
                                </svg>
                            </span>
            <span data-func="italic">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#italic"></use>
                                </svg>
                            </span>
            <span data-func="underline">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#underline"></use>
                                </svg>
                            </span>
            <span data-func="striket">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#striket"></use>
                                </svg>
                            </span>
            <span data-func="spoiler">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#eye"></use>
                                </svg>
                            </span>
        </div>
        <div>
            <button type="button" class="send" data-send-id="main" data-action="{{ route('comments.store') }}"
                    data-commentable-type="\{{ get_class($model) }}" data-commentable-id="{{ $model->getKey() }}">
                Отправить
            </button>
            <meta name="comment-action" content="{{ route('comments.store') }}">
            <meta name="comment-model" content="{{Config::get('comments.model')}}">
        </div>
    </div>
</div>
@if(false)
    <div class="card">
        <div class="card-body">
            @if($errors->has('commentable_type'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('commentable_type') }}
                </div>
            @endif
            @if($errors->has('commentable_id'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('commentable_id') }}
                </div>
            @endif
            <form method="POST" action="{{ route('comments.store') }}">
                @csrf
                @honeypot
                <input type="hidden" name="commentable_type" value="\{{ get_class($model) }}"/>
                <input type="hidden" name="commentable_id" value="{{ $model->getKey() }}"/>

                {{-- Guest commenting --}}
                @if(isset($guest_commenting) and $guest_commenting == true)
                    <div class="form-group">
                        <label for="message">Enter your name here:</label>
                        <input type="text" class="form-control @if($errors->has('guest_name')) is-invalid @endif"
                               name="guest_name"/>
                        @error('guest_name')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="message">Enter your email here:</label>
                        <input type="email" class="form-control @if($errors->has('guest_email')) is-invalid @endif"
                               name="guest_email"/>
                        @error('guest_email')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                @endif

                <div class="form-group">
                    <label for="message">Enter your message here:</label>
                    <textarea class="form-control @if($errors->has('message')) is-invalid @endif" name="message"
                              rows="3"></textarea>
                    <div class="invalid-feedback">
                        Your message is required.
                    </div>
                    <small class="form-text text-muted"><a target="_blank"
                                                           href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a>
                        cheatsheet.</small>
                </div>
                <button type="submit" class="btn btn-sm btn-outline-success text-uppercase">Submit</button>
            </form>
        </div>
    </div>
    <br/>
@endif
