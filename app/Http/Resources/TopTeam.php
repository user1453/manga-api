<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TopTeam extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $team = $this->resource;
        return [
            'id' => $team->id,
            'name' => $team->name,
            'quality' => round($team->averageRating, 1),
            'volume' => 0,
            'speed' => 0,
            'link' => route('team.page', ['id' => $team->id])
        ];
    }
}
