<?php

namespace App\Http\Controllers;

use App\Models\Bookmark;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getIndex(Request $request, $id = null)
    {
        if ($id == null)
            $id = Auth::user()->id;

        $user = User::with('premium')->find($id);
        $list = $request->get('list');
        $data = ['user' => $user, 'bookmarks' => []];
        if ($list != null) {
            $data['bookmarks'] = Bookmark::with('manga')->where('list', $list)->where('user_id', Auth::user()->id)->get();
        }

        return view('profile.index', $data);
    }
}
