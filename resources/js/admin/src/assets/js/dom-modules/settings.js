export function settings (selector, fn) {
    for (let item of document.querySelectorAll(selector)) {
        const api = {
            _funcBox: document.querySelectorAll('.settings__functional-box'),
            events: {

            },
            addEvent: function (prop, val, type = null) {
                if (type === 'array') {
                    console.log(this.events[prop]);
                    console.log(val);
                    if(this.events[prop] === undefined)
                        this.events[prop] = []
                    if (!this.events[prop].includes(val)) {
                        this.events[prop].push(val)
                    }
                } else if (type === 'object') {
                    if (this.events[prop] === undefined) {
                        this.events[prop] = {}
                    }
                    this.events[prop][val.split(':')[0].trim()] = val.split(':')[1].trim()
                } else if (type === null) {
                    this.events[prop] = val
                }
            },
            setTitle: function (text, fn = null) {
                item.querySelector('.settings__title-primary').html(text)
                item.querySelector('.settings__title-secondary').html('')
                if (typeof fn === 'function') {
                    fn()
                }
            },
            addCustomBlock: function (selector, fn = null) {
                let create = document.createElement(selector)

                this._funcBox._append(create)
                if (typeof fn === 'function') {
                    fn(create)
                }

            },
            addInput: function (text, id, fn = null) {
                let label = document.createElement('label')
                let p = document.createElement('p')
                let input = document.createElement('input')

                p.addClass('settings__label')
                p.html(text)

                input.addClass('settings__input')
                input.attr('type text')

                label.attr(`settings-child-id ${id}`)
                label._append(p)
                label._append(input)
                if (typeof fn === 'function') {
                    fn(label, input, p)
                }
                this._funcBox._append(label)
            },
            addToggleBtn: function (text, rest, fn = null, key = null) {
                let id = key;
                let div = document.querySelector('.settings__btn-box')
                let btn = document.createElement('button')
                if (id == null)
                    id = document.querySelectorAll('[id-toggle-btn]').length
                let status = Number(rest)

                btn.attr('type button', `id-toggle-btn ${id}`, `status-toggle-btn ${status}`)
                btn.addClass('btn margin-right-5px fnsBtn')
                if (rest) {
                    btn.addClass('btn_green')
                } else {
                    btn.addClass('btn_red')
                }
                btn.html(text)

                if (div === null) {
                    div = document.createElement('div')
                    div.addClass('margin-top-20px settings__btn-box')
                    this._funcBox._append(div)
                }
                if (div !== null) {
                    btn.addEventListener('click', (e) => {
                        btn.toggleClass('btn_green')
                        btn.toggleClass('btn_red')
                        if (btn.attr(`status-toggle-btn`) === '0') {
                            status = 1
                        } else if (btn.attr(`status-toggle-btn`) === '1') {
                            status = 0
                        }
                        btn.attr(`status-toggle-btn ${status}`)
                    })
                    div._append(btn)
                }

                if (typeof fn === 'function') {
                    fn(btn, div, text)
                }
            },
            addBtn: function (text, parent = null, fn = null) {
                let btn = document.createElement('button')

                btn.attr('type button')
                btn.addClass('btn btn_brand')
                btn.html(text)

                if (typeof fn === 'function') {
                    fn(btn, parent, text)
                }

                if (parent !== null) {
                    if (typeof parent === 'object') {
                        parent._append(btn)
                    } else {
                        storage.console.grpCollapsed(
                            () => {
                                console.error('Допустимый формат родительского элемента NodeList или Element')
                            },
                            '[Ошибка] settings api => addBtn()',
                            `${storage.console.statusCSS.err}`
                         )
                    }
                } else {
                    let div = document.createElement('div')
                    div.addClass('margin-top-20px')
                    div._append(btn)
                    this._funcBox._append(div)
                }
            }
        }

        document.querySelectorAll('.settings__functional-box').html('')
        fn(item, api)
    }
}
