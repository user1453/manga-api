<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Page extends Model
{

    protected $guarded = [];

//    public function GetPrivateUrl(){
//        Storage::disk('temp')->put('new/file1.jpg', Storage::get('old/file1.jpg'));
//    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getThreadedComments()
    {
        return $this->comments()->with('owner')->get()->threaded();
    }

    public function addComment($attributes)
    {
        $comment = (new Comment)->forceFill($attributes);
        $comment->user_id = auth()->id();

        return $this->comments()->save($comment);
    }

    public static function findByNumber($partId, $number)
    {
        return self::where('part_id', $partId)->where('number', $number)->first();
    }
}
