<?php

namespace App\Http\Controllers;

use App\Models\Manga;
use App\Models\Page;
use App\Models\Part;
use App\Models\Premium;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class PageController extends Controller
{
    public function getIndex(Request $request)
    {
        $input = $request->all();


        if ($request->has("typeRead") && $request->get('typeRead')) {

            $manga = Manga::find($input['id']);
            if ($manga == null)
                abort(404);


            $translator = User::findByLogin($input['translator']);
            if ($translator == null)
                abort(404);

            $part = Part::findByNumber($input['id'], $translator->id, $input['part']);

            if ($part == null)
                abort(404);


            $nextPart = Part::certified()
                ->where('manga_id', $manga->id)
                ->with('user')
                ->where('user_id', $translator->id)
                ->where('number', '>', $part->number)
                ->orderBy('number')
                ->first();
            if ($nextPart == null) {
                $nextUrl = route('manga.page', ['id' => $manga->id]);
            } else {
                $nextUrl = $nextPart->url(1, $input['typeRead'] ?? false);
            }

            $prevPart = Part::certified()
                ->where('manga_id', $manga->id)
                ->with('user')
                ->where('user_id', $translator->id)
                ->where('number', '<', $part->number)
                ->orderBy('number', 'desc')
                ->first();
            if ($prevPart == null) {
                $prevUrl = route('manga.page', ['id' => $manga->id]);
            } else {
                $prevPage = Page::where('part_id', $prevPart->id)
                    ->orderby('number', 'desc')->first();
                $prevUrl = $prevPart->url($prevPage->number, $input['typeRead'] ?? false);
            }


            $data = ['translator' => $translator, 'part' => $part, 'pages' => $part->pages, 'manga' => $manga, 'nextUrl' => $nextUrl, 'prevUrl' => $prevUrl];

            return view('reader.index', $data);

        }


        $key = 'reader:page:' . $input['id'] . ':' . $input['translator'] . ':' . $input['part'] . '#' . $input['page'];

        $data = Cache::rememberForever($key, function () use ($input) {

            $translator = User::findByLogin($input['translator']);
            if ($translator == null)
                return null;

            $part = Part::findByNumber($input['id'], $translator->id, $input['part']);

            if ($part == null)
                return null;

            $page = Page::findByNumber($part->id, $input['page']);
            if ($page == null)
                return null;

            $manga = Manga::find($input['id']);
            if ($manga == null)
                return null;

            $nextPage = Page::findByNumber($part->id, $input['page'] + 1);
            if ($nextPage == null) {
                $nextPart = Part::certified()
                    ->where('manga_id', $manga->id)
                    ->with('user')
                    ->where('user_id', $translator->id)
                    ->where('number', '>', $part->number)
                    ->orderBy('number')
                    ->first();
                if ($nextPart == null) {
                    $nextUrl = route('manga.page', ['id' => $manga->id]);
                } else {
                    $nextUrl = $nextPart->url(1, $input['typeRead'] ?? false);
                }

            } else {
                $nextUrl = $part->url($nextPage->number, $input['typeRead'] ?? false);
            }

            if ($page->number > 1) {
                $prevUrl = $part->url($page->number - 1, $input['typeRead'] ?? false);
            } else {
                $prevPart = Part::certified()
                    ->where('manga_id', $manga->id)
                    ->with('user')
                    ->where('user_id', $translator->id)
                    ->where('number', '<', $part->number)
                    ->orderBy('number', 'desc')
                    ->first();
                if ($prevPart == null) {
                    $prevUrl = route('manga.page', ['id' => $manga->id]);
                } else {
                    $prevPage = Page::where('part_id', $prevPart->id)
                        ->orderby('number', 'desc')->first();
                    $prevUrl = $prevPart->url($prevPage->number, $input['typeRead'] ?? false);
                }
            }

            return ['translator' => $translator, 'part' => $part, 'page' => $page, 'manga' => $manga, 'nextUrl' => $nextUrl, 'prevUrl' => $prevUrl];
        });

        if ($data == null)
            abort(404);

        return view('reader.index', $data);
    }

    public function parts(Request $request)
    {

        $parts = Part::certified()
            ->with(['pages', 'user'])
            ->where('manga_id', $request->get('id'))
            ->where('user_id', $request->get('translator'))->get();
        $pages = [];
//        dump($part);

        foreach ($parts as $part) {
            $pages [] = [
                'part' => $part->number,
                'vol' => $part->vol,
                'name' => $part->name,
                'url' => $part->url(),
            ];
        }

        return response()->json($pages);
    }

    public function pages(Request $request)
    {
        return Page::where('part_id', $request->get('id'))->count();
    }

    public function show(Request $requent)
    {
        $part = Part::where('manga_id', $requent->get('id'))
            ->where('number', $requent->get('part'))->first();
        if ($part == null)
            return response(null, 200);
        $page = Page::where('number', $requent->get('page'))
            ->where('part_id', $part->id)
            ->first();
        return response($page);
    }

    public function comment()
    {

    }

    public function image(Request $request, int $partId, int $pageNumber)
    {
        $part = Cache::remember('part#' . $partId, 300, function () use ($partId) {
            return Part::find($partId);
        });

        if ($part->premium) {
            $user = Auth::user();
            if ($user == null)
                abort(403);
            $teamId = Cache::remember('part:team_id#' . $partId, 300, function () use ($part) {
                $part->load('delegate');
                return $part->delegate->team_id;
            });

            $prem = Premium::where('user_id', $user->id)->where('team_id', $teamId)->first();
            if ($prem == null) {
                abort(403);
            }

        }

        $page = Cache::remember('part:page:' . $partId . '#' . $pageNumber, 300, function () use ($partId, $pageNumber) {
            return Page::where('part_id', $partId)->where('number', $pageNumber)->first();
        });

        $cookie = Cookie::get('eReaderZip');
        if (@$cookie == 'original') {

            if (!Storage::disk('originals')->exists(($page->path))) {
                return Response::make('File no found.', 404);
            }

            $file = Storage::disk('originals')->get($page->path);
            $type = Storage::disk('originals')->mimeType($page->path);
            return Response::make($file, 200)->header("Content-Type", $type);

        } else {
            if (!Storage::disk('public')->exists(($page->path))) {
                return Response::make('File no found.', 404);
            }

            $file = Storage::disk('public')->get($page->path);
            $type = Storage::disk('public')->mimeType($page->path);
            return Response::make($file, 200)->header("Content-Type", $type);
        }
    }

    public function getComments()
    {
//        $this->
    }
}
