<?php

namespace App\Http\Resources;

use App\Models\Part;
use Illuminate\Http\Resources\Json\JsonResource;

class TeamManga extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $part = $this->resource;

        return [
            'url' => route('manga.page',['id' =>$part->manga->id]),
            'title' => $part->manga->rus_name,
            'imgUrl' => $part->manga->cover,
            'translator' => $part->user->login,
            'views' => $part->manga->views,
            'update' => $part->updated_at->format('d:m:Y'),
            'created' => $part->manga->created_at->format('d:m:Y'),
            'count' => Part::certified()->where('manga_id', $part->manga_id)->distinct('number')->count()
        ];
    }
}
