<?php

namespace App\Http\Controllers;

use App\Models\ChatMessage;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getIndex()
    {
        return view('home.index', ['messages' =>
            ChatMessage::latest()->take(30)->get()->sortBy('id')
        ]);
    }
}
