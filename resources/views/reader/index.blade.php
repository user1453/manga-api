@extends('layouts.reader')

@section('title', 'teikei.me')

@section('header')
    <header class="fixed">
        <div class="name p-10px transition-m" data-link="{{route('manga.page',['id' => $manga->id])}}">
            <span class="font-14px bold">{{$manga->rus_name}}</span>
            <br>
            <span class="font-13px ">{{$manga->origin_name}}</span>
        </div>
    @if(@$_GET['typeRead'] == true)
        <!-- панель при чтении в вертикальном режиме -->

            <div class="select select-ereader transition-m">
            <span class="font-14px">
                Том {{$part->vol}} - Глава {{$part->number}}
            </span>
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                </svg>
                <ul class="simple-ul absolute left-0 list ereader-list"></ul>
            </div>
            <div class="select select-ereader-page transition-m">
                {{--                <span class="font-14px">Страница {{$page->number}}</span>--}}
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                </svg>
                <ul class="simple-ul absolute left-0 list ereader-page-list"></ul>
            </div>
    @else
        <!-- панель при чтении в горизонтальном режиме -->
            <div class="select select-ereader transition-m">
            <span class="font-14px">
                Том {{$part->vol}} - Глава {{$part->number}}
            </span>
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                </svg>
                <ul class="simple-ul absolute left-0 list ereader-list"></ul>
            </div>
        @endif
        <div class="select select-ereader-mode transition-m">
            <span class="font-14px">Режим чтения</span>
            <svg>
                <use xlink:href="/assets/img/sprite.svg#exArr"></use>
            </svg>
            <ul class="simple-ul absolute left-0 list ereader-mode-list">
                <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" id="typeHorisontal">Горизонтальный</li>
                <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" id="typeVertikal">Вертикальный</li>
            </ul>
        </div>
        <div class="translator">
            <!-- <svg>
                <use xlink:href="/assets/img/sprite.svg#translator"></use>
            </svg> -->
            <span class="font-14px">{{$translator->login}}</span>
        </div>
        <button type="button" class="bold font-14px transition-m" id="ereaderSettingsBtn">Настройки</button>
        <div class="addBookmark">
            <svg class="transition-m">
                <use xlink:href="/assets/img/sprite.svg#addBookmark"></use>
            </svg>
        </div>
    </header>

@stop
@section('main')
    <div class="ereader-box">
        @if(!empty($pages))
            <div class="ereader-img-box absolute left-0 right-0">
                @foreach($pages as $page)
                    <div class="id-{{$page->path}}">
                        <img src="{{route('reader.image', ['part' => $page->part_id, 'page' => $page->number])}}" alt="">
                    </div>
                @endforeach
            </div>
            <div class="prev transition-m" style="width: 200px">
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#back"></use>
                </svg>
            </div>
            <div class="next transition-m" style="width: 200px">
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#back"></use>
                </svg>
            </div>
        @else

            <div class="ereader-img-box absolute left-0 right-0">
                <div class="id-">
                    <img src="{{route('reader.image', ['part' => $page->part_id, 'page' => $page->number])}}" alt="">
                </div>
            </div>
            <div class="prev transition-m" style="width: 200px">
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#back"></use>
                </svg>
            </div>
            <div class="next transition-m" style="width: 200px">
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#back"></use>
                </svg>
            </div>
        @endif
    </div>
    <div class="substrate fixed top-0 left-0 bottom-0 right-0">
        <div id="ereaderSettingsModal" class="absolute top-0">
            <h3 class="font-16px b-se-d c-li normal">Тема читалки</h3>
            <section>
                <div class="radio">
                    <input class="radio_input" name="eReaderTheme" type="radio" id="light" checked>
                    <label class="radio_label" for="light">Светлая</label>
                </div>
                <div class="radio">
                    <input class="radio_input" name="eReaderTheme" type="radio" id="dark">
                    <label class="radio_label" for="dark">Темная</label>
                </div>
                <div class="radio">
                    <input class="radio_input" name="eReaderTheme" type="radio" id="oled">
                    <label class="radio_label" for="oled">Для OLED экранов</label>
                </div>
            </section>

            <h3 class="font-16px b-se-d c-li normal">Качество изображений</h3>
            <section>
                <div class="radio">
                    <input class="radio_input" name="eReaderZip" type="radio" id="origin" checked>
                    <label class="radio_label" for="origin">Оригинал</label>
                </div>
                <div class="radio">
                    <input class="radio_input" name="eReaderZip" type="radio" id="ziped">
                    <label class="radio_label" for="ziped">Сжатые (экономия трафика)</label>
                </div>
            </section>

            <h3 class="font-16px b-se-d c-li normal">Рзмер изображения</h3>
            <section>
                <div class="radio">
                    <input class="radio_input" name="eReaderSizeImg" type="radio" id="normal" checked>
                    <label class="radio_label" for="normal">Обычный</label>
                </div>
                <div class="radio">
                    <input class="radio_input" name="eReaderSizeImg" type="radio" id="full">
                    <label class="radio_label" for="full">На весь экран</label>
                </div>
                <div class="radio">
                    <input class="radio_input" name="eReaderSizeImg" type="radio" id="height">
                    <label class="radio_label" for="height">По высоте</label>
                </div>
            </section>
        </div>
    </div>
@stop

@push('css')

    <meta name="parts-url"
          content="{{route('reader.parts', ['id' => $part->manga_id,'translator' => $part->user_id])}}">
    <meta name="pages-url" content="{{route('reader.pages', ['id' => $part->id])}}">
    <meta name="next-page-url" content="{{$nextUrl}}">
    <meta name="prev-page-url" content="{{$prevUrl}}">
    {{--    <link rel="stylesheet" href="/assets/css/catalog.css">--}}
@endpush

@push('scripts')
    {{--    <script src="/assets/js/catalog.js" charset="utf-8"></script>--}}
@endpush
