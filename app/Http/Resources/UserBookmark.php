<?php

namespace App\Http\Resources;

use App\Models\Manga;
use App\Models\Part;
use Illuminate\Http\Resources\Json\JsonResource;

class UserBookmark extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $bookmark = $this->resource;
        $manga = $bookmark->manga;

        return [
            'list' => $bookmark->list,
            'mangaId' => $manga->id,
            'rusName' => $manga->rus_name,
            'cover' => $manga->cover,
            'translater' => Manga::translators($manga->id)
        ];
    }
}
