@foreach($users as $user)

    <a href="{{route('profile.page',['id' => $user->id])}}" class="line">

        <img src="{{asset('/assets/img/coder-ava.jpg')}}" alt="{{$user->login}}">
        <div>
            <h3 class="font-18px m-b-10px">{{$user->login}}</h3>
            <p class="font-16px">
                @foreach($user->getRoleNames() as $name)
                    <span class="role">{{$name}}</span>
                @endforeach
            </p>
        </div>
    </a>
    <hr>
@endforeach
