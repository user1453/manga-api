@extends('layouts.page')

@section('title', 'catalog')

@section('main')
    <div class="wrapper">
        <div class="content-area">
            <header class="shadow-dark p-10px">
                <!-- <span class="online font-15px"><span>online</span></span> -->
                <div class="img" style="background-image: url({{$team->background ?? '/assets/img/bg.png'}})"></div>
                <div class="main-info p-10px">
                    <h1>{{$team->name}}</h1>
                    @if(!$team->certified)

                        <span class="font-14px">На модерации</span>
                    @endif
                    <div class="info">
                        <div class="select person">
                            <span class="font-14px">Представители</span>
                            <svg class="arr">
                                <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                            </svg>
                            <table class="person">
                                <tbody>
                                @foreach($team->delegates as $delegate)
                                    <tr>
                                        <td>{{$delegate->position}}</td>
                                        <td>{{$delegate->user->login}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="select contacts">
                            <span class="font-14px">Контакты</span>
                            <svg class="arr">
                                <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                            </svg>
                            <table class="contacts">
                                <tbody>
                                <tr>
                                    <td>Discord</td>
                                    <td>
                                        <a class="c-fd" href="{{$team->discord}}">
                                            {{$team->discord}}
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>ВКонтакте</td>
                                    <td>
                                        <a class="c-fd" href="{{$team->vk}}">
                                            {{$team->vk}}
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <table class="m-t-10px">
                            <tbody>
                            <tr>
                                <td>Дата основания</td>
                                <td>03.08.2016</td>
                            </tr>
                            @if($team->leader() != null)
                                <tr>
                                    <td>Глава</td>
                                    <td>{{$team->leader()->user->login}}</td>
                                </tr>
                            @endif
                            <tr>
                                <td>Описание</td>
                                <td>{{$team->description}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <button type="button" class="buy-premium">
                            <svg>
                                <use xlink:href="/assets/img/sprite.svg#prime"></use>
                            </svg>
                            <span>Купить премиум</span>
                        </button>
                    </div>
                </div>
            </header>
            <div class="lists shadow-dark p-10px b-li m-t-10px">
                <div class="main-filter">
                    <button class="active">По рейтингу</button>
                    <button>По названию</button>
                    <button>По просмотрам</button>
                    <button>По дате добавления</button>
                    <button>По дате обновления</button>
                    <button>По количеству глав</button>
                </div>
                <div class="help-filter">
                    <button class="active">По убыванию</button>
                    <button>По возрастанию</button>
                </div>
                <div class="m-t-10px center manga-read-cont"></div>
            </div>
        </div>
    </div>

@stop

@push('css')
    <meta name="team-id" content="{{$team->id}}">
    <link rel="stylesheet" href="/assets/css/team.css">
@endpush

@push('scripts')

    <script src="/assets/js/team.js" charset="utf-8"></script>
@endpush
