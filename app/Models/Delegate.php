<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Delegate extends Model
{
    protected $fillable = ['user_id', 'position', 'team_id'];

    public function user()
    {
//        return $this->belongsTo('App\User', 'foreign_key', 'other_key');
        return $this->belongsTo(User::class);
    }


    public function team(){
        return $this->belongsTo(Team::class);
    }
}
