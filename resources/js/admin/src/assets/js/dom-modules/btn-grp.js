const { storage } = require('../api/storage')

let prefix = storage.system.classPrefix

export function btnGrp () {
    for (let item of document.querySelectorAll(`.${prefix}btn-grp`)) {
        let cl = item.getAttribute('cl') || undefined
        let btnNodeList = item.querySelectorAll(`.${prefix}btn`)
        let rollersNodeList = item.querySelectorAll(`.${prefix}btn-grp__roller`)

        if (cl === undefined || cl === '') {
            cl = `${prefix}active`
        }

        try{
            btnGrpRollerSetPosition(btnNodeList[0].offsetLeft, btnNodeList[0].offsetWidth)
        } catch (err) {
            storage.console.grpCollapsed(
                () => {
                    console.error(err)
                },
                '[Ошибка] модуль btn-grp',
                `${storage.console.statusCSS.err}`
             )
        }

        for (let btn of btnNodeList) {
            btn.addEventListener('click', (e) => {
                item.querySelectorAll(`.${prefix}btn`).removeClass(cl)
                btn.addClass(cl)
                try {
                    btnGrpRollerSetPosition(btn.offsetLeft, btn.offsetWidth)
                } catch (err) {
                    storage.console.grpCollapsed(
                        () => {
                            console.error(err)
                        },
                        '[Ошибка] модуль btn-grp',
                        `${storage.console.statusCSS.err}`
                     )
                }
            })
        }

        function btnGrpRollerSetPosition(l, w) {
            if (rollersNodeList !== null) {
                for (let roller of rollersNodeList) {
                    roller.style.left = `${l}px`
                    roller.style.width = `${w}px`
                }
            }
        }
    }
}
