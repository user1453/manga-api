import './assets/scss/ereader.sass'

import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => {
    if (el != undefined) {
        el.classList.add(el2)
    }
},
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)

for (let it of document.querySelectorAll('[data-link]')) {
    it.onclick = (e) => {
        document.location.href = it.dataset.link
    }
}
document.addEventListener("DOMContentLoaded", function () {
    setInterval(function () {
        for (let it of document.querySelectorAll('[data-link]')) {
            it.onclick = (e) => {
                document.location.href = it.dataset.link
            }
        }
    }, 1000);

})

class MangaList {
    constructor(cl) {
        this.id = cl
    }
    _addItemSimple (title, url) {
        let html = `
        <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" data-link="${url}">${title}</li>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }
    _addItemSimplePage (title, url) {
        let html = `
        <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" data-link="${url}">${title}</li>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }
}

function select (parent, list) {
    for (let it of dQSA(parent)) {
        it.onclick = () => {
            for (let it2 of dQSA(list)) {
                addClass(it2, 'show')
            }
        }
    }
    for (let it of dQSA(list + ' li')) {
        it.onmousedown = (e) => {
            for (let it2 of dQSA(`${parent} span`)) {
                it2.innerText = it.innerText.split(":")[0]
            }
            for (let it2 of dQSA(list)) {
                removeClass(it2, 'show')
            }
        }
    }
}

function modal (modal, button) {
    dQS(button).onclick = (e) => {
        addClass(dQS(`.substrate`), 'show')
        addClass(dQS(modal), 'show')
    }
    dQS(`.substrate`).onclick = (e) => {
        removeClass(e.target, 'show')
        for (let it of e.target.querySelectorAll('div')) {
            removeClass(it, 'show')
        }
    }
}

select('.select-ereader', '.ereader-list')
select('.select-ereader-page', '.ereader-page-list')
select('.select-ereader-mode', '.ereader-mode-list')
modal('#ereaderSettingsModal', '#ereaderSettingsBtn')

if (localStorage.getItem('ereaderstatus') === null) {
    localStorage.setItem('eReaderSizeImg', 'normal')
    localStorage.setItem('eReaderZip', 'origin')
    localStorage.setItem('eReaderTheme', 'dark')
    localStorage.setItem('eReaderType', 'horis')
    localStorage.setItem('ereaderstatus', 'ok')
}

function eReaderRadioChecked (name) {
    for (let it of dQSA(`[name="${name}"]`)) {
        it.checked = false
    }
}

function eReaderSizeImg () {
    let m = localStorage.getItem('eReaderSizeImg')
    let css = ''
    let dheight = document.documentElement.offsetHeight - 60
    eReaderRadioChecked('eReaderSizeImg')
    switch (m) {
        case 'full':
            css += `.ereader-box .ereader-img-box img {width: 100%;height: auto;}`
        break
        case 'normal':
            css += `.ereader-box .ereader-img-box img {width: auto;height: auto;}`
        break
        case 'height':
            css += `.ereader-box .ereader-img-box img {width: auto;height: ${dheight}px;}`
        break
    }
    let head = document.getElementsByTagName('head')[0]
    let s = document.createElement('style')
    s.innerText = css
    head.appendChild(s)
    dGetId(`${m}`).checked = true
}

function eReaderZip () {
    let m = localStorage.getItem('eReaderZip')
    eReaderRadioChecked('eReaderZip')
    switch (m) {
        case 'origin':

        break
        case 'ziped':

        break
    }
    dGetId(`${m}`).checked = true
}

function eReaderTheme () {
    let m = localStorage.getItem('eReaderTheme')
    let css = ''
    eReaderRadioChecked('eReaderTheme')
    switch (m) {
        case 'light':
            css += `
                p, span, div, button {
                    color: #fff !important
                }
                button {
                    background: rgba(0,0,0,.1)
                }
                button:hover {
                    background: rgba(0,0,0,.05)
                }
                header {
                    background: #00c0ff
                }
                .ereader-box {
                    background: #eee
                }
                .select {
                    background: rgba(0,0,0,.1)
                }
                .select:hover {
                    background: rgba(0,0,0,.05)
                }
                .addBookmark svg {
                    fill: #fff
                }
                .addBookmark:hover svg {
                    fill: #eee
                }
                .select ul {
                    background: #fff;
                }
                .select li {
                    color: #4d4d4d;
                }
                .select svg {
                    fill: #fff;
                }
                #ereaderSettingsModal {
                    background: #fff;
                }
                #ereaderSettingsModal h3 {
                    background: #00c0ff;
                    font-weight: bold;
                    color: #fff;
                }
                #ereaderSettingsModal * {
                    color: #00c0ff;
                }
                #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {
                    background: #fff;
                }
                #ereaderSettingsModal .radio .radio_label::before {
                    background: #eee;
                }
            `
        break
        case 'dark':
        css += `
            p, span, div, button {
                color: #fff !important
            }
            button {
                background: rgba(255,255,255,.1)
            }
            button:hover {
                background: rgba(255,255,255,.05)
            }
            header {
                background: rgb(39, 39, 39);
            }
            .ereader-box {
                background: rgb(29, 29, 29);
            }
            .select {
                background: rgba(255,255,255,.1)
            }
            .select:hover {
                background: rgba(255,255,255,.05)
            }
            .addBookmark svg {
                fill: #fff
            }
            .addBookmark:hover svg {
                fill: #00c0ff
            }
            .select ul {
                background: rgb(39, 39, 39);
            }
            .select li {
                color: rgb(238, 238, 238);
            }

            .select li:hover {
                background: rgba(0,0,0,.05)
            }
            .select svg {
                fill: #fff;
            }
            #ereaderSettingsModal {
                background: #fff;
                background: rgb(26, 26, 26);
            }
            #ereaderSettingsModal h3 {
                background: rgb(45, 45, 45);
                font-weight: normal;
            }
            #ereaderSettingsModal * {
                color: rgb(225, 225, 225);
            }
            #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {
                background: #4d4d4d;
            }
            #ereaderSettingsModal .radio .radio_label::before {
                background: rgb(77, 77, 77);
            }
        `
        break
        case 'oled':
        css += `
            p, span, div, button {
                color: #fff !important
            }
            button {
                background: rgba(255,255,255,.1)
            }
            button:hover {
                background: rgba(255,255,255,.05)
            }
            header {
                background: #000;
            }
            .ereader-box {
                background: #000;
            }
            .select {
                background: rgba(255,255,255,.1)
            }
            .select:hover {
                background: rgba(255,255,255,.05)
            }
            .addBookmark svg {
                fill: #fff
            }
            .addBookmark:hover svg {
                fill: #00c0ff
            }
            .select ul {
                background: #000;
            }
            .select li {
                color: rgb(238, 238, 238);
            }

            .select li:hover {
                background: rgba(0,0,0,.05)
            }
            .select svg {
                fill: #fff;
            }
            #ereaderSettingsModal {
                background: #fff;
                background: #000;
            }
            #ereaderSettingsModal h3 {
                background: #000;
                font-weight: normal;
            }
            #ereaderSettingsModal * {
                color: rgb(225, 225, 225);
            }
            #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {
                background: #4d4d4d;
            }
            #ereaderSettingsModal .radio .radio_label::before {
                background: rgb(77, 77, 77);
            }
        `
        break
    }
    let head = document.getElementsByTagName('head')[0]
    let s = document.createElement('style')
    s.innerText = css
    head.appendChild(s)
    dGetId(`${m}`).checked = true
}

eReaderSizeImg()
eReaderZip()
eReaderTheme()

for (let it of dQSA(`[name="eReaderTheme"], [name="eReaderZip"], [name="eReaderSizeImg"]`)) {
    it.onchange = (e) => {
        localStorage[e.target.name] = it.id
        eReaderSizeImg()
        eReaderZip()
        eReaderTheme()
    }
}

let searchUrl = document.location.search
searchUrl = searchUrl.replace('?', '').split('&')
let searchs = {}

searchUrl.map((item, i, arr) => {
    searchs[item.split('=')[0]] = item.split('=')[1]
})

dQS('.addBookmark').onclick = () => {
    let addSimpleList = fetch('/bookmark/store', {
        method: 'POST',
        mode: "cors",
        headers: {
            'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({id: parseInt(searchs.id)})
    }).then(req => alert('Добавлено в закладки'))

}

let addSimpleList = fetch( dQS('[name="parts-url"]').getAttribute('content'), {
    method: 'POST',
    mode: "cors",
    headers: {
        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content')
    },
})
addSimpleList
.then(req => req.json())
.then(req => {
    let simpleList = new MangaList('ereader-list')
    localStorage.setItem('partsCount', 3)
    for (var item of req) {
        localStorage.setItem('partsCount', Number(localStorage.getItem('partsCount')) + 1)
        simpleList._addItemSimple(
            `Том ${item.vol} - Глава ${item.part} : ${item.name}`,
            item.url
        )
    }
})

let addSimpleListPage = fetch( dQS('[name="pages-url"]').getAttribute('content'), {
    method: 'POST',
    mode: "cors",
    headers: {
        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content')
    },
})
addSimpleListPage
.then(req => req.json())
.then(req => {
    let simpleListPage = new MangaList('ereader-page-list')
    localStorage.setItem('pageCount', req)
    for (let i = 1; i <= req; i++) {
        simpleListPage._addItemSimple(`Страница ${i}`, `/ereader?id=${searchs.id}&part=${searchs.part}&page=${String(i).padStart(3,'0')}&translator=${searchs.translator}`)
    }
})

if (localStorage.getItem('eReaderType') == 'horis') {
    if (document.location.href.includes('&typeRead=true')) {
        document.location.href = document.location.href.replace('&typeRead=true', '')
    }
    for (let it of dQSA(`.next`)) {
        it.onclick = (e) => {
            window.location = dQS('[name="next-page-url"]').getAttribute('content')
        }
    }

    for (let it of dQSA('[class*="id-"]')) {
        it.onclick = (e) => {
            window.location = dQS('[name="next-page-url"]').getAttribute('content')
        }
    }

    for (let it of dQSA(`.prev`)) {
        it.onclick = (e) => {
            window.location = dQS('[name="prev-page-url"]').getAttribute('content')
        }
    }
}

if (localStorage.getItem('eReaderType') == 'vert') {
    if (!document.location.href.includes('&typeRead=true')) {
        document.location.href += `&typeRead=true`
    }
    for (let it of dQSA(`.next`)) {
        it.onclick = (e) => {
                window.location = dQS('[name="next-page-url"]').getAttribute('content')
        }
    }
    for (let it of dQSA('[class*="id-"]')) {
        it.onclick = (e) => {
            window.location = dQS('[name="next-page-url"]').getAttribute('content')
        }
    }
    for (let it of dQSA(`.prev`)) {
        it.onclick = (e) => {
            window.location = dQS('[name="prev-page-url"]').getAttribute('content')
        }
    }
}

dQS('#typeHorisontal').onmousedown = () => {
    localStorage.setItem('eReaderType', 'horis')
    document.location.href = document.location.href.replace('&typeRead=true', '')
}

dQS('#typeVertikal').onmousedown = () => {
    localStorage.setItem('eReaderType', 'vert')
    document.location.href += `&typeRead=true`
}

for (let it of dQSA('img')) {
    it.ondragstart = () => {
        return false
    }
}

document.body.oncontextmenu = (e) => {
    return false
}

document.onclick = (e) => {
    let target = e.target
    if (!(target.closest('.select-ereader'))) {
        for (let it of dQSA('.ereader-list')) {
            removeClass(it, 'show')
        }
    }
    if (!(target.closest('.select-ereader-page'))) {
        for (let it of dQSA('.ereader-page-list')) {
            removeClass(it, 'show')
        }
    }
    if (!(target.closest('.select-ereader-mode'))) {
        for (let it of dQSA('.ereader-mode-list')) {
            removeClass(it, 'show')
        }
    }
}
