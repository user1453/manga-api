const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => el.classList.add(el2),
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)

let storage = {}

for (let it of dQSA('.menu__nav__search-box__panel__params span')) {
    it.onclick = (e) => {
        for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {
            removeClass(it2, 'active')
        }
        storage.searchTypeSort = it.dataset.typeSort
        addClass(it, 'active')
    }
}

for (let it of dQSA('.menu__nav__search-box__search-input')) {
    it.onfocus = () => {
        for (let it2 of dQSA('.menu__nav__search-box__panel')) {
            addClass(it2, 'show')
        }
    }
    it.oninput = () => {
        if (it.value.length >= 3) {
            for (let it2 of dQSA('.menu__nav__search-box__search-process')) {
                addClass(it2, 'search-process-anim')
            }
            for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {
                addClass(it2, 'show')
            }
        } else {
            for (let it2 of dQSA('.menu__nav__search-box__search-process')) {
                removeClass(it2, 'search-process-anim')
            }
            for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {
                removeClass(it2, 'show')
            }
        }
    }
}

document.onmousedown = (e) => {
    let target = e.target
    if (!(target.closest('.menu__nav__search-box__panel')) && (!(target.closest('.menu__nav__search-box__search-input')))) {
        for (let it of dQSA('.menu__nav__search-box__panel')) {
            removeClass(it, 'show')
        }
    }
}
