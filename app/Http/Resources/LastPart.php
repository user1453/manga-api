<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LastPart extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $part = $this->resource;
        return [
            'vol' => $part->vol,
            'part' => $part->number,
            'translater' => $part->user->login,
            'pUrl' => route('profile.page', ['id' => $part->user->id]),
            'url' => $part->url(),
        ];
    }
}
