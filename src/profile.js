import './assets/scss/profile.sass'
import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => el.classList.add(el2),
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)
