@foreach($parts as $part)

    <a href="{{route('manga.page',['id' => $part->manga_id])}}" class="line" data-name-manga="{{$part->manga->rus_name}}">

        <img src="{{$part->manga->cover}}" alt="{{$part->manga->rus_name}}">

        <div>

            <h3 class="font-18px m-b-10px">{{$part->manga->origin_name}}</h3>

            <p class="font-16px">{{$part->manga->rus_name}}</p>

        </div>

    </a>

    <hr>
@endforeach
