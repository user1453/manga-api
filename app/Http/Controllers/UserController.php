<?php

namespace App\Http\Controllers;

use App\Http\Resources\ErrorResponse;
use App\Http\Resources\AuthResponse;
use App\Http\Resources\StatusResponse;
use App\Models\User;
use http\Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function authenticate(Request $request)
    {
        $credentials = [
            'login' => $request->get('log'),
            'password' => $request->get('pass')
        ];

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(new ErrorResponse('invalid_credentials'), 200);
            }
        } catch (JWTException $e) {
            return response()->json(new ErrorResponse('could_not_create_token'), 200);
        }

        return response()->json(new AuthResponse($token));
    }

    public function register(Request $request)
    {
        $input = [
            'login' => $request->get('log'),
            'password' => $request->get('pass'),
            'email' => $request->get('eMail'),
            'password_confirmation' => $request->get('passCheck')
        ];

        $validator = Validator::make($input, [
            'login' => 'required|string|unique:users|max:255|min:1|regex:/^[A-Za-z0-9]+(?:[ _-][A-Za-z0-9]+)*$/',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:1|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(new ErrorResponse($validator->errors()), 200);
        }

        $user = User::create([
            'login' => $input['login'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

//        event(new Registered($user));
        $token = JWTAuth::fromUser($user);

        return response()->json(new AuthResponse($token), 201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function uniqueLogin(Request $request)
    {
        $login = $request->get('log');
        $user = User::where('login', $login)->first();

        return response()->json(new StatusResponse($user == null ? 1 : 0));
    }

    public function uniqueEmail(Request $request)
    {
        $email = $request->get('eMail');
        $user = User::where('email', $email)->first();

        return response()->json(new StatusResponse($user == null ? 1 : 0));
    }

    public function recoverPassword(Request $request)
    {

    }

    public function ajaxLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'log' => 'required|string',
            'pass' => 'required',
        ]);

        if ($validator->passes()) {
            $login = $request->get('log');

            $user = User::where('login', $login)->first();
            if ($user == null) {
                $user = User::where('email', $login)->first();
                if ($user == null)
                    return response(new StatusResponse(0), 200);
            }

            if (auth()->attempt(array('email' => $user->email,
                'password' => $request->input('pass')), true)) {
                return response(new StatusResponse(1), 200);
            }
            return response(['error' => 'Sorry User not found.'], 200);
        }

        return response(new ErrorResponse($validator->errors()), 200);
    }

    public function getName(Request $request)
    {
        $user = User::where('login', $request->get('login'))->first();
        if ($user == null)
            return response(null, 204);


        return response()->json($user->login, 200);
    }

    public function regInfoCheckLog(Request $request)
    {
        $input = [
            'login' => $request->get('log'),
        ];

        $validator = Validator::make($input, [
            'login' => 'required|string|unique:users|max:255|min:4|regex:/^[A-Za-z0-9_]+(?:[_-][A-Za-z0-9]+)*$/',
        ]);
        if ($validator->fails()) {
            return response(new ErrorResponse($validator->errors()), 200);
        }
        return response(new StatusResponse(1), 200);
    }

    public function regInfoCheckMail(Request $request)
    {
        $input = [
            'email' => $request->get('eMail'),
        ];

        $validator = Validator::make($input, [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        if ($validator->fails()) {
            return response(new ErrorResponse($validator->errors()), 200);
        }
        return response(new StatusResponse(1), 200);
    }

    public function loginRecCheck(Request $request)
    {
        $login = $request->get('log');
        if (User::where('login', $login)->orWhere('email', $login)->first() != null)
            return response(new StatusResponse(1));
        return response(new StatusResponse(0));
    }


    public function forgotPassword(Request $request)
    {
        $login = $request->get('log');
        $user = User::where('login', $login)->orWhere('email', $login)->first();

//Create Password Reset Token
        DB::table('password_resets')->insert([
            'email' => $user->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);
//Get the token just created above
        $tokenData = DB::table('password_resets')
            ->where('email', $user->email)->first();

        if ($this->sendResetEmail($user, $tokenData->token)) {
            return response(new StatusResponse(trans('A reset link has been sent to your email address.')), 200);
        } else {

            return response(new ErrorResponse(['error' => trans('A Network Error occurred. Please try again.')]), 200);
        }
    }

    private function sendResetEmail($user, $token)
    {
//Generate, the password reset link. The token generated is embedded in the link
        $link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($user->email);

        try {
            Mail::send('emails.resetpass', ['user' => $user, 'link' => $link], function ($m) use ($user) {
                $m->from('info@teikei.me', 'teikei.me');

                $m->to($user->email, $user->login)->subject('Reset password');
            });
            //Here send the link with CURL with an external email API
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
