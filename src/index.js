const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
    dGetClass = (el) => document.getElementsByClassName(el),
    dGetTag = (el) => document.getElementsByTagName(el),
    dGetId = (el) => document.getElementById(el),
    dQS = (el) => document.querySelector(el),
    dQSA = (el) => document.querySelectorAll(el),
    removeClass = (el, el2) => el.classList.remove(el2),
    addClass = (el, el2) => {
        if (el !== undefined) {
            el.classList.add(el2)
        }
    },
    replaceClass = (el, clr, cla) => {
        el.classList.remove(clr)
        el.classList.add(cla)
    },
    hasClass = (el, el2) => el.classList.contains(el2)

document.addEventListener("DOMContentLoaded", function () {
    setInterval(function () {
        for (let it of document.querySelectorAll('p, td, h1, h2, h3, h4')) {
            let text = it.innerHTML
            let teams = [
                {
                    name: 'GL Alliance',
                    url: '/team.html'
                }, {
                    name: 'Черный кролик',
                    url: '/team.html'
                }
            ]
            let names = [
                {
                    name: 'Дайлайн',
                    url: '/profile.html'
                }, {
                    name: 'Кадис',
                    url: '/profile.html'
                }
            ]
            for (let it2 of teams) {
                text = text.replace(new RegExp(`${it2.name}`, 'g'), `<a href="${it2.url}" class="global-search-link">${it2.name}</a>`)
            }
            for (let it2 of names) {
                text = text.replace(new RegExp(`${it2.name}`, 'g'), `<a href="${it2.url}" class="global-search-link">${it2.name}</a>`)
            }
            it.innerHTML = text
        }
        for (let it of document.querySelectorAll('[data-link]')) {
            it.onclick = (e) => {
                document.location.href = it.dataset.link
            }
        }
    }, 1000);

    // class Comments {
    //     constructor(selector) {
    //         this.selector = selector
    //         this.htmlMain = `
    //         <div class="textarea m-t-10px b-r-5px">
    //             <div class="input font-14px" contenteditable="true"></div>
    //             <div class="controls">
    //                 <div>
    //                     <span data-func="bold">
    //                         <svg class="folder transition-m">
    //                             <use xlink:href="/assets/img/sprite.svg#bold"></use>
    //                         </svg>
    //                     </span>
    //                     <span data-func="italic">
    //                         <svg class="folder transition-m">
    //                             <use xlink:href="/assets/img/sprite.svg#italic"></use>
    //                         </svg>
    //                     </span>
    //                     <span data-func="underline">
    //                         <svg class="folder transition-m">
    //                             <use xlink:href="/assets/img/sprite.svg#underline"></use>
    //                         </svg>
    //                     </span>
    //                     <span data-func="striket">
    //                         <svg class="folder transition-m">
    //                             <use xlink:href="/assets/img/sprite.svg#striket"></use>
    //                         </svg>
    //                     </span>
    //                     <span data-func="spoiler">
    //                         <svg class="folder transition-m">
    //                             <use xlink:href="/assets/img/sprite.svg#eye"></use>
    //                         </svg>
    //                     </span>
    //                 </div>
    //                 <div>
    //                     <button type="button" class="send">Отправить</button>
    //                 </div>
    //             </div>
    //         </div>
    //             <div class="m-t-10px comm-box">
    //
    //             </div>
    //         </div>`
    //         dQS(`${this.selector} .controls .send`).onmousedown = (e) => {
    //             // отправка комментария
    //             dQS(`${this.selector} .reply`).onclick = (e) => {
    //                 log('reply')
    //                 this._reply()
    //             }
    //             // this._addComm(dQS(`${this.selector} .input`).innerHTML)
    //             dQS(`${this.selector} .textarea`).remove()
    //         }
    //         // редактирование в .textarea
    //
    //     }
    //     _reply () {
    //         let reply = new Comments(`${this.selector} .reply-box`)
    //     }
    // }
    //
    // if (dQS('#manga-comments') != null) {
    //     let mainComm = new Comments('#manga-comments', 'main')
    // }

    /*





    */

    // for (let it of dQSA(`${this.selector} .controls span`)) {
    //     it.onmousedown = (e) => {
    //         let el = dQS('.textarea .input')
    //         el.focus()
    //         switch (it.dataset.func) {
    //             case 'bold':
    //                 document.execCommand('bold')
    //             break
    //             case 'italic':
    //                 document.execCommand('italic')
    //             break
    //             case 'underline':
    //                 document.execCommand('underline')
    //             break
    //             case 'striket':
    //                 document.execCommand('strikeThrough')
    //             break
    //             case 'spoiler':
    //                 dQS('.textarea .input').innerHTML += `
    //                 <div class="before-spoiler"></div>
    //                 <span class="spoiler"></span>
    //                 <div class="after-spoiler"></div>
    //                 `
    //             break
    //         }
    //
    //     }
    //     it.onclick = (e) => {
    //         let el = dQS('.textarea .input')
    //     }
    // }

    for (let item of dQSA('button[data-send-id]')) {
        item.onmousedown = (e) => {
            let sendId = item.dataset.sendId
            let fData = new FormData;
            fData.append('message', dQS(`[data-textarea-id="${sendId}"]`).innerHTML)
            fData.append('commentable_type', item.dataset.commentableType)
            fData.append('commentable_id', item.dataset.commentableId)

            let response = fetch(item.dataset.action, {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                },
                body: fData,
            })
            response
                .then(req => req.text())
                .then(req => {
                    document.location.reload()
                })
        }
    }

    for (let item of dQSA(`[data-t-textarea-id="main"] span`)) {
        item.onmousedown = (e) => {
            let el = dQSA(`[data-textarea-id="main"]`)
            switch (item.dataset.func) {
                case 'bold':
                    document.execCommand('bold')
                    break
                case 'italic':
                    document.execCommand('italic')
                    break
                case 'underline':
                    document.execCommand('underline')
                    break
                case 'striket':
                    document.execCommand('strikeThrough')
                    break
                case 'spoiler':
                    dQS(`[data-textarea-id="main"]`).innerHTML += `
                    <div class="before-spoiler"></div>
                    <span class="spoiler"></span>
                    <div class="after-spoiler"></div>
                    `
                    break
            }

        }
    }

    for (let item of dQSA('a[data-comm-reply-id]')) {
        item.onmousedown = (e) => {

            let replyId = item.dataset.commReplyId
            let replyAction = item.dataset.commReplyAction

            if (dQS(`[data-t-textarea-id="${replyId}"]`) !== null) {
                dQS(`[data-t-textarea-id="${replyId}"]`).remove()
            }

            dQS(`div[data-comm-reply-id="${replyId}"]`).innerHTML += `
            <div class="textarea m-t-10px b-r-5px" data-t-textarea-id="${replyId}">
                <div class="input font-14px" contenteditable="true" data-textarea-id="${replyId}"></div>
                <div class="controls">
                    <div>
                        <span data-func="bold">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#bold"></use>
                            </svg>
                        </span>
                        <span data-func="italic">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#italic"></use>
                            </svg>
                        </span>
                        <span data-func="underline">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#underline"></use>
                            </svg>
                        </span>
                        <span data-func="striket">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#striket"></use>
                            </svg>
                        </span>
                        <span data-func="spoiler">
                            <svg class="folder transition-m">
                                <use xlink:href="/assets/img/sprite.svg#eye"></use>
                            </svg>
                        </span>
                    </div>
                    <div>
                        <button type="button" class="send" data-send-id="${replyId}">Отправить</button>
                    </div>
                </div>
            </div>`


            for (let item of dQSA(`[data-t-textarea-id="${replyId}"] span`)) {
                item.onmousedown = (e) => {
                    let el = dQSA(`[data-textarea-id="${replyId}"]`)
                    switch (item.dataset.func) {
                        case 'bold':
                            document.execCommand('bold')
                            break
                        case 'italic':
                            document.execCommand('italic')
                            break
                        case 'underline':
                            document.execCommand('underline')
                            break
                        case 'striket':
                            document.execCommand('strikeThrough')
                            break
                        case 'spoiler':
                            dQS(`[data-textarea-id="${replyId}"]`).innerHTML += `
                            <div class="before-spoiler"></div>
                            <span class="spoiler"></span>
                            <div class="after-spoiler"></div>
                            `
                            break
                    }

                }
            }

            dQS(`[data-send-id="${replyId}"]`).onclick = () => {

                let fData = new FormData;
                fData.append('message', dQS(`[data-textarea-id="${replyId}"]`).innerHTML);

                let response = fetch(replyAction, {
                    method: 'POST',
                    body: fData,
                    mode: "cors",
                    headers: {
                        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                    },
                })
                response
                    .then(req => req.text())
                    .then(req => {
                        if (req == 'ok') {
                            document.location.reload()
                        }
                    })
            }
        }
    }


    for (let it of dQSA('[data-auth]')) {
        it.onclick = (e) => {
            let type = it.dataset.auth

            if (type == 'recovery') {

                let val = dQS(`[data-auth-modal="sign-in"] input[data-auth-input="log"]`).value
                let item = dQS(`[data-auth-modal="recovery"] input[data-auth-input="log"]`)
                item.value = val
                // item.focus()

                validateInput(['login-rec'])

            }

            removeClass(dQS('.substrate'), 'hide')

            for (let item of dQSA(`[data-auth-modal]`)) {
                addClass(item, 'hide')
            }

            removeClass(dQS(`[data-auth-modal="${type}"]`), 'hide')

        }
        if (dQS('.substrate') != null)
            dQS('.substrate').onmousedown = (e) => {
                let target = e.target

                if (
                    target.closest('.substrate')
                    &&
                    !target.closest('.modal')
                ) {
                    addClass(dQS('.substrate'), 'hide')
                }
            }
    }

    for (let it of dQSA('.comment-box .func-comm button')) {
        it.onclick = (e) => {

            for (let it2 of dQSA('.comment-box .func-comm button')) {
                removeClass(it2, 'active')
            }

            addClass(it, 'active')
        }
    }

    function checkbox(el) {
        for (let it of dQSA(`${el}`)) {
            it.onclick = (e) => {
                if (hasClass(it, 'active')) {

                    removeClass(it, 'active')
                    it.removeAttribute('data-selected')

                } else {

                    addClass(it, 'active')
                    it.setAttribute('data-selected', 'true')

                }
            }
        }
    }

    // PAART

    checkbox('.checkbox')

    validateInput(['login', 'e-mail', 'pass', 'pass-check', 'login-rec'])

    function validateInput(types) {
        dQS(`input[data-auth-check="pass"]`).oninput = (e) => {
            let item = dQS(`input[data-auth-check="pass-check"]`)
            let error = dQS(`span[data-auth-check="pass"]`)

            removeClass(item, 'ok')
        }

        for (let type of types) {
            if (dQS(`input[data-auth-check="${type}"]`) !== null) {
                dQS(`input[data-auth-check="${type}"]`).onchange = (e) => {

                    let item = dQS(`input[data-auth-check="${type}"]`)
                    let error = dQS(`span[data-auth-check="${type}"]`)
                    let val = item.value.replace(/\s+/g, ' ').trim()
                    let valLower = item.value.replace(/\s+/g, ' ').trim().toLowerCase()
                    let errChars = ['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm', '_', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

                    if (val !== '') {

                        let next = true

                        if (type == 'login') {
                            for (let i of valLower) {
                                let flag = false
                                next = false

                                // if(val.length > 4)
                                for (let j of errChars) {
                                    if (i === j) {
                                        flag = true
                                        break
                                    }
                                }


                                if (flag === false) {

                                    error.innerText = 'Логин может содержать только латиницу, цифры и _'

                                    addClass(error, 'show')
                                    removeClass(item, 'ok')

                                    next = false

                                    break

                                } else {

                                    error.innerText = 'Упс! Такой уже существует'
                                    next = true

                                }

                            }
                        }

                        if (type == 'pass') {
                            removeClass(dQS(`input[data-auth-check="pass-check"]`), 'ok')
                            dQS(`input[data-auth-check="pass-check"]`).value = ''



                            for (let i of valLower) {
                                let flag = false
                                next = false

                                // if(val.length > 4)
                                for (let j of errChars) {
                                    if (i === j) {
                                        flag = true
                                        break
                                    }
                                }


                                if (flag === false) {

                                    addClass(error, 'show')
                                    removeClass(item, 'ok')
                                    removeClass(dQS(`input[data-auth-check="pass-check"]`), 'ok')

                                    next = false

                                    break

                                } else {

                                    removeClass(error, 'show')
                                    addClass(item, 'ok')
                                    sendReg('[data-auth-modal="recovery"]')
                                    sendReg('[data-auth-modal="reg"]')

                                    next = true

                                }

                            }
                        }

                        if (type == 'pass-check') {
                            if (val == dQS(`input[data-auth-check="pass"]`).value.replace(/\s+/g, ' ').trim()) {

                                removeClass(error, 'show')
                                addClass(item, 'ok')
                                sendReg('[data-auth-modal="recovery"]')
                                sendReg('[data-auth-modal="reg"]')
                                next = true

                            } else {

                                addClass(error, 'show')
                                removeClass(item, 'ok')

                            }
                        }


                        if (next) {

                            let form = '[data-auth-modal="reg"]'

                            if (type == 'login') {
                                let obj = {
                                    log: dQS(`${form} [data-auth-input="log"]`).value,
                                }

                                let response = fetch('/reg-info-check-log', {
                                    method: 'POST',
                                    mode: "cors",
                                    headers: {
                                        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                                        'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify(obj),
                                })

                                response
                                    .then(req => req.json())
                                    .then(req => {
                                        if (req.status == 1) {

                                            removeClass(error, 'show')
                                            addClass(item, 'ok')
                                            sendReg('[data-auth-modal="reg"]')

                                        } else if (req.status == 0) {

                                            addClass(error, 'show')
                                            removeClass(item, 'ok')

                                        }
                                    })
                            }

                            if (type == 'e-mail') {
                                let obj = {
                                    eMail: dQS(`${form} [data-auth-input="e-mail"]`).value,
                                }

                                let response = fetch('/reg-info-check-mail', {
                                    method: 'POST',
                                    mode: "cors",
                                    headers: {
                                        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                                        'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify(obj),
                                })

                                response
                                    .then(req => req.json())
                                    .then(req => {
                                        if (req.status == 1) {

                                            removeClass(error, 'show')
                                            addClass(item, 'ok')
                                            sendReg('[data-auth-modal="reg"]')

                                        } else if (req.status == 0) {

                                            addClass(error, 'show')
                                            removeClass(item, 'ok')

                                        }
                                    })
                            }

                            if (type == 'login-rec') {
                                let obj = {
                                    log: dQS(`[data-auth-modal="recovery"] [data-auth-check="login-rec"]`).value,
                                }

                                let response = fetch('/login-rec-check', {
                                    method: 'POST',
                                    mode: "cors",
                                    headers: {
                                        'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                                        'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify(obj),
                                })

                                response
                                    .then(req => req.json())
                                    .then(req => {
                                        if (req.status == 1) {

                                            removeClass(error, 'show')
                                            addClass(item, 'ok')
                                            sendReg('[data-auth-modal="recovery"]')

                                        } else if (req.status == 0) {

                                            addClass(error, 'show')
                                            removeClass(item, 'ok')

                                        }
                                    })
                            }

                            /*
                            *       response status -> 0 - err, 1 - ok
                            */
                        }

                    } else {

                        removeClass(item, 'ok')
                        removeClass(error, 'show')

                    }

                    /*

                    *       WARNING: при регистрации сделать повторную валидацию
                    *       login: наличие в БД, отсутствие: любых символов кроме письменых
                    *       e-mail: наличие в БД, валидность

                    */
                }
            }
        }
    }

    function sendReg(form) {
        let send = dQS(`${form} .send-form`)
        let flag = true

        dQSA(`${form} input`).forEach(item => {
            if (!hasClass(item, 'ok')) {
                flag = false
            }
        })

        if (flag === true) {
            send.disabled = false
        } else {
            send.disabled = true
        }

        for (let input of dQSA(`${form} input[data-auth-check]`)) {
            input.oninput = (e) => {
                let item = e.target

                send.disabled = true
            }
        }
    }

    dQS(`[data-auth-modal="reg"] .send-form`).onclick = (e) => {
        let form = '[data-auth-modal="reg"]'
        let type = dQS(`${form}`).dataset.authModal

        let obj = {
            log: dQS(`${form} [data-auth-input="log"]`).value,
            eMail: dQS(`${form} [data-auth-input="e-mail"]`).value,
            pass: dQS(`${form} [data-auth-input="pass"]`).value,
            passCheck: dQS(`${form} [data-auth-input="pass-check"]`).value,
        }

        let response = fetch('/api/v1/register', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
        })

        response
            .then(req => req.json())
            .then(req => {
                log(req)
                if (req.status == 1) {

                    // document.cookie = `SESSION_KEY=${req.session}; max-age=1209600`

                    window.location.reload()

                } else {

                    alert(`Сервер отказался обрабатывать ваш запрос. Попробуйте еще раз. Он иногда капризничает... Если не получится: предложите ему конфеты или обратитесь в службу поддержки`)

                }
            })
    }

    dQS(`[data-auth-modal="sign-in"] .send-form`).onclick = (e) => {
        let form = '[data-auth-modal="sign-in"]'
        let type = dQS(`${form}`).dataset.authModal

        let obj = {
            log: dQS(`${form} [data-auth-input="log"]`).value,
            pass: dQS(`${form} [data-auth-input="pass"]`).value,
        }
        let response = fetch('/ajax-login', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
        })

        response
            .then(req => req.json())
            .then(req => {
                log(req)
                if (req.status == 1) {

                    let age = 86400

                    if (hasClass(dQS(`${form} .checkbox`), 'active')) {
                        age = 1209600
                    }
                    // document.cookie = `SESSION_KEY=${req.session}; max-age=${age}`
                    window.location.reload()

                } else {

                    if (req.err == 'log') {
                        removeClass(dQS(`${form} [data-auth-check="pass"]`), 'show')
                        addClass(dQS(`${form} [data-auth-check="log"]`), 'show')
                    } else if (req.err == 'pass') {
                        removeClass(dQS(`${form} [data-auth-check="log"]`), 'show')
                        addClass(dQS(`${form} [data-auth-check="pass"]`), 'show')
                    }

                }
            })
    }

    dQS(`[data-auth-modal="recovery"] .send-form`).onclick = (e) => {
        let form = '[data-auth-modal="recovery"]'
        let type = dQS(`${form}`).dataset.authModal

        let obj = {
            log: dQS(`${form} [data-auth-input="log"]`).value,
        }

        let response = fetch('/login-rec', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(obj),
        })

        response
            .then(req => req.json())
            .then(req => {
                log(req)
                if (req.status == 1) {

                    addClass(dQS(`${form} .rec-w`), 'hide')
                    removeClass(dQS(`${form} .rec-c`), 'hide')

                } else {

                    alert(`Сервер отказался обрабатывать ваш запрос. Попробуйте еще раз. Он иногда капризничает... Если не получится: предложите ему конфеты или обратитесь в службу поддержки`)

                }
            })
    }

    select('.team-select', '.team-select-ul')

    function select(parent, list) {
        for (let it of dQSA(parent)) {
            it.onclick = () => {
                for (let it2 of dQSA(list)) {
                    removeClass(it2, 'hide')
                    it2.style.left = -it2.offsetWidth + 50
                }
            }
        }
        document.onclick = (e) => {
            let target = e.target
            if (!target.closest(parent)) {
                for (let it2 of dQSA(list)) {
                    addClass(it2, 'hide')
                }
            }
        }
    }

    dQSA('[data-exit]').forEach(item => {
        item.onclick = (e) => {
            dGetId('logout-form').submit();
            // document.cookie = "SESSION_KEY=; max-age=0"
            // document.location.reload()
        }
    })

    if (auth)
        dQS('.open-notif').onclick = () => {
            if (hasClass(dQS('.notifications'), 'hide')) {
                removeClass(dQS('.notifications'), 'hide')
                dQS('.close-notif').onclick = () => {
                    addClass(dQS('.notifications'), 'hide')
                }
            } else {
                addClass(dQS('.notifications'), 'hide')
            }
        }

    let sInput = dQS('.menu__nav__search-box__search-input')
    let sPanel = dQS('.menu__nav__search-box__panel')
    let sResult = dQS('.menu__nav__search-box__panel__result')
    let sType = 'manga'

    for (var item of dQSA('[data-type-sort]')) {
        item.onclick = (e) => {
            for (var btn of dQSA('[data-type-sort]')) {
                removeClass(btn, 'active')
            }
            addClass(e.target, 'active')
            sType = e.target.dataset.typeSort
            sResponse({
                val: sInput.value.trim(),
                type: sType
            })
        }
    }

    function sResponse(options) {
        fetch('/search', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(options),
        })
            .then(req => req.text())
            .then(req => {
                sResult.innerHTML = req
            })
    }

    sInput.onclick = (e) => {
        let flag = false

        sInput.oninput = () => {
            flag = true
            sResult.innerHTML = `<div class="center p-20px">
                <img src="/assets/img/loader.gif" style="width:100px; height:100px"></img>
            </div>`

            if (sInput.value.trim() !== '' && sInput.value.trim().length >= 3) {
                addClass(sResult, 'show')
            }
            if (sInput.value.trim() === '' && sInput.value.trim().length < 3) {
                removeClass(sResult, 'show')
            }
        }

        addClass(sPanel, 'show')

        setInterval(function () {
            if (sInput.value.trim() !== '' && sInput.value.trim().length >= 3 && flag == true) {
                sResponse({
                    val: sInput.value.trim(),
                    type: sType
                })
                flag = false
            }
        }, 2000)

        document.onclick = (e) => {
            let target = e.target
            if (
                !hasClass(target, 'menu__nav__search-box__search-input') &&
                !hasClass(target, 'noresult') &&
                !hasClass(target, 'menu__nav__search-box__panel__params') &&
                !hasClass(target, 'sort') &&
                !hasClass(target, 'menu__nav__search-box__panel__result')
            ) {
                removeClass(sPanel, 'show')
            }
        }
    }

})
