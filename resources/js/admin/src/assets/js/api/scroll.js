/ requires
const futureH = require('./futureH.js')

// variables
let $   = futureH.modules.$selectors.list
let doc = futureH.modules.doc

for (let item of $('[data-scroll-class]')) {
    let cl = item.dataset.scrollClass || undefined
    let time = item.dataset.scrollTime || undefined

    if (time === undefined || time === '') {
        time = 300
    }

    function visible(target) {
        let targetPosition = {
            top: window.pageYOffset + target.getBoundingClientRect().top,
            bottom: window.pageYOffset + target.getBoundingClientRect().bottom
        },
        windowPosition = {
            top: window.pageYOffset,
            bottom: window.pageYOffset + document.documentElement.clientHeight
        }

        if (
            targetPosition.bottom > windowPosition.top - 20 &&
            targetPosition.top < windowPosition.bottom - 20
        ) {
            setTimeout(function () {
                item.addClass(cl)
                item.removeClass('opacity-0')
            }, time)
        } else {
            item.removeClass(cl)
            item.addClass('opacity-0')
        }
    }

    window.addEventListener('scroll', function() {
        visible(item)
    })

    visible(item)
}
