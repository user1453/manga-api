export function blackout (html) {
    let blackout = document.querySelector('.blackout')
    blackout.innerHTML = html
    blackout.removeClass('hide')
}

document.querySelector('.blackout').addEventListener('click', (e) => {
    let blackout = e.target

    blackout.addEventListener('click', (event) => {
        let target = event.target

        if (!target.closest('.nonactive-blackout-item')) {
            blackout.addClass('hide')
        }
    })
})
