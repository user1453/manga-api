/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"catalog": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/catalog.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/catalog.sass":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/catalog.sass ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/assets/scss/catalog.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./src/assets/scss/catalog.sass":
/*!**************************************!*\
  !*** ./src/assets/scss/catalog.sass ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js!../../../node_modules/postcss-loader/src??ref--5-3!../../../node_modules/sass-loader/dist/cjs.js!./catalog.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/catalog.sass\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/assets/scss/catalog.sass?");

/***/ }),

/***/ "./src/catalog.js":
/*!************************!*\
  !*** ./src/catalog.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_scss_catalog_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/scss/catalog.sass */ \"./src/assets/scss/catalog.sass\");\n/* harmony import */ var _assets_scss_catalog_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_catalog_sass__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/menu.js */ \"./src/js/menu.js\");\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_menu_js__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nconst log = text => console.log(text);\n\nlet random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;\n\nconst dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet generatorUrl = `/catalog?`;\n\nfunction checker(el) {\n  for (let it of dQSA(`${el} button`)) {\n    it.onclick = e => {\n      let target = e.target;\n      window.location.href = target.dataset.url;\n    };\n  }\n}\n\nclass MangaList {\n  constructor(cl) {\n    this.id = cl;\n  }\n\n  _add(title, imgUrl, url, translater) {\n    let html = `\n        <div class=\"manga-box margin-10px\" data-link=\"${url}\">\n            <div class=\"shadow-dark-big\" style=\"background-image: url('${imgUrl}')\">\n                <span class=\"after\">\n                    <span class=\"shadow-dark\">Читать</span>\n                </span>\n                <span class=\"translater transition-m\">${translater}</span>\n            </div>\n            <h3 class=\"font-14px m-t-10px transition-m\">${title}</h3>\n        </div>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n}\n\nchecker('.main-filter');\nchecker('.help-filter');\nlet urlObj = {\n  cat: 'cat=',\n  statTranslate: 'statTranslate=',\n  form: 'form=',\n  genres: 'genres=',\n  year: 'year=',\n  hide: 'hide='\n};\n\ndQS('#relYear').onchange = () => {\n  urlObj.year += dQS('#relYear').value;\n};\n\nfor (let i = 1; i <= 6; i++) {\n  dQS(`[data-cat=\"${i}\"]`).addEventListener('mousedown', e => {\n    if (urlObj.cat.includes(`${i}_`)) {\n      urlObj.cat = urlObj.cat.replace(`${i}_`, '');\n    } else {\n      urlObj.cat += `${i}_`;\n    } // let index = filter.cat.indexOf(i);\n    //         // if (index > -1) {\n    //         //     filter.cat.splice(index, 1);\n    //         // } else {\n    //         //     filter.cat.push(i)\n    //         // }\n\n  });\n}\n\nfor (let i = 0; i <= 2; i++) {\n  dQS(`[data-statTr=\"${i}\"]`).addEventListener('mousedown', e => {\n    if (urlObj.statTranslate.includes(`${i}_`)) {\n      urlObj.statTranslate = urlObj.statTranslate.replace(`${i}_`, '');\n    } else {\n      urlObj.statTranslate += `${i}_`;\n    }\n  });\n}\n\nfor (let i = 0; i <= 1; i++) {\n  dQS(`[data-hide=\"${i}\"]`).addEventListener('mousedown', e => {\n    if (urlObj.hide.includes(`${i}_`)) {\n      urlObj.hide = urlObj.hide.replace(`${i}_`, '');\n    } else {\n      urlObj.hide += `${i}_`;\n    }\n  });\n}\n\nfor (let i = 0; i <= 5; i++) {\n  dQS(`[data-form=\"${i}\"]`).addEventListener('mousedown', e => {\n    if (urlObj.form.includes(`${i}_`)) {\n      urlObj.form = urlObj.form.replace(`${i}_`, '');\n    } else {\n      urlObj.form += `${i}_`;\n    }\n  });\n}\n\nfor (let i = 1; i <= 42; i++) {\n  dQS(`[data-genres=\"${i}\"]`).addEventListener('mousedown', e => {\n    if (urlObj.genres.includes(`${i}_`)) {\n      urlObj.genres = urlObj.genres.replace(`${i}_`, '');\n    } else {\n      urlObj.genres += `${i}_`;\n    }\n  });\n} // let mangaListRead = new MangaList('manga-read-cont')\n// for (var i = 0; i < 10; i++) {\n//     mangaListRead._add('Городская болезнь', '/assets/img/demo-manga.jpg', '/ereader.html', 'GL Alliance')\n// }\n\n\nfunction select(parent, list) {\n  for (let it of dQSA(parent)) {\n    it.onclick = () => {\n      for (let it2 of dQSA(list)) {\n        if (hasClass(it2, 'show')) {\n          removeClass(it2, 'show');\n        } else {\n          addClass(it2, 'show');\n        }\n      }\n    };\n  }\n\n  for (let it of dQSA(list + ' li')) {\n    it.onmousedown = e => {\n      for (let it2 of dQSA(`${parent} span`)) {\n        it2.innerText = it.innerText;\n      }\n\n      for (let it2 of dQSA(list)) {\n        removeClass(it2, 'show');\n      }\n    };\n  }\n}\n\ndQS('.add-filters').onclick = () => {\n  log(urlObj);\n\n  for (var key in urlObj) {\n    if (urlObj.hasOwnProperty(key)) {\n      if (/\\d/.test(urlObj[key])) {\n        generatorUrl += urlObj[key] + '&';\n      }\n    }\n  }\n\n  window.location = generatorUrl;\n};\n\ndQS('.remove-filters').onclick = () => {\n  window.location = generatorUrl;\n};\n\n//# sourceURL=webpack:///./src/catalog.js?");

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet storage = {};\n\nfor (let it of dQSA('.menu__nav__search-box__panel__params span')) {\n  it.onclick = e => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {\n      removeClass(it2, 'active');\n    }\n\n    storage.searchTypeSort = it.dataset.typeSort;\n    addClass(it, 'active');\n  };\n}\n\nfor (let it of dQSA('.menu__nav__search-box__search-input')) {\n  it.onfocus = () => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel')) {\n      addClass(it2, 'show');\n    }\n  };\n\n  it.oninput = () => {\n    if (it.value.length >= 3) {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        addClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        addClass(it2, 'show');\n      }\n    } else {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        removeClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        removeClass(it2, 'show');\n      }\n    }\n  };\n}\n\ndocument.onmousedown = e => {\n  let target = e.target;\n\n  if (!target.closest('.menu__nav__search-box__panel') && !target.closest('.menu__nav__search-box__search-input')) {\n    for (let it of dQSA('.menu__nav__search-box__panel')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/js/menu.js?");

/***/ })

/******/ });