<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowManga extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $manga = $this->resource;

        return [
            'id' => $manga->id,
            'rusName' => $manga->rus_name,
            'engName' => $manga->en_name,
            'origName' => $manga->origin_name,
            'altName' => $manga->alt_name,
            'description' => $manga->description,
            'cover' => $manga->cover,
            'background' => $manga->background,
            'type' => $manga->type,
            'relDate' => $manga->date_release,
            'author' => $manga->author,
            'painter' => $manga->painter,
            'relHead' => $manga->publisher,
            'genres' => explode(',', $manga->genres),
            'form' => $manga->format,
            'translators' => [],
            'yo' => $manga->age_limit,
            'rate' => round($manga->averageRating, 1),
            'rateCount' => $manga->usersRated()
        ];
    }
}
