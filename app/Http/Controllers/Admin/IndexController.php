<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\RolesCollection;
use App\Http\Resources\StatusResponse;
use App\Http\Resources\TeamsCollection;
use App\Http\Resources\UsersCollection;
use App\Models\Manga;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class IndexController extends Controller
{
    public function getIndex()
    {
        $mangas = Manga::where('certified', false)->get();
        $teams = Team::where('certified', false)->with('delegates')->get();
        $permissions = Permission::all()->pluck('label', 'id');

        return view('admin.index', ['teams' => $teams, 'mangas' => $mangas, 'permissions' => $permissions]);
    }

    public function users()
    {
        return new UsersCollection(User::with('roles')->get());
    }

    public function teams()
    {
        return new TeamsCollection(Team::get());
    }

    public function events(Request $request)
    {
        $authUser = Auth::user();
        $type = $request->get("eventType");
        switch ($type) {
            case 'createRole':
                if ($authUser->can('edit role')) {
                    $title = $request->get('title');
                    if ($title == 'для пользователя') {
                        Role::create(['name' => $request->get('editName'), 'type' => 'user', 'color' => $request->get('editColor')]);
                    }
                    if ($title == 'для команды') {
                        Role::create(['name' => $request->get('editName'), 'type' => 'team', 'color' => $request->get('editColor')]);
                    }
                }
                break;

            case 'editRole':
                if ($authUser->can('edit role')) {

                    $role = Role::findById($request->get('targetId'));
                    if ($request->has('editColor')) {
                        $role->color = $request->get('editColor');
                    }
                    if ($request->has('editName')) {
                        $role->name = $request->get('editName');
                    }
                    if ($request->has('updateFunction')) {

                        foreach ($request->get('updateFunction') as $permissionId => $value) {
                            if ($value == "1") {
                                $permission = Permission::findById($permissionId);
                                $permission->assignRole($role);
                            }
                            if ($value == "0") {
                                $permission = Permission::findById($permissionId);
                                $permission->removeRole($role);
                            }

                        }
                    }
                    $role->save();
                }
                break;
            case 'user':
                $user = User::find($request->get('targetId'));
                if ($authUser->can('edit role')) {
                    if ($request->has('addRole')) {
                        $roles = $request->get('addRole');


                        foreach ($roles as $id) {
                            $role = Role::findById($id);
                            $user->assignRole($role);
                        }
                    }
                    if ($request->has('removeRole')) {
                        $roles = $request->get('removeRole');

                        foreach ($roles as $id) {
                            $role = Role::findById($id);
                            $user->removeRole($role);
                        }
                    }
                }

                if ($authUser->can('issue ban')) {
                    if ($request->has('editStatus')) {
                        $user->status = $request->get('editStatus');
                    }
                }
                if ($request->has('editName')) {
                    $user->login = $request->get('editName');
                }

                $user->save();

                break;

        }

        return new StatusResponse(1);
    }

    public function roles()
    {
        return new RolesCollection(Role::all());
    }
}
