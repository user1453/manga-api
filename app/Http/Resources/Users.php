<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class Users extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $roles = $this->roles->all();
//        dd($roles);

        return [
            'id' => $this->id,
            'name' => $this->login,
            'email' => $this->email,
            'date' => $this->created_at->format('d:m:Y'),
            'roles' => $this->roles->pluck('id'),
            'premium' => 0,
            'status' => $this->status ? 1: 0
        ];
    }
}
