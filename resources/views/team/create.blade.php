@extends('layouts.page')

@section('title', 'Согдание команды')

@section('main')

    <div class="wrapper">
        <div class="content-area">
            <div class="content b-li margin-10px p-10px shadow-dark">



                <!-- добавление команды -->

                 <div class="progress" data-steps-count="6">
                    <span class="complete active"></span>
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="step-1" data-creat-type="team">
                    <span class="font-15px">Название команды</span>
                    <br>
                    <input type="text" class="m-t-10px" id="name">
                    <p class="font-14px error m-t-10px hide" id="nameError">Данное название занято</p>
                </div>
                <div class="step-2 hide">
                    <span class="font-15px">Дата основания</span>
                    <br>
                    <input type="text" class="m-t-10px" placeholder="дд.мм.гггг" id="dateStart">
                </div>
                <div class="step-3 hide">
                    <div class="flex-1">
                        <span class="font-15px">Глава <span class="font-13px"></span> </span>
                        <input type="text" class="m-t-10px font-13px" value="{{\Illuminate\Support\Facades\Auth::user()->login}}" id="headLink">
                    </div>
                    <div class="flex-1 m-t-20px">
                        <span class="font-15px">Представители <span class="font-13px">(без главы)</span>
                        <div class="center">
                            <input type="text" class="m-t-10px font-13px deligator-1" placeholder="Должность" style="width: 110px">
                            <input type="text" class="m-t-10px font-13px deligator-2" placeholder="Логин" style="width: 140px; margin-left: 10px">
                            <button type="button" class="m-t-10px addDeligate">
                                <svg>
                                    <use xlink:href="/assets/img/sprite.svg#add"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div class="delegates m-t-10px font-13px flex-1"></div>
                </div>
                <div class="step-4 hide">
                    <div class="flex-1">
                        <span class="font-15px">Discord</span>
                        <input type="text" class="m-t-10px font-13px" placeholder="инвайт-ссылка" id="discordLink">
                    </div>
                    <div class="flex-1 m-t-20px">
                        <span class="font-15px">ВКонтакте</span>
                        <input type="text" class="m-t-10px font-13px" id="vkLink">
                    </div>
                </div>
                <div class="step-5 hide">
                    <span class="font-15px">Описание</span>
                    <textarea class="m-t-20px" id="description"></textarea>
                </div>
                <div class="step-6 hide">
                    <p class="font-18px bold center m-b-10px status-send">Отправляется...</p>
                    <div class="center m-t-20px loader-send">
                        <img src="/assets/img/loader.gif" style="height: 60px">
                    </div>
                    <div class="center m-t-20px">
                        <a href="/" class="b-fd c-li p-10px m-t-20px">На главную</a>
                    </div>
                </div>
                <nav>
                    <button class="prev-step hide">Назад</button>
                    <button class="next-step">Далее</button>
                </nav>
            </div>
        </div>
    </div>
    <div class="left-0 right-0 top-0 bottom-0 substrate fixed hide">
        <div class="absolute left-0 right-0 top-0 bottom-0 center">
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="sign-in">
                <input type="text" placeholder="Логин или e-mail" class="m-t-10px" data-auth-input="log">
                <br>
                <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-input="pass">
                <br>
                <div class="m-t-10px">
                    <span class="error font-13px m-t-10px hide" data-auth-check="log">Неверный логин</span>
                    <span class="error font-13px m-t-10px hide" data-auth-check="pass">Неверный пароль</span>
                </div>
                <div class="btn-box">
                    <button type="button" class="btn-stand font-14px m-t-10px send-form">Вход</button>
                    <div class="font-14px m-t-10px">
                        <div class="checkbox">
                            <span class="input"></span>
                            <span class="text">запомнить меня</span>
                        </div>
                    </div>
                </div>
                <div class="center">
                    <hr>
                </div>
                <div class="btn-box">
                    <button type="button" class="btn-help font-13px m-t-10px" data-auth="recovery">Забыли пароль?</button>
                    <button type="button" class="btn-help font-13px m-t-10px" data-auth="reg">Регистрация</button>
                </div>
            </div>
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="reg">
                <div class="center">
                    <h2>Регистрация</h2>
                </div>
                <div class="m-t-20px">
                    <div>
                        <input type="text" placeholder="Логин" class="m-t-20px" data-auth-check="login"  data-auth-input="log">
                        <span class="error font-13px m-t-10px hide" data-auth-check="login">Упс! Такой уже существует</span>
                    </div>
                    <div>
                        <input type="text" placeholder="E-mail" class="m-t-10px" data-auth-check="e-mail" data-auth-input="e-mail">
                        <span class="error font-13px m-t-10px hide" data-auth-check="e-mail">Упс! Такой уже существует</span>
                    </div>
                    <div>
                        <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-check="pass"  data-auth-input="pass">
                        <span class="error font-13px m-t-10px hide" data-auth-check="pass">Пароль может содержать только латиницу, цифры и _</span>
                    </div>
                    <div>
                        <input type="text" placeholder="Повторите пароль" class="m-t-10px" data-auth-check="pass-check"  data-auth-input="pass-check">
                        <span class="error font-13px m-t-10px hide" data-auth-check="pass-check">Пароли не совпадают</span>
                    </div>
                    <div class="btn-box m-t-20px">
                        <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Регистрация</button>
                        <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="sign-in">У меня есть аккаунт</button>
                    </div>
                </div>
            </div>
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="recovery">
                <div class="center">
                    <h2 class="font-18px">Восстановление пароля</h2>
                </div>
                <div class="m-t-20px rec-w">
                    <div>
                        <input type="text" placeholder="Логин или e-mail" class="m-t-20px" data-auth-input="log" data-auth-check="login-rec">
                        <span class="error font-13px m-t-10px hide" data-auth-check="login-rec">Пользователь не найден</span>
                    </div>
                    <div class="btn-box m-t-20px">
                        <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Восстановить</button>
                        <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="reg">Регистрация</button>
                    </div>
                </div>
                <div class="rec-c hide">
                    <p class="c-se font-14px m-t-20px text-center">Письмо с инструкциями отправлено на ваш электронный адрес</p>
                </div>
            </div>
        </div>
    </div>

@stop

@push('css')

    <link rel="stylesheet" href="/assets/css/create.css">
@endpush

@push('scripts')

    <script src="/assets/js/create.js" charset="utf-8"></script>
@endpush
