<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Roles extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->name,
            'type' => $this->type,
            'color' => $this->color,
            'functions' => $this->permissions->pluck('id')
        ];
    }
}
