import './assets/scss/manga.sass'
import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => el.classList.add(el2),
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)

function select (parent, list) {
    for (let it of dQSA(parent)) {
        it.onclick = () => {
            for (let it2 of dQSA(list)) {
                if (hasClass(it2, 'show')) {
                    removeClass(it2, 'show')
                } else {
                    addClass(it2, 'show')
                }
            }
        }
    }
    for (let it of dQSA(list + ' li')) {
        it.onmousedown = (e) => {
            let response = fetch('/bookmark/store', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    list: it.dataset.list,
                    name: dQS('#idManga').innerText.trim()
                }),
            })
            response
            .then(req => req.json())
            .then(req => {

            })
            for (let it2 of dQSA(`${parent} span`)) {
                it2.innerText = it.innerText
            }
            for (let it2 of dQSA(list)) {
                removeClass(it2, 'show')
            }
        }
    }
}

select('.read-status', '.status-list')

for (let it of dQSA('[data-rating]')) {
    it.onmouseover = (e) => {
        for (let it of dQSA('[data-rating]')) {
            removeClass(it, 'hover-rating')
        }
        for (let i = 1; i <= it.dataset.rating; i++) {
            addClass(dQSA(`[data-rating="${i}"]`)[0], 'hover-rating')
        }
    }
    it.onclick = (e) => {
        let rating = it.dataset.rating
        let response = fetch('/manga/rate', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                rating: rating,
                name: dQS('#idManga').innerText.trim()
            }),
        })
        response
        .then(req => req.json())
        .then(req => {
            // console.log(req);
        })
    }
}


document.onclick = (e) => {
    let target = e.target
    if (!target.closest('.read-status')) {
        for (let it2 of dQSA('.status-list')) {
            removeClass(it2, 'show')
        }
    }
}

document.onmouseover = (e) => {
    let target = e.target
    if (!target.closest('.rating')) {
        for (let it of dQSA('[data-rating]')) {
            removeClass(it, 'hover-rating')
        }
    }
}

//
