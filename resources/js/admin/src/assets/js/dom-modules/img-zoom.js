const { blackout } = require('./blackout')

export function imgZoom (selector) {
    for (let item of document.querySelectorAll(`.${selector}`)) {
        item.addEventListener('click', (e) => {
            blackout(item.outerHTML)
        })
    }
}
