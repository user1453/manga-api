<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gl Alliance admin panel</title>
    <link rel="stylesheet" href="/adm/assets/css/index.css">
</head>
<body>
<script>
    window.roleFunctions = @json($permissions)
</script>

<div class="wrap">
    <div class="cnt">
        <div class="tab s-tab">
            <div class="tab__btn-grp s-btn-grp" cl="tab__btn_active">
                <button class="tab__btn s-btn tab__btn_active" tab-link="0">Заявки команд</button>
                <button class="tab__btn s-btn" tab-link="1">Заявки манг</button>
                <button class="tab__btn s-btn" tab-link="2">Роли пользователей</button>
                <button class="tab__btn s-btn" tab-link="3">Роли команд</button>
                <button class="tab__btn s-btn" tab-link="4">Пользователи</button>
                <div class="s-btn-grp__roller"></div>
            </div>
            <div class="tab__view">
                <div tab-cnt="0" tab-cnt-cl="fade-in hide">


                    @foreach($teams as $team)
                        <div class="plate" application-team-id="{{$team->id}}">
                            <div class="plate__info">
                                <div class="plate__info-item">
                                    <span>Название</span>
                                    {{$team->name}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Дата основания</span>
                                    {{$team->date_start}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Глава</span>
                                    <a href="{{route('profile.page', ['id' =>$team->leader()->user->id])}}">{{$team->leader()->user->login}}</a>
                                </div>
                                <div class="plate__info-item">
                                    <span>Представители</span>
                                    @foreach($team->delegates as $delegate)
                                        <a href="{{route('profile.page', ['id' =>$delegate->user->id])}}">{{$delegate->user->login}}</a>
                                        ({{$delegate->position}})
                                    @endforeach
                                </div>
                                <div class="plate__info-item">
                                    <span>Discord</span>
                                    <a href="{{$team->doscord}}">{{$team->doscord}}</a>
                                </div>
                                <div class="plate__info-item">
                                    <span>ВКонтакте</span>
                                    <a href="{{$team->vk}}">{{$team->vk}}</a>
                                </div>
                                <div class="plate__info-item">
                                    <span>Описание</span>
                                    {{$team->description}}
                                </div>
                                <div class="plate__nav margin-top-20px f ai-c jc-fs">
                                    <button type="button" class="btn btn_green margin-right-20px application-yes">
                                        Одобрить
                                    </button>
                                    <button type="button" action="/admin/team/{{$team->id}}/failure"
                                            class="btn btn_red application-no">Отклонить
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div tab-cnt="1" tab-cnt-cl="fade-in hide" class="hide">


                    @foreach($mangas as $manga)
                        <div class="plate" application-manga-id="{{$manga->id}}">
                            <div class="plate__info">
                                <div class="plate__info-item">
                                    <span>Оригинальное название</span>
                                    {{$manga->origin_name}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Русское название</span>
                                    {{$manga->rus_name}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Английское название</span>
                                    {{$manga->en_name}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Альтернативные название</span>
                                    {{$manga->alt_name}}
                                </div>
                                <div class="plate__info-item">
                                    <span>Обложка</span>
                                    <br>
                                    <img src="{{$manga->cover}}" class="s-img-zoom nonactive-blackout-item">
                                </div>
                                <div class="plate__info-item">
                                    <span>Фон на странице</span>
                                    <br>
                                    <img src="{{$manga->background}}" class="s-img-zoom nonactive-blackout-item">
                                </div>
                                <div class="plate__info-item">
                                    <span>Описание</span>
                                    {{ $manga->description }}
                                </div>
                                <div class="plate__nav margin-top-20px f ai-c jc-fs">
                                    <button type="button" class="btn btn_green margin-right-20px application-yes">
                                        Одобрить
                                    </button>
                                    <button type="button" action="/admin/manga/{{$manga->id}}/failure"
                                            class="btn btn_red application-no">Отклонить
                                    </button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div tab-cnt="2" tab-cnt-cl="fade-in hide" tab-cnt-show-fn="loadUserRoles" class="hide">
                    <div class="plate__nav f ai-c jc-fs margin-top-20px">
                        <button type="button" class="btn btn_brand" open-spoiler="settings-window" id="addUserRole">
                            Добавить
                        </button>
                    </div>
                    <div class="plate" role-box></div>
                </div>
                <div tab-cnt="3" tab-cnt-cl="fade-in hide" tab-cnt-show-fn="loadTeamRoles" class="hide">
                    <div class="plate__nav margin-top-20px f ai-c jc-fs">
                        <button type="button" class="btn btn_brand" open-spoiler="settings-window" id="addTeamRole">
                            Добавить
                        </button>
                    </div>
                    <div class="plate" role-box></div>
                </div>
                <div tab-cnt="4" tab-cnt-cl="fade-in hide" tab-cnt-show-fn="loadUsers" class="hide" user-box></div>
                <div class="plate hide fullscreen-abs-box settings" spoiler="settings-window" cl-show="fade-in-down"
                     cl-hide="fade-out-down hide~0.3">
                    <div class="plate__nav f ai-c jc-sb">
                        <p class="settings__title">
                            <span class="settings__title-primary"></span>
                            <span class="settings__title-secondary"></span>
                        </p>
                        <button class="btn btn_back-link close_cross" close-spoiler="settings-window"></button>
                    </div>
                    <div class="col-2 row-1">
                        <div class="settings__functional-box">

                        </div>
                        <div class="settings__view-box">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blackout hide"></div>

<script src="/adm/assets/js/index.js" charset="utf-8"></script>
<script src="/adm/assets/js/vendors.js" charset="utf-8"></script>
</body>
</html>
