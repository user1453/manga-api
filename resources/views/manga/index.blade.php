@extends('layouts.page')

@section('title', $manga->rus_name)

@section('main')

    <div class="cover" style="background-image: url({{$manga->background}});"></div>
    <div class="wrapper">
        <div class="content-area m-t--">
            <div class="column">
                <main class="p-10px margin-10px shadow-dark">
                    <div class="rating absolute">
                        <div class="stars">
                            @for($i = 1 ; $i <= 5; $i++)
                                <svg class="folder transition-m {{$i <= $manga->averageRating() ? 'user-rating': '' }}"
                                     data-rating="{{$i}}">
                                    <use xlink:href="/assets/img/sprite.svg#star"></use>
                                </svg>
                            @endfor
                        </div>
                        <div class="font-20px bold">
                            {{ round($manga->averageRating(), 1)}}
                        </div>
                        <span class="font-13px c-se right">Голосов: {{$manga->usersRated()}}</span>
                    </div>
                    <section class="m-flex-block">
                        <div class="img" style="background-image: url({{$manga->cover}})"></div>
                        @if($firstPart != null)
                            <button type="button" class="start-reading transition-m"
                                    data-link="/ereader?id={{$manga->id}}&part={{$firstPart->number}}&page={{1}}&translator={{$firstPart->user->login}}">
                                Начать читать
                            </button>
                            <br>
                        @endif
                    <!-- в ТЗ указано для кого эта кнопка появляется -->
                        @auth
                            @can('add_part')
                                <button type="button" class="start-reading transition-m"
                                        data-link="{{route('manga.addparts',['id' => $manga->id])}}">Добавить главы
                                </button>
                                <br>
                            @endcan                            @can('edit manga')
                                <button type="button" class="start-reading transition-m"
                                        data-link="#">Редактировать
                                </button>
                                <br>
                            @endcan
                            <div class="select read-status transition-m">
                                <svg class="folder transition-m">
                                    <use xlink:href="/assets/img/sprite.svg#folder"></use>
                                </svg>
                                <span class="font-14px" id="idList">Добавить в список</span>
                                <svg class="arr">
                                    <use xlink:href="/assets/img/sprite.svg#exArr"></use>
                                </svg>
                                <ul class="simple-ul status-list absolute left-0 right-0 shadow-dark transition-m">
                                    <li class="blue" data-list="plans">В планах</li>
                                    <li class="grey" data-list="fors">Брошено</li>
                                    <li class="green" data-list="read">Прочитано</li>
                                    <li class="red" data-list="like">Любимое</li>
                                </ul>
                            </div>

                        @endauth
                    </section>
                    <section class="m-flex-block">
                        <h1>
                            <span class="m-r-10px" id="idManga">{{$manga->rus_name}}</span>
                            @if($manga->tags)
                                @foreach(explode( ',', $manga->tags) as $tag )
                                    <span class="tag-red font-11px">{{$tag}}</span>
                                @endforeach
                            @endif
                        </h1>
                        <h2>{{$manga->origin_name}} / {{$manga->en_name}}</h2>
                        <div class="info p-10px">
                            <div>
                                <span class="bold">В закладках у</span>
                                <span class="c-fd font-16px">{{$bookmarksCount}}</span>
                            </div>
                            <div>
                                <span class="bold">Тип</span>
                                <span data-global-link="">{{$manga->type}}</span>
                            </div>
                            <div>
                                <span class="bold">Автор</span>
                                <span data-global-link="">{{$manga->author}}</span>
                            </div>
                            <div>
                                <span class="bold">Издатель</span>
                                <span data-global-link="">{{$manga->publisher}}</span>
                            </div>
                            <div>
                                <span class="bold">Перевод</span>
                                <span class="tag-blue font-13px">{{$manga->translate_status}}</span>
                            </div>
                            <div>
                                <span class="bold">Просмотров</span>
                                <span>{{$manga->views}}</span>
                            </div>
                            <div>
                                <span class="bold">Дата релиза</span>
                                <span>{{$manga->date_release}}</span>
                            </div>
                            <div>
                                <span class="bold">Жанры</span>
                                <span data-global-link="">{{$manga->genres}}</span>
                            </div>
                            <div>
                                <span class="bold">Переводчики</span>
                                <span>{{$translators}}</span>
                            </div>
                            <div>
                                <span class="bold">Другие названия</span>
                                <span>{{$manga->alt_name}}</span>
                            </div>
                            <div>
                                <span class="bold">Описание</span>
                                <br>
                                <span class="description font-14px">{{$manga->description}}</span>
                            </div>
                        </div>
                    </section>
                </main>
                <div class="manga-list p-10px margin-10px shadow-dark b-li">
                    <p class="font-16px c-fd">Список глав</p>
                    <table>
                        <tbody>
                        @foreach($parts as $part)
                            <tr>
                                <td class="hover-color-fd"><a
                                        href="{{$part->url()}}">Том {{$part->vol}}
                                        . Глава {{$part->number}} - {{$part->name}}</a></td>
                                <td class="hover-color-fd"><a
                                        href="{{route('profile.page', ['id' => $part->user->id])}}">{{$part->user->login}}</a>
                                </td>
                                <td>{{$part->created_at}}</td>
                                <td>
                                    <svg>
                                        <use xlink:href="/assets/img/sprite.svg#edit"></use>
                                    </svg>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @comments([
                'model' => $manga,
                {{--                'perPage' => 2--}}
                ])

            </div>
            <div class="column margin-10px">
                <div class="list-manga b-li shadow-dark p-t-10px">
                    <p class="font-16px margin-10px m-t-0">Вам может понравиться</p>
                    <div class="list p-10px p-t--">
                        @foreach( $recommends as $item)
                            <div class="line m-t-10px transition-m"
                                 onclick="location.href='{{route('manga.page',['id' => $item->id])}}'">
                                <div class="cover" style="background-image: url({{$item->cover}})"></div>
                                <div>
                                    <p class="font-15px bold">{{$item->rus_name}}</p>
                                    <span class="font-13px">{{$item->en_name}}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@push('css')

    <link rel="stylesheet" href="/assets/css/manga.css">
@endpush

@push('scripts')

    <script src="/assets/js/manga.js" charset="utf-8"></script>
@endpush
