<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMangasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mangas', function (Blueprint $table) {
            $table->id();
            $table->string('rus_name')->nullable();
            $table->string('en_name')->nullable();
            $table->string('origin_name')->nullable();
            $table->string('alt_name')->nullable();
            $table->string('description')->nullable();
            $table->string('date_release')->nullable();
            $table->string('author');
            $table->string('painter');
            $table->string('publisher');
            $table->string('type');
            $table->string('genres');
            $table->string('format');
            $table->string('age_limit');
            $table->string('translate_status');
            $table->string('cover')->default('');
            $table->string('background')->default('');
            $table->integer('views')->default(0);
            $table->boolean('certified')->default(false);
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mangas');
    }
}
