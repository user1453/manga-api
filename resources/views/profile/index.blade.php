@extends('layouts.page')

@section('title', 'teikei.me')

@section('main')

    <div class="wrapper">
        <div class="content-area">
            <header class="shadow-dark p-10px">
                <!-- Это не трогать, это на будущее -->
                <!-- <span class="online font-15px"><span>online</span></span> -->
                <div class="img" style="background-image: url({{asset('assets/img/coder-ava.png')}})"></div>
                <div class="main-info p-10px">
                    <h1>
                        <span class="name">{{$user->login}}</span>
                        @foreach($user->roles as $role)
                            <style>.role-{{$role->id}}{ color: {{$role->color}} ; border-color: {{$role->color}}}
                                .role-{{$role->id}}::after {
                                 background: {{$role->color}} }
                            </style>
                            <span class="role role-{{$role->id}}">{{$role->name}}</span>
                        @endforeach
                    </h1>
                    <div class="info">
                        <table>
                            <tbody>
                            <tr>
                                <td class="first">Дата регистрации</td>
                                <td>{{$user->created_at->format('d:m:Y')}}</td>
                            </tr>
                            @if($user->gender != 'unknown')
                                <tr>
                                    <td class="first">Пол</td>
                                    <td>{{$user->gender}}</td>
                                </tr>
                            @endif
                            @if($user->city != 'unknown')
                                <tr>
                                    <td class="first">Город</td>
                                    <td>{{$user->city}}</td>
                                </tr>
                            @endif
                            <tr>
                                <td class="first">Премиум</td>
                                <td>
                                    {{$user->premium->implode('name', ', ')}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </header>
            <div class="lists shadow-dark p-10px b-li m-t-10px">
                <nav>
                    <button type="button" class="lists-sort shadow-dark"
                            data-link="{{route('profile.page', ['id' => $user->id, 'list' => 'i-read'])}}"
                            style="background-image: url(/assets/img/demo-manga.jpg)">
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#book"></use>
                        </svg>
                        <span>Читаю</span>
                    </button>
                    <button type="button" class="lists-sort shadow-dark"
                            data-link="{{route('profile.page', ['id' => $user->id, 'list' => 'like'])}}"
                            style="background-image: url(/assets/img/demo-manga.jpg)">
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#bookLove"></use>
                        </svg>
                        <span>Рекомендует</span>
                    </button>
                    <button type="button" class="lists-sort shadow-dark"
                            data-link="{{route('profile.page', ['id' => $user->id, 'list' => 'plans'])}}"
                            style="background-image: url(/assets/img/demo-manga.jpg)">
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#list"></use>
                        </svg>
                        <span>В планах</span>
                    </button>
                    <button type="button" class="lists-sort shadow-dark"
                            data-link="{{route('profile.page', ['id' => $user->id, 'list' => 'read'])}}"
                            style="background-image: url(/assets/img/demo-manga.jpg)">
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#bookPlus"></use>
                        </svg>
                        <span>Прочитано</span>
                    </button>
                    <button type="button" class="lists-sort shadow-dark"
                            data-link="{{route('profile.page', ['id' => $user->id, 'list' => 'fors'])}}"
                            style="background-image: url(/assets/img/demo-manga.jpg)">
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#trashcan"></use>
                        </svg>
                        <span>Брошено</span>
                    </button>
                </nav>
                <div class="m-t-10px center manga-read-cont">
                    @foreach($bookmarks as $bookmark)
                        <div class="manga-box margin-10px"
                             data-link="{{route('manga.page',['id' => $bookmark->manga->id ])}}">
                            <div class="shadow-dark-big" style="background-image: url({{$bookmark->manga->cover}})">
                            <span class="after">
                                <span class="shadow-dark">Читать</span>
                            </span>
                                <span
                                    class="translater transition-m">{{\App\Models\Manga::translators($bookmark->manga->id)}}</span>
                            </div>
                            <h3 class="font-14px m-t-10px transition-m">{{$bookmark->manga->rus_name}}</h3>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')

    <link rel="stylesheet" href="/assets/css/profile.css">
@endpush

@push('scripts')

    <script src="/assets/js/profile.js" charset="utf-8"></script>
@endpush
