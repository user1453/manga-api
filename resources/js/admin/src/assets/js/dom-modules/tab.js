const { storage } = require('../api/storage')

let prefix = storage.system.classPrefix

export function tab (options = null) {
    for (let item of document.querySelectorAll(`.${prefix}tab`)) {
        for (let btn of item.querySelectorAll(`[tab-link]`)) {
            let link = btn.attr('tab-link') || undefined

            if (link !== undefined && link !== '') {
                btn.addEventListener('click', (e) => {
                    for (let cntac of item.querySelectorAll(`[tab-cnt="${link}"]`)) {
                        let cl = cntac.attr('tab-cnt-cl') || undefined
                        let fns = cntac.attr('tab-cnt-show-fn') || undefined

                        try {
                            for (let cnt of item.querySelectorAll(`[tab-cnt]`)) {
                                cnt.removeClass(cnt.attr('tab-cnt-cl').split(' ')[0])
                                cnt.addClass(cnt.attr('tab-cnt-cl').split(' ')[1])
                            }
                            cntac.removeClass(cl.split(' ')[1])
                            cntac.addClass(cl.split(' ')[0])
                            try {
                                if (typeof options.always === 'function') {
                                    options.always(cntac)
                                }
                                if (fns !== undefined && fns !== '') {
                                    for (let fn of fns.split(' ')) {
                                        options[fn](cntac)
                                    }
                                }
                            } catch (e) {
                                storage.console.grpCollapsed(
                                    () => {
                                        console.error(e)
                                    },
                                    '[Ошибка] модуль tab',
                                    `${storage.console.statusCSS.err}`
                                 )
                            }
                        } catch {
                            storage.console.grpCollapsed(
                                () => {
                                    console.error('Не определен атрибут tab-cnt-cl')
                                    console.warn('Используйте атрибут tab-cnt-cl="<class show> <class hide>"')
                                },
                                '[Ошибка] модуль tab',
                                `${storage.console.statusCSS.err}`
                             )
                        }
                    }
                })
            }
        }
    }
}
