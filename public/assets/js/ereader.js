/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"ereader": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/ereader.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/ereader.sass":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/ereader.sass ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/assets/scss/ereader.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./src/assets/scss/ereader.sass":
/*!**************************************!*\
  !*** ./src/assets/scss/ereader.sass ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js!../../../node_modules/postcss-loader/src??ref--5-3!../../../node_modules/sass-loader/dist/cjs.js!./ereader.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/ereader.sass\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/assets/scss/ereader.sass?");

/***/ }),

/***/ "./src/ereader.js":
/*!************************!*\
  !*** ./src/ereader.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_scss_ereader_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/scss/ereader.sass */ \"./src/assets/scss/ereader.sass\");\n/* harmony import */ var _assets_scss_ereader_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_ereader_sass__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/menu.js */ \"./src/js/menu.js\");\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_menu_js__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nconst log = text => console.log(text);\n\nlet random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;\n\nconst dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => {\n  if (el != undefined) {\n    el.classList.add(el2);\n  }\n},\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nfor (let it of document.querySelectorAll('[data-link]')) {\n  it.onclick = e => {\n    document.location.href = it.dataset.link;\n  };\n}\n\ndocument.addEventListener(\"DOMContentLoaded\", function () {\n  setInterval(function () {\n    for (let it of document.querySelectorAll('[data-link]')) {\n      it.onclick = e => {\n        document.location.href = it.dataset.link;\n      };\n    }\n  }, 1000);\n});\n\nclass MangaList {\n  constructor(cl) {\n    this.id = cl;\n  }\n\n  _addItemSimple(title, url) {\n    let html = `\n        <li class=\"p-10px c-fi2 b-r-10px font-13px normal transition-m\" data-link=\"${url}\">${title}</li>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemSimplePage(title, url) {\n    let html = `\n        <li class=\"p-10px c-fi2 b-r-10px font-13px normal transition-m\" data-link=\"${url}\">${title}</li>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n}\n\nfunction select(parent, list) {\n  for (let it of dQSA(parent)) {\n    it.onclick = () => {\n      for (let it2 of dQSA(list)) {\n        addClass(it2, 'show');\n      }\n    };\n  }\n\n  for (let it of dQSA(list + ' li')) {\n    it.onmousedown = e => {\n      for (let it2 of dQSA(`${parent} span`)) {\n        it2.innerText = it.innerText.split(\":\")[0];\n      }\n\n      for (let it2 of dQSA(list)) {\n        removeClass(it2, 'show');\n      }\n    };\n  }\n}\n\nfunction modal(modal, button) {\n  dQS(button).onclick = e => {\n    addClass(dQS(`.substrate`), 'show');\n    addClass(dQS(modal), 'show');\n  };\n\n  dQS(`.substrate`).onclick = e => {\n    removeClass(e.target, 'show');\n\n    for (let it of e.target.querySelectorAll('div')) {\n      removeClass(it, 'show');\n    }\n  };\n}\n\nselect('.select-ereader', '.ereader-list');\nselect('.select-ereader-page', '.ereader-page-list');\nselect('.select-ereader-mode', '.ereader-mode-list');\nmodal('#ereaderSettingsModal', '#ereaderSettingsBtn');\n\nif (localStorage.getItem('ereaderstatus') === null) {\n  localStorage.setItem('eReaderSizeImg', 'normal');\n  localStorage.setItem('eReaderZip', 'origin');\n  localStorage.setItem('eReaderTheme', 'dark');\n  localStorage.setItem('eReaderType', 'horis');\n  localStorage.setItem('ereaderstatus', 'ok');\n}\n\nfunction eReaderRadioChecked(name) {\n  for (let it of dQSA(`[name=\"${name}\"]`)) {\n    it.checked = false;\n  }\n}\n\nfunction eReaderSizeImg() {\n  let m = localStorage.getItem('eReaderSizeImg');\n  let css = '';\n  let dheight = document.documentElement.offsetHeight - 60;\n  eReaderRadioChecked('eReaderSizeImg');\n\n  switch (m) {\n    case 'full':\n      css += `.ereader-box .ereader-img-box img {width: 100%;height: auto;}`;\n      break;\n\n    case 'normal':\n      css += `.ereader-box .ereader-img-box img {width: auto;height: auto;}`;\n      break;\n\n    case 'height':\n      css += `.ereader-box .ereader-img-box img {width: auto;height: ${dheight}px;}`;\n      break;\n  }\n\n  let head = document.getElementsByTagName('head')[0];\n  let s = document.createElement('style');\n  s.innerText = css;\n  head.appendChild(s);\n  dGetId(`${m}`).checked = true;\n}\n\nfunction eReaderZip() {\n  let m = localStorage.getItem('eReaderZip');\n  eReaderRadioChecked('eReaderZip');\n\n  switch (m) {\n    case 'origin':\n      break;\n\n    case 'ziped':\n      break;\n  }\n\n  dGetId(`${m}`).checked = true;\n}\n\nfunction eReaderTheme() {\n  let m = localStorage.getItem('eReaderTheme');\n  let css = '';\n  eReaderRadioChecked('eReaderTheme');\n\n  switch (m) {\n    case 'light':\n      css += `\n                p, span, div, button {\n                    color: #fff !important\n                }\n                button {\n                    background: rgba(0,0,0,.1)\n                }\n                button:hover {\n                    background: rgba(0,0,0,.05)\n                }\n                header {\n                    background: #00c0ff\n                }\n                .ereader-box {\n                    background: #eee\n                }\n                .select {\n                    background: rgba(0,0,0,.1)\n                }\n                .select:hover {\n                    background: rgba(0,0,0,.05)\n                }\n                .addBookmark svg {\n                    fill: #fff\n                }\n                .addBookmark:hover svg {\n                    fill: #eee\n                }\n                .select ul {\n                    background: #fff;\n                }\n                .select li {\n                    color: #4d4d4d;\n                }\n                .select svg {\n                    fill: #fff;\n                }\n                #ereaderSettingsModal {\n                    background: #fff;\n                }\n                #ereaderSettingsModal h3 {\n                    background: #00c0ff;\n                    font-weight: bold;\n                    color: #fff;\n                }\n                #ereaderSettingsModal * {\n                    color: #00c0ff;\n                }\n                #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {\n                    background: #fff;\n                }\n                #ereaderSettingsModal .radio .radio_label::before {\n                    background: #eee;\n                }\n            `;\n      break;\n\n    case 'dark':\n      css += `\n            p, span, div, button {\n                color: #fff !important\n            }\n            button {\n                background: rgba(255,255,255,.1)\n            }\n            button:hover {\n                background: rgba(255,255,255,.05)\n            }\n            header {\n                background: rgb(39, 39, 39);\n            }\n            .ereader-box {\n                background: rgb(29, 29, 29);\n            }\n            .select {\n                background: rgba(255,255,255,.1)\n            }\n            .select:hover {\n                background: rgba(255,255,255,.05)\n            }\n            .addBookmark svg {\n                fill: #fff\n            }\n            .addBookmark:hover svg {\n                fill: #00c0ff\n            }\n            .select ul {\n                background: rgb(39, 39, 39);\n            }\n            .select li {\n                color: rgb(238, 238, 238);\n            }\n\n            .select li:hover {\n                background: rgba(0,0,0,.05)\n            }\n            .select svg {\n                fill: #fff;\n            }\n            #ereaderSettingsModal {\n                background: #fff;\n                background: rgb(26, 26, 26);\n            }\n            #ereaderSettingsModal h3 {\n                background: rgb(45, 45, 45);\n                font-weight: normal;\n            }\n            #ereaderSettingsModal * {\n                color: rgb(225, 225, 225);\n            }\n            #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {\n                background: #4d4d4d;\n            }\n            #ereaderSettingsModal .radio .radio_label::before {\n                background: rgb(77, 77, 77);\n            }\n        `;\n      break;\n\n    case 'oled':\n      css += `\n            p, span, div, button {\n                color: #fff !important\n            }\n            button {\n                background: rgba(255,255,255,.1)\n            }\n            button:hover {\n                background: rgba(255,255,255,.05)\n            }\n            header {\n                background: #000;\n            }\n            .ereader-box {\n                background: #000;\n            }\n            .select {\n                background: rgba(255,255,255,.1)\n            }\n            .select:hover {\n                background: rgba(255,255,255,.05)\n            }\n            .addBookmark svg {\n                fill: #fff\n            }\n            .addBookmark:hover svg {\n                fill: #00c0ff\n            }\n            .select ul {\n                background: #000;\n            }\n            .select li {\n                color: rgb(238, 238, 238);\n            }\n\n            .select li:hover {\n                background: rgba(0,0,0,.05)\n            }\n            .select svg {\n                fill: #fff;\n            }\n            #ereaderSettingsModal {\n                background: #fff;\n                background: #000;\n            }\n            #ereaderSettingsModal h3 {\n                background: #000;\n                font-weight: normal;\n            }\n            #ereaderSettingsModal * {\n                color: rgb(225, 225, 225);\n            }\n            #ereaderSettingsModal .radio .radio_input:checked + .radio_label::after {\n                background: #4d4d4d;\n            }\n            #ereaderSettingsModal .radio .radio_label::before {\n                background: rgb(77, 77, 77);\n            }\n        `;\n      break;\n  }\n\n  let head = document.getElementsByTagName('head')[0];\n  let s = document.createElement('style');\n  s.innerText = css;\n  head.appendChild(s);\n  dGetId(`${m}`).checked = true;\n}\n\neReaderSizeImg();\neReaderZip();\neReaderTheme();\n\nfor (let it of dQSA(`[name=\"eReaderTheme\"], [name=\"eReaderZip\"], [name=\"eReaderSizeImg\"]`)) {\n  it.onchange = e => {\n    localStorage[e.target.name] = it.id;\n    eReaderSizeImg();\n    eReaderZip();\n    eReaderTheme();\n  };\n}\n\nlet searchUrl = document.location.search;\nsearchUrl = searchUrl.replace('?', '').split('&');\nlet searchs = {};\nsearchUrl.map((item, i, arr) => {\n  searchs[item.split('=')[0]] = item.split('=')[1];\n});\n\ndQS('.addBookmark').onclick = () => {\n  let addSimpleList = fetch('/bookmark/store', {\n    method: 'POST',\n    mode: \"cors\",\n    headers: {\n      'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n      'Content-Type': 'application/json'\n    },\n    body: JSON.stringify({\n      id: parseInt(searchs.id)\n    })\n  }).then(req => alert('Добавлено в закладки'));\n};\n\nlet addSimpleList = fetch(dQS('[name=\"parts-url\"]').getAttribute('content'), {\n  method: 'POST',\n  mode: \"cors\",\n  headers: {\n    'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content')\n  }\n});\naddSimpleList.then(req => req.json()).then(req => {\n  let simpleList = new MangaList('ereader-list');\n  localStorage.setItem('partsCount', 3);\n\n  for (var item of req) {\n    localStorage.setItem('partsCount', Number(localStorage.getItem('partsCount')) + 1);\n\n    simpleList._addItemSimple(`Том ${item.vol} - Глава ${item.part} : ${item.name}`, item.url);\n  }\n});\nlet addSimpleListPage = fetch(dQS('[name=\"pages-url\"]').getAttribute('content'), {\n  method: 'POST',\n  mode: \"cors\",\n  headers: {\n    'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content')\n  }\n});\naddSimpleListPage.then(req => req.json()).then(req => {\n  let simpleListPage = new MangaList('ereader-page-list');\n  localStorage.setItem('pageCount', req);\n\n  for (let i = 1; i <= req; i++) {\n    simpleListPage._addItemSimple(`Страница ${i}`, `/ereader?id=${searchs.id}&part=${searchs.part}&page=${String(i).padStart(3, '0')}&translator=${searchs.translator}`);\n  }\n});\n\nif (localStorage.getItem('eReaderType') == 'horis') {\n  if (document.location.href.includes('&typeRead=true')) {\n    document.location.href = document.location.href.replace('&typeRead=true', '');\n  }\n\n  for (let it of dQSA(`.next`)) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"next-page-url\"]').getAttribute('content');\n    };\n  }\n\n  for (let it of dQSA('[class*=\"id-\"]')) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"next-page-url\"]').getAttribute('content');\n    };\n  }\n\n  for (let it of dQSA(`.prev`)) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"prev-page-url\"]').getAttribute('content');\n    };\n  }\n}\n\nif (localStorage.getItem('eReaderType') == 'vert') {\n  if (!document.location.href.includes('&typeRead=true')) {\n    document.location.href += `&typeRead=true`;\n  }\n\n  for (let it of dQSA(`.next`)) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"next-page-url\"]').getAttribute('content');\n    };\n  }\n\n  for (let it of dQSA('[class*=\"id-\"]')) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"next-page-url\"]').getAttribute('content');\n    };\n  }\n\n  for (let it of dQSA(`.prev`)) {\n    it.onclick = e => {\n      window.location = dQS('[name=\"prev-page-url\"]').getAttribute('content');\n    };\n  }\n}\n\ndQS('#typeHorisontal').onmousedown = () => {\n  localStorage.setItem('eReaderType', 'horis');\n  document.location.href = document.location.href.replace('&typeRead=true', '');\n};\n\ndQS('#typeVertikal').onmousedown = () => {\n  localStorage.setItem('eReaderType', 'vert');\n  document.location.href += `&typeRead=true`;\n};\n\nfor (let it of dQSA('img')) {\n  it.ondragstart = () => {\n    return false;\n  };\n}\n\ndocument.body.oncontextmenu = e => {\n  return false;\n};\n\ndocument.onclick = e => {\n  let target = e.target;\n\n  if (!target.closest('.select-ereader')) {\n    for (let it of dQSA('.ereader-list')) {\n      removeClass(it, 'show');\n    }\n  }\n\n  if (!target.closest('.select-ereader-page')) {\n    for (let it of dQSA('.ereader-page-list')) {\n      removeClass(it, 'show');\n    }\n  }\n\n  if (!target.closest('.select-ereader-mode')) {\n    for (let it of dQSA('.ereader-mode-list')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/ereader.js?");

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet storage = {};\n\nfor (let it of dQSA('.menu__nav__search-box__panel__params span')) {\n  it.onclick = e => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {\n      removeClass(it2, 'active');\n    }\n\n    storage.searchTypeSort = it.dataset.typeSort;\n    addClass(it, 'active');\n  };\n}\n\nfor (let it of dQSA('.menu__nav__search-box__search-input')) {\n  it.onfocus = () => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel')) {\n      addClass(it2, 'show');\n    }\n  };\n\n  it.oninput = () => {\n    if (it.value.length >= 3) {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        addClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        addClass(it2, 'show');\n      }\n    } else {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        removeClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        removeClass(it2, 'show');\n      }\n    }\n  };\n}\n\ndocument.onmousedown = e => {\n  let target = e.target;\n\n  if (!target.closest('.menu__nav__search-box__panel') && !target.closest('.menu__nav__search-box__search-input')) {\n    for (let it of dQSA('.menu__nav__search-box__panel')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/js/menu.js?");

/***/ })

/******/ });