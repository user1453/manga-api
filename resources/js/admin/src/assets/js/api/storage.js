export let storage = {
    data: {
        roleFunctions: window.roleFunctions,
        userListShowLength: 0,
        userStatDecode: {
            0: 'ЗАБАНЕН',
            1: 'АКТИВЕН',
        }
    },
    system: {
        classPrefix: 's-',
    },
    console: {
        grpCollapsed: (fn, txt, css = undefined) => {
            if (css === undefined) {
                css = 'color: #000'
            }
            console.groupCollapsed(`%c${txt}`, css)
            fn()
            console.groupEnd()
        },
        statusCSS: {
            err: 'font-weight: normal; color: #f96262;'
        }
    },
    renderRole(id, title, fs = null) {
        let role = document.createElement('span')
        role.addClass(`role role-${id}`)
        if (fs !== null) {
            fs(role, id, title)
        }
        role.html(title)
        return role
    },
    getRoles() {

        let response = fetch('/admin/roles', {
            method: 'GET',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': document.querySelector('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            }
        });


        return response.then(req => req.json())


        /**
         * [fetchRes - прототип ответа fetch api]
         * @type {Object}
         */

        let fetchRes = {
            roles: [
                {
                    id: 0,
                    title: 'кодер',
                    type: 'user',
                    color: '00c0ff',
                    functions: [0]
                },
                {
                    id: 1,
                    title: 'администратор',
                    type: 'user',
                    color: 'e76e6e',
                    functions: [1, 2]
                },
                {
                    id: 2,
                    title: 'для команды',
                    type: 'team',
                    color: 'e76e6e',
                    functions: [0, 1, 2]
                }
            ]
        }

        return fetchRes.roles
    },
    getUsers(num) {

        let response = fetch('/admin/users', {
            method: 'GET',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': document.querySelector('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            }
        });


        return response
            .then(req => req.json())

        /**
         *
         * [fetchRes - прототип ответа fetch api]
         * @type {Object}
         */

        let fetchRes = {
            users: [
                {
                    id: 0,
                    name: 'coder',
                    date: '12.02.2020',
                    premium: 1,
                    roles: [0, 1],
                    status: 1
                },
                {
                    id: 1,
                    name: 'the_second_coder',
                    date: '13.02.2020',
                    premium: 1,
                    roles: [0],
                    status: 0
                },
            ]
        }

        // console.log(response)

        // return response.data
    }
}
