<?php

namespace App\Http\Controllers;

use App\Http\Resources\PartMangaCollection;
use App\Models\Manga;
use App\Models\Part;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PartController extends Controller
{
    public function newParts(Request $request)
    {
        $parts = Part::certified()->groupBy('manga_id')->selectRaw("max(manga_id) as id")->get()->toArray();
        $ids = [];
        foreach ($parts as $part)
            $ids[] = $part['id'];

        return response(new PartMangaCollection(Manga::whereIn('id', $ids)
//            ->where('created_at', '>=', Carbon::now()->startOfDay()->toDateTimeString())
            ->with('lastparts')
            ->get()), 200);
    }
}
