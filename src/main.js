import './assets/scss/main.sass'
import './js/menu.js'
import Echo from "laravel-echo"

window.io = require('socket.io-client');
window.Pusher = require('pusher-js');
const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
    dGetClass = (el) => document.getElementsByClassName(el),
    dGetTag = (el) => document.getElementsByTagName(el),
    dGetId = (el) => document.getElementById(el),
    dQS = (el) => document.querySelector(el),
    dQSA = (el) => document.querySelectorAll(el),
    removeClass = (el, el2) => el.classList.remove(el2),
    addClass = (el, el2) => el.classList.add(el2),
    replaceClass = (el, clr, cla) => {
        el.classList.remove(clr)
        el.classList.add(cla)
    },
    hasClass = (el, el2) => el.classList.contains(el2)

class Slider {
    constructor(id, nav) {
        this.id = id
        this.nav = nav
        this.numSlides = 0
        this.timeout = 2000
        this.count = 0
        this.interval = setInterval(() => {
            this._hideSlide()
            if (this.count == this.numSlides - 1) {
                this.count = 0
            } else {
                this.count++
            }
            this._showSlide()
        }, this.timeout)
    }

    _addSlide(img, link, alt = 'Слайд', title = 'Слайд') {
        this.numSlides++
        document.getElementById(this.id).innerHTML += `
        <div data-link="${link}" class="slide opacity-0">
            <img src="${img}" alt="${alt}" title="${title}">
        </div>`
        document.getElementById(this.nav).innerHTML += `<span></span>`
    }

    _hideSlide() {
        document.querySelectorAll('#homeSlider .slide')[this.count].classList.remove('opacity-1')
        document.querySelectorAll('#homeSlider .slide')[this.count].classList.add('opacity-0')
        document.querySelectorAll('#homeSliderNav span')[this.count].classList.remove('active')
    }

    _showSlide() {
        document.querySelectorAll('#homeSlider .slide')[this.count].classList.remove('opacity-0')
        document.querySelectorAll('#homeSlider .slide')[this.count].classList.add('opacity-1')
        document.querySelectorAll('#homeSliderNav span')[this.count].classList.add('active')
    }

    _activeSlide() {
        this.interval
    }

    _bindHandlers() {
        this._activeSlide()
        for (let it of document.querySelectorAll('#homeSliderNav span')) {
            it.onclick = () => {
                this._hideSlide()
                this.count = [].indexOf.call(document.querySelectorAll('#homeSliderNav span'), it)
                this._showSlide()
                clearInterval(this.interval)
                this.interval = setInterval(() => {
                    this._hideSlide()
                    if (this.count == this.numSlides - 1) {
                        this.count = 0
                    } else {
                        this.count++
                    }
                    this._showSlide()
                }, this.timeout)
            }
        }
    }

    _init() {
        this._showSlide()
        this._bindHandlers()
    }
}

class MangaList {
    constructor(cl) {
        this.id = cl
    }

    _addItemUpdatePop(title, imgUrl, url, info, translater) {
        let html = `
        <div class="manga-box margin-10px" data-link="${url}">
            <div class="shadow-dark-big" style="background-image: url('${imgUrl}')">
                <span class="after">
                    <span class="shadow-dark">Читать</span>
                </span>
                <span class="translater transition-m">${translater}</span>
                <span class="info transition-m">${info}</span>
            </div>
            <h3 class="font-14px m-t-10px transition-m">${title}</h3>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }

    _addItemSimple(title, url) {
        let html = `
        <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" data-link="${url}">${title}</li>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }

    _addItemSimplePage(title, url) {
        let html = `
        <li class="p-10px c-fi2 b-r-10px font-13px normal transition-m" data-link="${url}">${title}</li>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }

    _addItemPopular(title, altTitle, imgUrl, url, stat) {
        let html = `
        <div class="line m-t-10px transition-m" data-link="${url}">
            <div class="cover" style="background-image: url(${imgUrl})"></div>
            <div>
                <p class="font-15px bold">${title}</p>
                <span class="font-13px">${altTitle}</span>
                <br>
                <span class="rating detach-num font-13px m-t-10px">
                    ${stat}
                    <svg>
                        <use xlink:href="/assets/img/sprite.svg#views"></use>
                    </svg>
                </span>
            </div>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }

    _addItemNew(title, imgUrl, url, translater) {
        let html = `
        <div class="manga-box margin-10px" data-link="${url}">
            <div class="shadow-dark-big" style="background-image: url('${imgUrl}')">
                <span class="after">
                    <span class="shadow-dark">Читать</span>
                </span>
                <span class="translater transition-m">${translater}</span>
            </div>
            <h3 class="font-14px m-t-10px transition-m">${title}</h3>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }

    _addItemNewParts(title, altTitle, imgUrl, url, type, tags, part) {
        let
            tagsHtml = ``,
            items = ``
        for (let it in tags) {
            if (tags.hasOwnProperty(it)) {
                tagsHtml += `<span class="font-11px uppercase tag-${it}">${tags[it]}</span>`
            }
        }
        for (let it in part) {
            if (part.hasOwnProperty(it)) {
                items += `
                <tr>
                    <td class="hover-color-brand" data-link="${part[it].url}">${it}</td>
                    <td class="hover-color-brand"><a href="${part[it].pUrl}">${part[it].name}</a></td>
                </tr>`
            }
        }
        let html = `
        <div class="manga-box m-t-10px">
            <div class="img" style="background-image: url('${imgUrl}')">
                <span class="type">${type}</span>
            </div>
            <div class="info">
                <div class="start-info">
                    <h3>
                        <span class="font-18px">${title}</span>
                        <br>
                        <span class="font-14px normal">${altTitle}</span>
                    </h3>
                    <div class="info-box">
                        <div class="t-box">
                            ${tagsHtml}
                        </div>
                    </div>
                </div>
                <table class="table-srt m-t-10px">
                    <tbody class="font-14px">
                        ${items}
                    </tbody>
                </table>
            </div>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }
}

class TopTeams {
    constructor(teams) {
        this.teams = teams
    }

    _sort() {
        this.top = []
        for (let it of this.teams) {
            this.top.push({
                name: it.name,
                val: it.speed + it.quality + it.volume,
                link: it.link
            })
        }
        this.top = this.top.sort((prev, next) => next.val - prev.val)
    }

    _addItemsHome(cl) {
        this._sort()
        for (let it of document.querySelectorAll(`.${cl}`)) {
            for (let i = 0; i < this.top.length; i++) {
                it.innerHTML += `
                <div class="line transition-m" data-link="${this.top[i].link}">
                    <span class="num bold font-14px">${i + 1}</span>
                    <div class="info">
                        <span>${this.top[i].name}</span>
                        <span class="c-fd bold">${this.top[i].val}</span>
                        <svg>
                            <use xlink:href="/assets/img/sprite.svg#star"></use>
                        </svg>
                    </div>
                </div>`
            }
        }
    }
}


window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'a6fcf07fb835fe173b5f',
    cluster: 'eu',
    encrypted: true
});

class Chat {
    constructor(id) {
        this.date = new Date()
        this.id = id
    }

    _addMessage(text, name, urlImg, cl = '') {
        let html = `
        <tr>
            <td class="ava" style="background-image: url(${urlImg})"></td>
            <td class="text">
                <p class="bold font-15px">${name}</p>
                <span class="font-14px">${text}</span>
            </td>
        </tr>`
        document.querySelectorAll(`#${this.id} .message tbody`)[0].innerHTML += html
    }

    _bindHundlers() {
        for (let it of document.querySelectorAll(`#${this.id} .input-box input`)) {
            it.addEventListener('keyup', (e) => {
                let val = it.value.trim()
                if (e.keyCode === 13 && val.length != '') {
                    // тест поведения
                    if (val == 'Ошибка' || val == 'ошибка') {
                        document.querySelectorAll(`#${this.id} .input-box .error`)[0].style.display = 'block'
                    } else {
                        document.querySelectorAll(`#${this.id} .input-box .error`)[0].style.display = 'none'
                        let response = fetch('/chat', {
                            method: 'POST',
                            mode: "cors",
                            headers: {
                                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                                'Content-Type': 'application/json',
                            },
                            body: JSON.stringify({
                                message: String(val),
                            }),
                        });

                        response
                            .then(req => req.json())
                            .then(req => {
                                // console.log(req);
                            });


                    }
                    it.value = ''
                    for (let it2 of document.querySelectorAll(`#${this.id} .message`)) {
                        it2.scrollTop = it2.scrollHeight
                    }
                }
            })
        }
    }

    _init() {
        this._bindHundlers()
        window.Echo.channel('chat')
            .listen("Message", ({message}) => {
                this._addMessage(message.message, message.name, message.image)
            })
    }
}

// --- SLIDER ------------

let homeSlider = new Slider('homeSlider', 'homeSliderNav')
homeSlider._addSlide('/assets/img/slider-slide.png', '/')
homeSlider._addSlide('/assets/img/slider-slide-2.png', '/')
homeSlider._init()

let homeChat = new Chat('homeChat')
// homeChat._addMessage('Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.', 'H!dden', '/assets/img/demo-ava.jpg')
homeChat._init()

let addListUpdatePop = fetch('/api/v1/manga/popular-updates', {
    method: 'POST'
})
addListUpdatePop
    .then(req => req.json())
    .then(req => {
        let updateNewManga = new MangaList('manga-update-container')
        for (let item of req) {
            updateNewManga._addItemUpdatePop(item.rusName, item.cover, `/manga/${item.id}`, `Том ${item.vol} - Глава ${item.part}`, item.translater)
        }
    })

let addListNewManga = fetch('/api/v1/manga/new', {
    method: 'POST'
})
addListNewManga
    .then(req => req.json())
    .then(req => {
        let newManga = new MangaList('new-manga-list')
        for (let item of req) {
            newManga._addItemNew(item.rusName, item.cover, `/manga/${item.id}`, item.translater)
        }
    })

let addListNewMangaParts = fetch('/api/v1/part/new', {
    method: 'POST'
})
addListNewMangaParts
    .then(req => req.json())
    .then(req => {
        console.log(req);
        let newPartsManga = new MangaList('manga-new-container')
        for (let item of req) {
            let parts = {}
            let length = 5

            if (item.parts.length < 5) length = item.parts.length

            for (var i = 0; i < length; i++) {
                // console.log(item.parts[]);
                let part = item.parts[i]
                parts[`Том ${part.vol} Глава ${part.part}`] = {
                    name: part.translater,
                    url: part.url,
                    pUrl: part.pUrl
                }
            }

            newPartsManga._addItemNewParts(
                item.rusName,
                item.origName,
                item.cover,
                `/manga/${item.id}`,
                item.type,
                item.tags,
                parts
            )
        }
    })

let addListPopManga = fetch('/api/v1/manga/popular', {
    method: 'POST'
})
addListPopManga
    .then(req => req.json())
    .then(req => {
        console.log(req);
        let popularManga = new MangaList('top-manga-list')
        for (let item of req) {
            popularManga._addItemPopular(item.rusName, item.origName, item.cover, `/manga/${item.id}`, item.views)
        }
    })

let simpleList = new MangaList('ereader-list')
for (let i = 0; i < 20; i++) {
    simpleList._addItemSimple(`Том 1 - Глава ${i} : Юй Синь Ши, Чжу Сяо Оу!`, '/ereader.html')
}

let simpleListPage = new MangaList('ereader-page-list')
for (let i = 0; i < 10; i++) {
    simpleListPage._addItemSimple(`Страница ${i}`, '/ereader.html')
}

let addListTopTeam = fetch('/api/v1/team/top', {
    method: 'POST'
})
addListTopTeam
    .then(req => req.json())
    .then(req => {
        console.log(req);
        let topTeams = new TopTeams(req)

        topTeams._addItemsHome('top-teams')
    })
//
// let topTeams = new TopTeams([
//     {//2
//         name: 'Черный кролик',
//         speed: 4,
//         quality: 5,
//         volume: 5,
//         link: '/team.html'
//     },
//     {//1
//         name: 'Gl Alliance',
//         speed: 5,
//         quality: 5,
//         volume: 5,
//         link: '/team.html'
//     },
//     {//3
//         name: 'Ntro',
//         speed: 4,
//         quality: 4,
//         volume: 5,
//         link: '/team.html'
//     },
//     {//4
//         name: 'TlSamurai',
//         speed: 4,
//         quality: 4,
//         volume: 4,
//         link: '/team.html'
//     }
// ])
// topTeams._addItemsHome('top-teams')
