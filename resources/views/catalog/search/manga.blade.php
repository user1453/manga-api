@foreach($items as $manga)
    <a href="{{route('manga.page',['id' => $manga->id])}}" class="line" data-name-manga="{{$manga->rus_name}}">

        <img src="{{$manga->cover}}" alt="{{$manga->rus_name}}">

        <div>

            <h3 class="font-18px m-b-10px">{{$manga->origin_name}}</h3>

            <p class="font-16px">{{$manga->rus_name}}</p>

        </div>

    </a>

    <hr>
@endforeach
