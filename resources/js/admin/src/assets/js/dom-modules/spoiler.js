export function spoiler () {
    for (let item of document.querySelectorAll('[open-spoiler]')) {
        item.addEventListener('click', (e) => {
            toggleSpoiler(item.attr('open-spoiler') || undefined, true)
        })
    }

    for (let item of document.querySelectorAll('[close-spoiler]')) {
        item.addEventListener('click', (e) => {
            toggleSpoiler(item.attr('close-spoiler') || undefined, false)
        })
    }
}

function toggleSpoiler (val, flag) {
    if (val !== undefined && val !== '') {
        for (let spoiler of document.querySelectorAll(`[spoiler="${val}"]`)) {
            let classes = {
                clShow: {
                    val: spoiler.attr('cl-show') || undefined,
                    timeout: function () {
                        if (this.val !== undefined && this.val !== '') {
                            let arr = this.val.split('~')
                            if (arr.length === 2) {
                                return String(arr[1])
                            } else {
                                return String(0)
                            }
                        }
                    }
                },
                clHide: {
                    val: spoiler.attr('cl-hide') || undefined,
                    timeout: function () {
                        if (this.val !== undefined && this.val !== '') {
                            let arr = this.val.split('~')
                            if (arr.length === 2) {
                                this.val = arr[0]
                                return String(arr[1])
                            } else {
                                return String(0)
                            }
                        }
                    }
                }
            }

            let clShow = classes.clShow
            let clHide = classes.clHide

            if (flag) {
                if (clShow.val !== undefined && clShow.val !== '') {
                    for (let cl of clShow.val.split(' ')) {
                        new Promise((rej) => {
                            setTimeout(function () {
                                spoiler.addClass(cl.split('~')[0])
                                rej()
                            }, Number(cl.split('~')[1]) * 1000)
                        }).then(() => {
                            if (clHide.val !== undefined && clHide.val !== '') {
                                for (let cl of clHide.val.split(' ')) {
                                    spoiler.removeClass(cl.split('~')[0])
                                }
                            }
                        })
                    }
                }
            } else if (!flag) {
                if (clHide.val !== undefined && clHide.val !== '') {
                    for (let cl of clHide.val.split(' ')) {
                        new Promise((rej) => {
                            setTimeout(function () {
                                spoiler.addClass(cl.split('~')[0])
                                rej()
                            }, Number(cl.split('~')[1]) * 1000)
                        }).then(() => {
                            if (clShow.val !== undefined && clShow.val !== '') {
                                for (let cl of clShow.val.split(' ')) {
                                    spoiler.removeClass(cl.split('~')[0])
                                }
                            }
                        })
                    }
                }
            }
        }
    }
}
