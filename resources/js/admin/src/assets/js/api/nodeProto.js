/*
    PROTOTYPES
*/

// add html
Element.prototype.addHtml = function (str = null) {
    if (str !== null) {
        this.innerHTML += str
    }
    return this
}

NodeList.prototype.addHtml = function (str = null) {
    for (let item of this) {
        item.addHtml(str)
    }
    return this
}

// html
Element.prototype.html = function (str = null) {
    if (str === null) {
        return this.innerHTML
    } else {
        this.innerHTML = str
        return this
    }
}

NodeList.prototype.html = function (str = null) {
    for (let item of this) {
        item.html(str)
    }
    return this
}

// append
Element.prototype._append = function (node = null) {
    if (node !== null) {
        this.append(node)
    }
}

NodeList.prototype._append = function (node = null) {
    if (node !== null) {
        for (let item of this) {
            item.append(node)
        }
    }
    return this
}

// text
Element.prototype.text = function (str = null) {
    if (str === str) {
        return this.innerText
    } else {
        this.innerText = str
    }
}

NodeList.prototype.text = function (str = null) {
    return this[0].html(str)
}

// attributes
Element.prototype.attr = function (...arr) {
    for (let item of arr) {
        let attrs = item.split(' ')
        if (attrs.length === 1) {
            return this.getAttribute(attrs[0])
        } else if (attrs.length === 2) {
            this.setAttribute(attrs[0], attrs[1])
        }
    }
}

NodeList.prototype.attr = function (...arr) {
    for (let item of this) {
        item.attr(arr)
    }
}

// css
Element.prototype.css = function (obj) {
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            this.style[key] = obj[key]
        }
    }
    return this
}

NodeList.prototype.css = function (obj) {
     for (let item of this) {
         item.css(obj)
     }
     return this
}

// add class
Element.prototype.addClass = function (str) {
    let arr = str.split(' ')
    for (let _class of arr) {
        this.classList.add(_class)
    }
    return this
}

NodeList.prototype.addClass = function (str) {
    for (let item of this) {
        item.addClass(str)
    }
    return this
}

// remove class
Element.prototype.removeClass = function (str) {
    let arr = str.split(' ')
    for (let _class of arr) {
        this.classList.remove(_class)
    }
    return this
}

NodeList.prototype.removeClass = function (str) {
    for (let item of this) {
        item.removeClass(str)
    }
    return this
}

// toggle class
Element.prototype.toggleClass = function (str) {
    let arr = str.split(' ')
    for (let _class of arr) {
        this.classList.toggle(_class)
    }
    return this
}

NodeList.prototype.toggleClass = function (str) {
    for (let item of this) {
        item.toggleClass(str)
    }
    return this
}

// has class
Element.prototype.hasClass = function (str) {
    return this.classList.contains(str)
}

NodeList.prototype.hasClass = function (str) {
    for (let item of this) {
        item.hasClass(str)
    }
    return this
}

// click
Element.prototype.click = function (fn) {
    this.addEventListener('click', fn(this))
    return this
}

NodeList.prototype.click = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.click(fn(item))
        }
    }
    return this
}

// mouseover
Element.prototype.mouseover = function (fn) {
    this.addEventListener('mouseover', fn(this))
    return this
}

NodeList.prototype.mouseover = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.mouseover(fn(item))
        }
    }
    return this
}

// mouseout
Element.prototype.mouseout = function (fn) {
    this.addEventListener('mouseout', fn(this))
    return this
}

NodeList.prototype.mouseout = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.mouseout(fn(item))
        }
    }
    return this
}

// mousedown
Element.prototype.mousedown = function (fn) {
    this.addEventListener('mousedown', fn(this))
    return this
}

NodeList.prototype.mousedown = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.mousedown(fn(item))
        }
    }
    return this
}

// mouseup
Element.prototype.mouseup = function (fn) {
    this.addEventListener('mouseup', fn(this))
    return this
}

NodeList.prototype.mouseup = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.mouseup(fn(item))
        }
    }
    return this
}

// mousemove
Element.prototype.mousemove = function (fn) {
    this.addEventListener('mousemove', fn(this))
    return this
}

NodeList.prototype.mousemove = function (fn) {
    for (let item of this) {
        if (item !== undefined && item !== null) {
            item.mousemove(fn(item))
        }
    }
    return this
}

// ----------
