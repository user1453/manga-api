<?php

namespace App\Models;

use App\Collections\CommentCollection;
use DOMDocument;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['body'];

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function newCollection(array $models = [])
    {
        return new CommentCollection($models);
    }

    public static function stip_tags($html_str)
    {
        $xml = new DOMDocument();
        $allowed_tags = array("b", "br", "u", "em", "hr", "i", "p", "s", "span", "div", "strike");
//List the attributes you want to allow here
        $allowed_attrs = array("class", "style");
        if (!strlen($html_str)) {
            return false;
        }
        if ($xml->loadHTML(mb_convert_encoding($html_str, 'HTML-ENTITIES', 'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD)) {
            foreach ($xml->getElementsByTagName("*") as $tag) {
                if (!in_array($tag->tagName, $allowed_tags)) {
                    $tag->parentNode->removeChild($tag);
                } else {
                    foreach ($tag->attributes as $attr) {
                        if (!in_array($attr->nodeName, $allowed_attrs)) {
                            $tag->removeAttribute($attr->nodeName);
                        } else {
                            if ($attr->nodeName == 'class') {
                                if (!in_array($attr->value, ['after-spoiler', 'before-spoiler', 'spoiler'])) {
                                    $tag->removeAttribute($attr->nodeName);
                                }
                            }
                            if ($attr->nodeName == 'style') {
                                if (!in_array($attr->value, ['font-weight: bold;'])) {
                                    $tag->removeAttribute($attr->nodeName);
                                }
                            }
                        }
                    }
                }
            }
        }
        return $xml->saveHTML();
    }
}
