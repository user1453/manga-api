<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            0 => 'Манга',
            1 => 'Манхва',
            2 => 'Маньхуа',
            3 => 'OEL-манга',
            4 => 'Руманга',
            5 => 'Комикс западный'
        ];

        foreach ($categories as $key => $category) {
            Category::create(['id' => $key + 1, 'name' => $category]);
        }
    }
}
