<?php

use App\Models\Category;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            ['name' => 'panel access', 'label' => 'Доступ в админку'],
            ['name' => 'edit role', 'label' => 'Редактирование ролей'],
            ['name' => 'super admin', 'label' => 'Админ'],
            ['name' => 'add manga', 'label' => 'Добавлять мангу'],
            ['name' => 'edit manga', 'label' => 'Редактировать мангу'],
            ['name' => 'remove manga', 'label' => 'Удалять мангу'],
            ['name' => 'remove comment', 'label' => 'Удалять коментарии'],
            ['name' => 'view premium', 'label' => 'Просмотр премиум глав'],
            ['name' => 'issue ban', 'label' => 'Банить'],
            ['name' => 'add part', 'label' => 'Загружать главы'],
            ['name' => 'edit part', 'label' => 'Редактировать главы'],
            ['name' => 'issue team role', 'label' => 'Выдать роль участнику группы'],
            ['name' => 'edit team description', 'label' => 'Редактировать описание группы'],
            ['name' => 'add premium status', 'label' => 'Сделать главу платной'],
            ['name' => 'manage budget', 'label' => 'Распоряжаться бюджетом'],
            ['name' => 'viewing purchase statistics', 'label' => 'Просмотр статистики покупок'],
            ['name' => 'change palette', 'label' => 'Изменять палитру сайта'],

        ];

        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission['name'], 'label' => $permission['label']]);
        }
    }
}
