<!DOCTYPE html>
<html style="height: auto;">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/assets/css/ereader.css">
    <title>@yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('css')
    @if (app()->environment('production'))
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(65454673, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/65454673" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    @endif
</head>
<body>

@yield('header')
@yield('main')
<div class="left-0 right-0 top-0 bottom-0 substrate fixed hide">
    <div class="absolute left-0 right-0 top-0 bottom-0 center">
        <div class="modal b-li b-r-10px auth hide" data-auth-modal="sign-in">
            <input type="text" placeholder="Логин или e-mail" class="m-t-10px" data-auth-input="log">
            <br>
            <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-input="pass">
            <br>
            <div class="m-t-10px">
                <span class="error font-13px m-t-10px hide" data-auth-check="log">Неверный логин</span>
                <span class="error font-13px m-t-10px hide" data-auth-check="pass">Неверный пароль</span>
            </div>
            <div class="btn-box">
                <button type="button" class="btn-stand font-14px m-t-10px send-form">Вход</button>
                <div class="font-14px m-t-10px">
                    <div class="checkbox">
                        <span class="input"></span>
                        <span class="text">запомнить меня</span>
                    </div>
                </div>
            </div>
            <div class="center">
                <hr>
            </div>
            <div class="btn-box">
                <button type="button" class="btn-help font-13px m-t-10px" data-auth="recovery">Забыли пароль?</button>
                <button type="button" class="btn-help font-13px m-t-10px" data-auth="reg">Регистрация</button>
            </div>
        </div>
        <div class="modal b-li b-r-10px auth hide" data-auth-modal="reg">
            <div class="center">
                <h2>Регистрация</h2>
            </div>
            <div class="m-t-20px">
                <div>
                    <input type="text" placeholder="Логин" class="m-t-20px" data-auth-check="login"
                           data-auth-input="log">
                    <span class="error font-13px m-t-10px hide" data-auth-check="login">Упс! Такой уже существует</span>
                </div>
                <div>
                    <input type="text" placeholder="E-mail" class="m-t-10px" data-auth-check="e-mail"
                           data-auth-input="e-mail">
                    <span class="error font-13px m-t-10px hide"
                          data-auth-check="e-mail">Упс! Такой уже существует</span>
                </div>
                <div>
                    <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-check="pass"
                           data-auth-input="pass">
                    <span class="error font-13px m-t-10px hide" data-auth-check="pass">Пароль может содержать только латиницу, цифры и _</span>
                </div>
                <div>
                    <input type="text" placeholder="Повторите пароль" class="m-t-10px" data-auth-check="pass-check"
                           data-auth-input="pass-check">
                    <span class="error font-13px m-t-10px hide" data-auth-check="pass-check">Пароли не совпадают</span>
                </div>
                <div class="btn-box m-t-20px">
                    <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Регистрация</button>
                    <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="sign-in">У меня есть
                        аккаунт
                    </button>
                </div>
            </div>
        </div>
        <div class="modal b-li b-r-10px auth hide" data-auth-modal="recovery">
            <div class="center">
                <h2 class="font-18px">Восстановление пароля</h2>
            </div>
            <div class="m-t-20px rec-w">
                <div>
                    <input type="text" placeholder="Логин или e-mail" class="m-t-20px" data-auth-input="log"
                           data-auth-check="login-rec">
                    <span class="error font-13px m-t-10px hide"
                          data-auth-check="login-rec">Пользователь не найден</span>
                </div>
                <div class="btn-box m-t-20px">
                    <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Восстановить</button>
                    <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="reg">Регистрация
                    </button>
                </div>
            </div>
            <div class="rec-c hide">
                <p class="c-se font-14px m-t-20px text-center">Письмо с инструкциями отправлено на ваш электронный
                    адрес</p>
            </div>
        </div>
    </div>
</div>
<script>
    let auth = false;
</script>
{{--<script src="/assets/js/index.js" charset="utf-8"></script>--}}
<script src="/assets/js/ereader.js" charset="utf-8"></script>
@stack('scripts')
<script src="/assets/js/vendors.js" charset="utf-8"></script>
</body>
</html>
