<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use willvincent\Rateable\Rateable;

class Team extends Model
{
    use Rateable;
    use HasRoles;

    protected $guarded = [];

    public function delegates()
    {

        return $this->hasMany(Delegate::class)->with('user');
    }

    public function leader()
    {
        return Delegate::where('position', 'Глава')->where('team_id', $this->id)->with('user')->first();
//        return User::first();
        return $this->belongsTo(Delegate::class)->where('position', 'глава')->with('user');
    }

    /**
     * @return Builder
     */
    public static function certified()
    {
        return self::where('certified', true);
    }
}
