/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"addparts": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/addparts.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/addparts.sass":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/addparts.sass ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/assets/scss/addparts.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./src/addparts.js":
/*!*************************!*\
  !*** ./src/addparts.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_scss_addparts_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/scss/addparts.sass */ \"./src/assets/scss/addparts.sass\");\n/* harmony import */ var _assets_scss_addparts_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_addparts_sass__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/menu.js */ \"./src/js/menu.js\");\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_menu_js__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nconst log = text => console.log(text);\n\nlet random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;\n\nconst dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet s = document.querySelector('select');\nlet select = '';\n\nfor (let i = 0; i < s.length; i++) {\n  select += `<option value=\"${s.options[i].value}\">${s.options[i].value}</option>`;\n}\n\nlet files = [];\nlet searchUrl = document.location.search;\nsearchUrl = searchUrl.replace('?', '').split('&');\nlet searchs = {};\nsearchUrl.map((item, i, arr) => {\n  searchs[item.split('=')[0]] = item.split('=')[1];\n});\nlocalStorage.setItem('flagLoad', 'true');\nsetInterval(function () {\n  for (let item of dQSA('input')) {\n    item.oninput = () => {\n      item.setAttribute('value', item.value);\n    };\n  }\n\n  for (let item of dQSA('.remove')) {\n    item.onclick = e => {\n      let target = e.target;\n\n      if (!target.closest('.part-box').classList.contains('firstbox')) {\n        target.closest('.part-box').remove();\n      }\n    };\n  }\n\n  for (let item of dQSA('.part-box [type=\"file\"]')) {\n    item.onchange = () => {\n      if (item.files[0].name.split('.')[1] == 'rar' || item.files[0].name.split('.')[1] == 'zip') {\n        addClass(item.closest('label').querySelector('span'), 'active');\n        item.closest('label').querySelector('span').innerText = 'Добавлено';\n        files.push({\n          file: item.files[0],\n          part: item.closest('.part-box').dataset.part\n        });\n      } else {\n        addClass(item.closest('label').querySelector('span'), 'error');\n        item.closest('label').querySelector('span').innerText = 'Только rar или zip!';\n      }\n    };\n  }\n\n  dQS('.add-part-btn').onclick = () => {\n    let vol = Number(dQSA('.vol input')[dQSA('.vol input').length - 1].value);\n    let part = Number(dQSA('.part input')[dQSA('.part input').length - 1].value) + 1;\n    dQS('.addparts').innerHTML += `<div class=\"part-box\" data-vol=\"${vol}\" data-part=\"${part}\">\n            <div class=\"remove\">\n                <svg>\n                    <use xlink:href=\"/assets/img/sprite.svg#remove\"></use>\n                </svg>\n            </div>\n            <div class=\"section vol\">\n                <p>Том</p>\n                <input type=\"text\" value=\"${Number(dQSA('.vol input')[dQSA('.vol input').length - 1].value)}\">\n            </div>\n            <div class=\"section part\">\n                <p>Глава</p>\n                <input type=\"text\" value=\"${Number(dQSA('.part input')[dQSA('.part input').length - 1].value) + 1}\">\n            </div>\n            <div class=\"section namePart\">\n                <p>Название</p>\n                <input type=\"text\" style=\"width: 250px\">\n            </div>\n            <div class=\"section select\">\n                <p>Переводчик</p>\n                <select>\n                    ${select}\n                </select>\n            </div>\n            <label>\n                <span class=\"load-zip\">\n                    <svg>\n                        <use xlink:href=\"/assets/img/sprite.svg#folder\"></use>\n                    </svg>\n                    <span>Загрузка <span class=\"c-fd\">zip</span> или <span class=\"c-fd\">rar</span></span>\n                </span>\n                <input type=\"file\">\n            </label>\n        </div>`;\n  };\n\n  dQS('.load-btn').onclick = () => {\n    const fData = new FormData();\n    let parts = '';\n\n    for (let i = 0; i < dQSA('.part-box').length; i++) {\n      let item = dQSA('.part-box')[i];\n      let partId = document.querySelectorAll('.part-box')[i].querySelector('.part input').value;\n      fData.append(`part[${partId}][vol]`, document.querySelectorAll('.part-box')[i].querySelector('.vol input').value);\n      fData.append(`part[${partId}][part]`, partId);\n      fData.append(`id`, dQS('[name=\"manga-id\"]').getAttribute('content'));\n      fData.append(`part[${partId}][name]`, document.querySelectorAll('.part-box')[i].querySelector('.namePart input').value);\n      fData.append(`part[${partId}][translater]`, document.querySelectorAll('.part-box')[i].querySelector('select').value);\n\n      for (let file of files) {\n        if (file.part == item.dataset.part) {\n          fData.append(`file_${partId}`, file.file);\n        }\n      }\n\n      parts += String(item.dataset.part) + ',';\n    }\n\n    fData.append('parts', parts.substring(0, parts.length - 1));\n\n    if (Boolean(localStorage.getItem('flagLoad'))) {\n      alert('Началась загрузка. Не закрывайте страницу.');\n      localStorage.setItem('flagLoad', false);\n      fetch('/manga/upload', {\n        method: 'POST',\n        mode: \"cors\",\n        headers: {\n          'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content')\n        },\n        body: fData\n      }).then(req => req.json()).then(req => {\n        if (req.status == 1) {\n          alert('Успешно загружено');\n          localStorage.setItem('flagLoad', true);\n        }\n      });\n    } else {\n      alert('Уже загружается! Не закрывайте страницу.');\n    }\n  };\n}, 100);\n\n//# sourceURL=webpack:///./src/addparts.js?");

/***/ }),

/***/ "./src/assets/scss/addparts.sass":
/*!***************************************!*\
  !*** ./src/assets/scss/addparts.sass ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js!../../../node_modules/postcss-loader/src??ref--5-3!../../../node_modules/sass-loader/dist/cjs.js!./addparts.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/addparts.sass\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/assets/scss/addparts.sass?");

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet storage = {};\n\nfor (let it of dQSA('.menu__nav__search-box__panel__params span')) {\n  it.onclick = e => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {\n      removeClass(it2, 'active');\n    }\n\n    storage.searchTypeSort = it.dataset.typeSort;\n    addClass(it, 'active');\n  };\n}\n\nfor (let it of dQSA('.menu__nav__search-box__search-input')) {\n  it.onfocus = () => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel')) {\n      addClass(it2, 'show');\n    }\n  };\n\n  it.oninput = () => {\n    if (it.value.length >= 3) {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        addClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        addClass(it2, 'show');\n      }\n    } else {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        removeClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        removeClass(it2, 'show');\n      }\n    }\n  };\n}\n\ndocument.onmousedown = e => {\n  let target = e.target;\n\n  if (!target.closest('.menu__nav__search-box__panel') && !target.closest('.menu__nav__search-box__search-input')) {\n    for (let it of dQSA('.menu__nav__search-box__panel')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/js/menu.js?");

/***/ })

/******/ });