<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\StatusResponse;
use App\Models\Manga;
use App\Models\Team;
use App\Models\User;
use App\Notifications\MangaFailureNotification;
use Illuminate\Http\Request;

class MangaController extends Controller
{
    public function failure(Request $request, $id)
    {
        $manga = Manga::where('id', $id)->first();
        $user = User::find($manga->user_id);

        $user->notify(new MangaFailureNotification($request->get('message')));
        $manga->delete();

        return new StatusResponse(1);
    }

    public function certified(Request $request, $id)
    {
        Manga::where('id', $id)->update(['certified' => true]);
        $manga = Manga::where('id', $id)->first();
        $manga->publish();

        return new StatusResponse(1);
    }
}
