import './assets/scss/addparts.sass'

import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
    dGetClass = (el) => document.getElementsByClassName(el),
    dGetTag = (el) => document.getElementsByTagName(el),
    dGetId = (el) => document.getElementById(el),
    dQS = (el) => document.querySelector(el),
    dQSA = (el) => document.querySelectorAll(el),
    removeClass = (el, el2) => el.classList.remove(el2),
    addClass = (el, el2) => el.classList.add(el2),
    replaceClass = (el, clr, cla) => {
        el.classList.remove(clr)
        el.classList.add(cla)
    },
    hasClass = (el, el2) => el.classList.contains(el2)

let s = document.querySelector('select');
let select = ''
for (let i = 0; i < s.length; i++) {
    select += `<option value="${s.options[i].value}">${s.options[i].value}</option>`
}
let files = []

let searchUrl = document.location.search
searchUrl = searchUrl.replace('?', '').split('&')
let searchs = {}

searchUrl.map((item, i, arr) => {
    searchs[item.split('=')[0]] = item.split('=')[1]
})

localStorage.setItem('flagLoad', 'true')

setInterval(function () {
    for (let item of dQSA('input')) {
        item.oninput = () => {
            item.setAttribute('value', item.value)
        }
    }
    for (let item of dQSA('.remove')) {
        item.onclick = (e) => {
            let target = e.target
            if (!target.closest('.part-box').classList.contains('firstbox')) {
                target.closest('.part-box').remove()
            }
        }
    }
    for (let item of dQSA('.part-box [type="file"]')) {
        item.onchange = () => {
            if (item.files[0].name.split('.')[1] == 'rar' || item.files[0].name.split('.')[1] == 'zip') {
                addClass(item.closest('label').querySelector('span'), 'active')
                item.closest('label').querySelector('span').innerText = 'Добавлено'
                files.push({
                    file: item.files[0],
                    part: item.closest('.part-box').dataset.part
                })
            } else {
                addClass(item.closest('label').querySelector('span'), 'error')
                item.closest('label').querySelector('span').innerText = 'Только rar или zip!'
            }
        }
    }
    dQS('.add-part-btn').onclick = () => {
        let vol = Number(dQSA('.vol input')[dQSA('.vol input').length - 1].value)
        let part = Number(dQSA('.part input')[dQSA('.part input').length - 1].value) + 1
        dQS('.addparts').innerHTML += `<div class="part-box" data-vol="${vol}" data-part="${part}">
            <div class="remove">
                <svg>
                    <use xlink:href="/assets/img/sprite.svg#remove"></use>
                </svg>
            </div>
            <div class="section vol">
                <p>Том</p>
                <input type="text" value="${Number(dQSA('.vol input')[dQSA('.vol input').length - 1].value)}">
            </div>
            <div class="section part">
                <p>Глава</p>
                <input type="text" value="${Number(dQSA('.part input')[dQSA('.part input').length - 1].value) + 1}">
            </div>
            <div class="section namePart">
                <p>Название</p>
                <input type="text" style="width: 250px">
            </div>
            <div class="section select">
                <p>Переводчик</p>
                <select>
                    ${select}
                </select>
            </div>
            <label>
                <span class="load-zip">
                    <svg>
                        <use xlink:href="/assets/img/sprite.svg#folder"></use>
                    </svg>
                    <span>Загрузка <span class="c-fd">zip</span> или <span class="c-fd">rar</span></span>
                </span>
                <input type="file">
            </label>
        </div>`
    }
    dQS('.load-btn').onclick = () => {
        const fData = new FormData
        let parts = ''
        for (let i = 0; i < dQSA('.part-box').length; i++) {
            let item = dQSA('.part-box')[i]
            let partId =  document.querySelectorAll('.part-box')[i].querySelector('.part input').value;

            fData.append(`part[${partId}][vol]`, document.querySelectorAll('.part-box')[i].querySelector('.vol input').value)
            fData.append(`part[${partId}][part]`, partId)
            fData.append(`id`, dQS('[name="manga-id"]').getAttribute('content'))
            fData.append(`part[${partId}][name]`, document.querySelectorAll('.part-box')[i].querySelector('.namePart input').value)
            fData.append(`part[${partId}][translater]`, document.querySelectorAll('.part-box')[i].querySelector('select').value)

            for (let file of files) {
                if (file.part == item.dataset.part) {
                    fData.append(`file_${partId}`, file.file)
                }
            }

            parts += String(item.dataset.part) + ','

        }

        fData.append('parts', parts.substring(0, parts.length - 1))

        if (Boolean(localStorage.getItem('flagLoad'))) {
            alert('Началась загрузка. Не закрывайте страницу.')
            localStorage.setItem('flagLoad', false)

            fetch('/manga/upload', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content')
                },
                body: fData,
            })
                .then(req => req.json())
                .then(req => {
                    if (req.status == 1) {
                        alert('Успешно загружено')
                        localStorage.setItem('flagLoad', true)
                    }
                })
        } else {
            alert('Уже загружается! Не закрывайте страницу.')
        }
    }
}, 100);
