<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

/**
 * @property string status
 */
class Part extends Model
{
    /**
     * @param $id
     * @return Builder|Model|object|null
     */
    public static function firstPart($id)
    {
        return Cache::rememberForever('manga:firstpart#' . $id, function () use ($id) {
            return self::certified()->with('user')->where('manga_id', $id)->orderBy('number')->first();
        });
    }

    /**
     * Get the user that owns.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function manga()
    {
        return $this->belongsTo(Manga::class);
    }

    public function delegate()
    {
        return $this->hasOne(Delegate::class, 'user_id', 'user_id');
    }

    /**
     * @return Builder
     */
    public static function certified()
    {
        return self::where('certified', true);
    }

    public function publish()
    {
        if ($this->status != 'local')
            return;

        $pages = Page::where('part_id', $this->id)->get();
        foreach ($pages as $page) {
            $file = Storage::disk('local')->get($page->path);
            Storage::disk('public')->put($page->path, $file);
        }

        Cache::forget('manga:firstpart#' . $this->maga_id);

        self::where('id', $this->id)->update(['status' => 'public']);
    }

    public function pages()
    {
        return $this->hasMany(Page::class);
    }

    /**
     * @param $mangaId
     * @param $translatorId
     * @param $partNumber
     * @return Model
     */
    public static function findByNumber($mangaId, $translatorId, $partNumber)
    {
        return self::certified()
            ->where('manga_id', $mangaId)
            ->where('user_id', $translatorId)
            ->where('number', $partNumber)
            ->first();
    }

    public function url($page = 1, $typeRead = false)
    {

        $data = ['id' => $this->manga_id, 'page' => $page, 'part' => str_pad($this->number, 3, '0', STR_PAD_LEFT), 'translator' => $this->user->login];
        if ($typeRead) {
            $data['typeRead'] = $typeRead;
        }

        return route('reader', $data);
    }
}
