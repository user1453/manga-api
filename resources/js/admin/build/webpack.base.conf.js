const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { VueLoaderPlugin } = require('vue-loader');

const PATHS = {
    src: path.join(__dirname, '../src'),
    dist: path.join(__dirname, '../../../../public/adm'),
    assets: 'assets/'
}

function newHtml(html) {
    let result = new HtmlWebpackPlugin ({
        hash: false,
        template: `${PATHS.src}/${html}.html`,
        filename: `./${html}.html`,
        inject: false
    })
    return result
}

module.exports = {
    externals: {
        paths: PATHS
    },
    entry: {
        index: `${PATHS.src}/index.js`,
    },
    output: {
        filename: `${PATHS.assets}js/[name].js`,
        path: PATHS.dist,
        publicPath: '/'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    name: 'vendors',
                    test: /node_modules/,
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    module: {
        rules: [{
            //подключаем babel лоадер.
            test: /\.js$/, //расширение файлов на которые действует лоадер
            loader: 'babel-loader', //сам лоадер
            exclude: '/node_modules/' //исключения (так как node_modules оптимизированы для всех браузеров)
        }, {
            test: /\.vue$/,
            loader: 'vue-loader',
            options: { //настройки
                loader: { //запускаем необходимые лоадеры для работы библиотеки
                    scss: 'vue-style-loader!css-loader!sass-loader' //подключаем препроцессор-лоадер
                }
            }
        }, {
            test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }, {
            test: /\.(png|jpg|jpeg|gif|svg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }, {
            test: /\.css$/,
            use: [ //указывает, какой загрузчик следует использовать для сопоставленных модулей
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader'
                }, {
                    loader: 'postcss-loader',
                    options: { config: { path: `./postcss.config.js` } } //указываем путь до конфига
                },
            ]
        }, {
            test: /\.s(c|a)ss$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader'
                }, {
                    loader: 'postcss-loader',
                    options: { config: { path: `./postcss.config.js` } }
                }, {
                    loader: "sass-loader"
                }
            ]
        }]
    },
    resolve: {
        alias: {
            '~': 'src',
            'vue$': 'vue/dist/vue.js'
        }
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: `${PATHS.assets}css/[name].css`
        }),
        // newHtml('index'),
        new CopyWebpackPlugin([
            {from: `${PATHS.src}/${PATHS.assets}img`, to: `${PATHS.assets}img`},
            {from: `${PATHS.src}/${PATHS.assets}fonts`, to: `${PATHS.assets}fonts`},
            {from: `${PATHS.src}/static`, to: ''}
        ])
    ],
}
