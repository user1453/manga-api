'use strict'

import './assets/index.sass'
import './assets/js/api/nodeProto'

// requires api
const {storage} = require('./assets/js/api/storage')
const {customPrompt} = require('./assets/js/api/prompt.js')
const {editStyle} = require('./assets/js/api/style.js')

// requires dom modules
const {btnGrp} = require('./assets/js/dom-modules/btn-grp.js')
const {ntf, addNtf} = require('./assets/js/dom-modules/ntf.js')
const {tab} = require('./assets/js/dom-modules/tab.js')
const {imgZoom} = require('./assets/js/dom-modules/img-zoom.js')
const {spoiler} = require('./assets/js/dom-modules/spoiler.js')
const {settings} = require('./assets/js/dom-modules/settings.js')

const sysClassPrefix = storage.system.classPrefix

spoiler()
imgZoom(`${sysClassPrefix}img-zoom`)
btnGrp()
ntf()
tab({
    always: function (cnt) {
        document.querySelectorAll('.settings').addClass('hide')
    },
    loadUserRoles: function (cnt) {
        createRoleByType(cnt, 'user')
    },
    loadTeamRoles: function (cnt) {
        createRoleByType(cnt, 'team')
    },
    loadUsers: function (cnt) {
        document.querySelectorAll('[user-box]').html('')
        createUserList(cnt, storage.data.userListShowLength, storage.data.userListShowLength + 50)
    }
})
document.addEventListener('createRole', function (event) {
    alert(event.detail);
});
let settingsPatterns = {
    _addSaveBtn: function (item, api, fn = null) {
        if (item.querySelector('[save="true"]') === null) {
            api.addBtn('Сохранить', null, (btn) => {
                btn.attr('save true')
                btn.addClass('btn btn_brand fade-in-left')
                btn.addEventListener('click', (e) => {
                    addNtf('Сохранено', 'ok', 't r')
                    console.log(api.events)
                    let response = fetch('/admin/events', {
                        method: 'POST',
                        mode: "cors",
                        headers: {
                            'X-CSRF-TOKEN': document.querySelector('[name="csrf-token"]').getAttribute('content'),
                            'Content-Type': 'application/json',
                        }, body: JSON.stringify(api.events)
                    });
                })
                if (typeof fn === 'function') {
                    fn(btn, api)
                }
            })

        }
    },
    editRole: function (DATA, role) {
        settings('.settings', (item, api) => {
            api.addEvent('eventType', 'editRole')
            api.addEvent('targetId', role.id)
            api.setTitle('настройки', () => {
                item
                    .querySelectorAll('.settings__title-secondary')
                    ._append(storage.renderRole(role.id, role.title))
            })
            api.addInput('Название', 'name', (label, input) => {
                input.value = role.title
                input.addEventListener('input', (e) => {
                    if (input.value.length === 0) {
                        item
                            .querySelectorAll('.settings__title-secondary')
                            .html('')
                            ._append(storage.renderRole(role.id, role.title))
                    } else {
                        item
                            .querySelectorAll(`.settings__title-secondary .role-${role.id}`)
                            .html(input.value)
                        api.addEvent('editName', input.value)
                        this._addSaveBtn(item, api)
                    }
                })
            })
            api.addInput('Цвет (hex)', 'color', (label, input, p) => {
                input.value = role.color ? role.color : '#'
                input.attr('maxlength 7')
                input.addEventListener('input', (e) => {
                    if (input.value[0] !== '#') {
                        input.value = `#${input.value}`
                    } else if (input.value[1] === '#') {
                        input.value = input.value.replace('#', '')
                    }
                    editStyle(
                        'role-edit-styles',
                        `.role-${role.id} {
                            color: ${input.value} !important;
                            border-color: ${input.value} !important;
                        }
                        .role-${role.id}::after {
                            background: ${input.value} !important;
                        }`
                    )
                    api.addEvent('editColor', input.value)
                    this._addSaveBtn(item, api)
                })
            })
            for (let key in DATA.roleFunctions) {
                if (DATA.roleFunctions.hasOwnProperty(key)) {
                    key = Number(key)
                    if (role.functions.includes(key)) {
                        api.addToggleBtn(DATA.roleFunctions[key], true, (btn) => {
                            btn.addEventListener('click', (e) => {
                                api.addEvent('updateFunction', `${btn.attr('id-toggle-btn')}:${btn.attr('status-toggle-btn')}`, 'object')
                                this._addSaveBtn(item, api)
                            })
                        }, key)
                    } else {
                        api.addToggleBtn(DATA.roleFunctions[key], false, (btn) => {
                            btn.addEventListener('click', (e) => {
                                api.addEvent('updateFunction', `${btn.attr('id-toggle-btn')}:${btn.attr('status-toggle-btn')}`, 'object')
                                this._addSaveBtn(item, api)
                            })
                        }, key)
                    }
                }
            }

            function addBtn() {
                if (item.querySelector('[save="true"]') === null) {
                    api.addBtn('Сохранить', null, (btn) => {
                        btn.attr('save true')
                        btn.addClass('btn btn_brand fade-in-left')
                        btn.addEventListener('click', (e) => {
                            addNtf('Сохранено', 'ok', 't r')
                        })
                    })
                }
            }
        })
    },
    createRole: function (DATA, title, fn = null) {
        settings('.settings', (item, api) => {
            api.addEvent('eventType', 'createRole')
            api.addEvent('title', title)
            api.setTitle(`создание ${title}`, () => {
                item
                    .querySelectorAll('.settings__title-secondary')
                    ._append(storage.renderRole(null, 'новая роль'))
            })
            api.addInput('Название', 'name', (label, input) => {
                input.addEventListener('input', (e) => {
                    if (input.value.length === 0) {
                        item
                            .querySelectorAll('.settings__title-secondary')
                            .html('')
                            ._append(storage.renderRole(null, 'новая роль'))
                    } else {
                        item
                            .querySelectorAll('.settings__title-secondary .role-null')
                            .html(input.value)
                        api.addEvent('editName', input.value)
                        this._addSaveBtn(item, api)
                    }
                })
            })
            api.addInput('Цвет (hex)', 'color', (label, input, p) => {
                input.value = '#'
                input.attr('maxlength 7')
                input.addEventListener('input', (e) => {
                    if (input.value[0] !== '#') {
                        input.value = `#${input.value}`
                    } else if (input.value[1] === '#') {
                        input.value = input.value.replace('#', '')
                    }
                    editStyle(
                        'role-null-styles',
                        `.role-null {
                            color: ${input.value};
                            border-color: ${input.value};
                        }
                        .role-null::after {
                            background: ${input.value} !important;
                        }`
                    )
                    api.addEvent('editColor', input.value)
                    this._addSaveBtn(item, api)
                })
            })
            for (let key in DATA.roleFunctions) {
                if (DATA.roleFunctions.hasOwnProperty(key)) {
                    key = Number(key)
                    api.addToggleBtn(DATA.roleFunctions[key], false, (btn) => {
                        btn.addEventListener('click', (e) => {
                            api.addEvent('updateFunction', `${btn.attr('id-toggle-btn')}:${btn.attr('status-toggle-btn')}`, 'object')
                            this._addSaveBtn(item, api)
                        })
                    })
                }
            }
        })
    },
    editUser: function (user) {
        settings('.settings', (item, api) => {
            api.addEvent('eventType', 'user')
            api.addEvent('targetId', user.id)
            api.setTitle(`редактирование`, () => {
                item
                    .querySelectorAll('.settings__title-secondary')
                    .html('пользователь')
            })
            api.addInput('Имя', 'name', (label, input, p) => {
                input.value = user.name
                input.addEventListener('input', (e) => {
                    api.addEvent('editName', input.value)
                    this._addSaveBtn(item, api)
                })
            })
            api.addToggleBtn(storage.data.userStatDecode[user.status], Boolean(user.status), (btn) => {
                btn.addEventListener('click', (e) => {
                    if (user.status === 0) user.status = 1
                    else if (user.status === 1) user.status = 0
                    api.addEvent('editStatus', user.status)
                    this._addSaveBtn(item, api)
                    if (Boolean(user.status)) {
                        btn.html('АКТИВЕН')
                    } else {
                        btn.html('ЗАБАНЕН')
                    }
                })
            })
            api.addCustomBlock('p', (block) => {
                let p = document.createElement('p')
                p.addClass('settings__label')
                p.html('Текущие роли (удалить нажатием)')
                block._append(p)
                block.addClass('margin-top-20px')
            })
            api.addCustomBlock('div', (block) => {
                block.addClass('margin-top-10px')
                block.attr('active-roles true')

                storage.getRoles().then(response => {
                    storage.data.rolesList = response.data;
                    console.log(user.roles)
                    for (let el of user.roles) {

                        let role = storage.data.rolesList.find((element, index, array) => {
                            return element.id === el;
                        })
                        let rendered = storage.renderRole(role.id, role.title, (role, id) => {
                            role.attr(`role-id ${id}`)
                        })

                        rendered.addEventListener('click', (e) => {
                            api.addEvent('removeRole', rendered.attr('role-id'), 'array')
                            rendered.remove()
                            this._addSaveBtn(item, api)
                        })

                        block._append(rendered)
                    }
                })

            })
            api.addCustomBlock('p', (block) => {
                let p = document.createElement('p')
                p.addClass('settings__label')
                p.html('Добавить роль (нажатием)')
                block._append(p)
                block.addClass('margin-top-20px')
            })
            api.addCustomBlock('div', (block) => {
                block.addClass('margin-top-10px')
                block.attr('role-box true')

                createRoleByType(item, 'user', (role, obj) => {
                    role.addEventListener('click', (e) => {
                        let acRolesBox = item.querySelectorAll('[active-roles]')

                        if (item.querySelector(`[active-roles] [role-id="${obj.id}"]`) === null) {
                            acRolesBox.addHtml(role.outerHTML)
                            api.addEvent('addRole', obj.id, 'array')
                            role.remove()
                            this._addSaveBtn(item, api)
                        } else {
                            addNtf('Роль уже добавлена', 'dang', 't r')
                        }
                    })
                }, false)
            })
        })
    }
}

const mutationObserver = new MutationObserver(mutations => {

    // tracking the addition of elements
    for (let nodes of mutations) {
        let addNodes = nodes.addedNodes

        if (addNodes.length !== 0) {
            let node = addNodes[0]

            if (node.classList !== undefined) {
                if (node.classList.contains(`${sysClassPrefix}btn-grp`)) {
                    btnGrp()
                }
                if (node.classList.contains(`${sysClassPrefix}tab`)) {
                    tab()
                }
                if (node.classList.contains(`${sysClassPrefix}ntf`)) {
                    ntf()
                }
                if (
                    node.attr('spoiler') !== null ||
                    node.attr('open-spoiler') !== null ||
                    node.attr('close-spoiler') !== null
                ) {
                    spoiler()
                }
            }
        }
    }

})

mutationObserver.observe(document.documentElement, {
    attributes: true,
    characterData: true,
    childList: true,
    subtree: true,
    attributeOldValue: true,
    characterDataOldValue: true
})

function createRoleByType(cnt, type, fn = null, bind = true) {
    storage.getRoles().then(response => {
        storage.data.rolesList = response.data;
        let data = storage.data.rolesList;
        let boxList = cnt.querySelectorAll('[role-box]').html('')

        for (let role of data) {
            if (role.type === type) {
                let rendered = storage.renderRole(role.id, role.title, (role, id) => {
                    role.attr(`role-id ${id}`, `open-spoiler settings-window`)
                })
                boxList._append(rendered)

                if (bind) {
                    bindRole(rendered, boxList)
                }

                if (typeof fn === 'function') {
                    fn(rendered, role, boxList)
                }
            }
        }
    })
}

function createUserList(cnt, intervalStart, intervalEnd) {
    // if (storage.data.usersList === undefined) {
    storage.getUsers().then(res => {
            storage.data.usersList = res.data;
            let data = storage.data
            let usersList = data.usersList

            for (let i = intervalStart; i < intervalEnd; i++) {
                let item = usersList[i]

                if (item !== undefined) {
                    let user = document.createElement('div')
                    user.addClass(`plate plate_user row-1 col-5`)
                    user.attr('open-spoiler settings-window', `user-id ${item.id}`)
                    user.html(`<div class="col">${item.name}</div>
            <div class="col">${item.date}</div>
            <div class="col">Премиум: ${item.premium}</div>
            <div class="col">Роли: ${item.roles.length}</div>`)
                    if (item.status === 0) {
                        user.addHtml(`<button type="button" class="btn btn_red">${storage.data.userStatDecode[item.status]}</button>`)
                    } else if (item.status === 1) {
                        user.addHtml(`<button type="button" class="btn btn_green">${storage.data.userStatDecode[item.status]}</button>`)
                    }
                    user.addEventListener('click', (e) => {
                        settingsPatterns.editUser(item)
                    })
                    document.querySelectorAll('[user-box]')._append(user)
                } else {
                    break
                }
            }


            storage.userListShowLength += intervalEnd
        }
    )
    // }


}

function bindRole(target, parent) {
    target.addEventListener('click', () => {
        const ID = Number(target.attr('role-id'))
        const DATA = storage.data

        for (let role of DATA.rolesList) {
            if (role.id === ID) {
                settingsPatterns.editRole(DATA, role)
            }
        }
    })
}

application('application-team-id', 'team')
application('application-manga-id', 'manga')

function application(attr, type, promptTitle = 'Укажите причину отклонения') {
    for (let item of document.querySelectorAll(`[${attr}]`)) {
        let id = item.getAttribute(attr)


        item.querySelector('.application-yes').addEventListener('click', (e) => {
            /*
            ...Заявка одобрена
            */
            fetch('/admin/' + type + '/' + id + '/certified', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': document.querySelector('[name="csrf-token"]').getAttribute('content'),
                    'Content-Type': 'application/json',
                }
            }).then(response => response.json()
                .then(response => {
                    console.log(response);

                    item.addClass('fade-out-left')
                    setTimeout(function () {
                        item.remove()
                    }, 1000)
                })
            );
        });

        item.querySelector('.application-no').addEventListener('click', (e) => {
            customPrompt(promptTitle, (p) => {
                /**
                 * prompt.on() returned {p}
                 * [p - введенная причина отказа]
                 * @type {String}
                 */

                /*
                ...Заявка отклонена
                */
                console.log(p);
                if (p !== null) {
                    fetch(e.currentTarget.getAttribute('action'), {
                        method: 'POST',
                        mode: "cors",
                        headers: {
                            'X-CSRF-TOKEN': document.querySelector('[name="csrf-token"]').getAttribute('content'),
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({'message': p})
                    }).then(response => response.json()
                        .then(response => {
                            console.log(response);

                            item.addClass('fade-out')
                            setTimeout(function () {
                                item.remove()
                            }, 1000)
                        })
                    );
                }
            })
        })
    }
}

document.querySelector('#addUserRole').addEventListener('click', (e) => {
    const DATA = storage.data

    settingsPatterns.createRole(DATA, 'для пользователя')
})

document.querySelector('#addTeamRole').addEventListener('click', (e) => {
    const DATA = storage.data
    settingsPatterns.createRole(DATA, 'для команды')
})


//
