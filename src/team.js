import './assets/scss/team.sass'
import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
    dGetClass = (el) => document.getElementsByClassName(el),
    dGetTag = (el) => document.getElementsByTagName(el),
    dGetId = (el) => document.getElementById(el),
    dQS = (el) => document.querySelector(el),
    dQSA = (el) => document.querySelectorAll(el),
    removeClass = (el, el2) => el.classList.remove(el2),
    addClass = (el, el2) => el.classList.add(el2),
    replaceClass = (el, clr, cla) => {
        el.classList.remove(clr)
        el.classList.add(cla)
    },
    hasClass = (el, el2) => el.classList.contains(el2)

function checker(el) {
    for (let it of dQSA(`${el} button`)) {
        it.onclick = (e) => {
            let target = e.target
            for (let it2 of dQSA(`${el} button`)) {
                removeClass(it2, 'active')
            }
            addClass(target, 'active')
        }
    }
}

checker('.main-filter')
checker('.help-filter')

class MangaList {
    constructor(cl) {
        this.id = cl
    }

    _add(title, imgUrl, url, translator) {
        let html = `
        <div class="manga-box margin-10px" data-link="${url}">
            <div class="shadow-dark-big" style="background-image: url('${imgUrl}')">
                <span class="after">
                    <span class="shadow-dark">Читать</span>
                </span>
                <span class="translater transition-m">${translator}</span>
            </div>
            <h3 class="font-14px m-t-10px transition-m">${title}</h3>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }
}


let addMangaList = fetch('/api/v1/team/' + dQS('[name="team-id"]').getAttribute('content') + '/manga', {
    method: 'POST'
})
addMangaList
    .then(req => req.json())
    .then(req => {
        console.log(req);

        let mangaListRead = new MangaList('manga-read-cont')
        for (var i = 0; i < req.length; i++) {
            mangaListRead._add(req[i].title, req[i].imgUrl, req[i].url, req[i].translator)
        }

    })


function select(parent, list) {
    for (let it of dQSA(`${parent} span, ${parent} svg`)) {
        it.onclick = (e) => {
            for (let it2 of dQSA(list)) {
                if (hasClass(it2, 'show')) {
                    removeClass(it2, 'show')
                } else {
                    addClass(it2, 'show')
                }
            }
        }
    }
    for (let it of dQSA(list + ' li')) {
        it.onmousedown = (e) => {
            for (let it2 of dQSA(`${parent} span`)) {
                it2.innerText = it.innerText
            }
            for (let it2 of dQSA(list)) {
                removeClass(it2, 'show')
            }
        }
    }
}

select('div.person', 'table.person')
select('div.contacts', 'table.contacts')
