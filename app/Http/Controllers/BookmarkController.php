<?php

namespace App\Http\Controllers;

use App\Http\Resources\StatusResponse;
use App\Http\Resources\UserBookmarkCollection;
use App\Models\Bookmark;
use App\Models\Manga;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class BookmarkController extends Controller
{
    public function store(Request $request)
    {
        $id = $request->get('id');
        if ($id != null) {
            $bookmark = Bookmark::firstOrNew(['user_id' => Auth::user()->id, 'manga_id' => $id]);
            $bookmark->list = 'i-read';
            $bookmark->save();
            return response($bookmark, 200);
        }


        $manga = Manga::where('rus_name', $request->name)->first();
        if ($manga == null)
            return response(new StatusResponse(0), 200);

        $bookmark = Bookmark::firstOrNew(['user_id' => Auth::user()->id, 'manga_id' => $manga->id]);
        $bookmark->list = $request->get('list');
        $bookmark->save();

        Cache::forget('bookmarks:count#' . $manga->id);

        return response(new StatusResponse(1), 200);
    }

    public function show(Request $request, $id)
    {
        $bookmark = Bookmark::where('user_id', Auth::user()->id)->where('manga_id', $id)->first();
        return response($bookmark, 200);
    }

    public function get(Request $request)
    {
        $sort = $request->get('sort');
        $query = Bookmark::where('user_id', Auth::user()->id)->with('manga');

        if ($sort != null) {
            $query = $query->where('list', $sort);
        }
        return response(new UserBookmarkCollection($query->get()));
    }
}
