<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravelista\Comments\Commenter;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property mixed id
 */
class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use HasRoles;
    use Notifiable;
    use SoftDeletes;
    use Commenter;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password', 'gender', 'city', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function team()
    {
        return $this->hasOne(Team::class, 'user_id', 'id');
    }

    public static function findByLogin($login)
    {
        return Cache::remember('user#' . $login, 600, function () use ($login) {
            return User::where('login', $login)->first();
        });
    }

    public function premium()
    {
        return $this->belongsToMany(Team::class, 'premia', 'user_id', 'team_id');
    }


    public function hasDelegate()
    {
        return Delegate::where('user_id', $this->id)->first() != null ? true : false;
    }

    /**
     * @return Builder
     */
    public static function verified()
    {
        return self::whereNotNull('email_verified_at');
    }



    public function hasPermission($name)
    {
        $permission = Permission::where('name','=', $name)->first();
        if(! $permission) {
            return false;
        }

        return $this->hasRole($permission->roles);
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return (bool) $role->intersect($this->roles)->count();
    }

}
