<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\StatusResponse;
use App\Models\Delegate;
use App\Models\Manga;
use App\Models\Team;
use App\Models\User;
use App\Notifications\TeamFailureNotification;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function failure(Request $request, $id)
    {
        $team = Team::where('id', $id)->first();
        $user = $team->leader();

        if ($user != null)
            $user->notify(new TeamFailureNotification($request->get('message')));

        Delegate::where('team_id', $team->id)->delete();
        $team->delete();

        return new StatusResponse(1);
    }

    public function certified(Request $request, $id)
    {
        Team::where('id', $id)->update(['certified' => true]);

        return new StatusResponse(1);
    }
}
