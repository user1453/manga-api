import './assets/scss/catalog.sass'
import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => el.classList.add(el2),
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)

let generatorUrl = `/catalog?`

function checker (el) {
    for (let it of dQSA(`${el} button`)) {
        it.onclick = (e) => {
            let target = e.target
            window.location.href=target.dataset.url
        }
    }
}
class MangaList {
    constructor(cl) {
        this.id = cl
    }
    _add (title, imgUrl, url, translater) {
        let html = `
        <div class="manga-box margin-10px" data-link="${url}">
            <div class="shadow-dark-big" style="background-image: url('${imgUrl}')">
                <span class="after">
                    <span class="shadow-dark">Читать</span>
                </span>
                <span class="translater transition-m">${translater}</span>
            </div>
            <h3 class="font-14px m-t-10px transition-m">${title}</h3>
        </div>`
        for (let it of document.getElementsByClassName(this.id)) {
            it.innerHTML += html
        }
    }
}

checker('.main-filter')
checker('.help-filter')

let urlObj = {
    cat: 'cat=',
    statTranslate: 'statTranslate=',
    form: 'form=',
    genres: 'genres=',
    year: 'year=',
    hide: 'hide='
}

dQS('#relYear').onchange = () => {
    urlObj.year += dQS('#relYear').value
}



for (let i = 1; i <= 6; i++) {
    dQS(`[data-cat="${i}"]`).addEventListener('mousedown', (e) => {
        if (urlObj.cat.includes(`${i}_`)) {
            urlObj.cat = urlObj.cat.replace(`${i}_`, '')
        } else {
            urlObj.cat += `${i}_`
        }

        // let index = filter.cat.indexOf(i);
        //         // if (index > -1) {
        //         //     filter.cat.splice(index, 1);
        //         // } else {
        //         //     filter.cat.push(i)
        //         // }
    })
}

for (let i = 0; i <= 2; i++) {
    dQS(`[data-statTr="${i}"]`).addEventListener('mousedown', (e) => {
        if (urlObj.statTranslate.includes(`${i}_`)) {
            urlObj.statTranslate = urlObj.statTranslate.replace(`${i}_`, '')
        } else {
            urlObj.statTranslate += `${i}_`
        }
    })
}

for (let i = 0; i <= 1; i++) {
    dQS(`[data-hide="${i}"]`).addEventListener('mousedown', (e) => {
        if (urlObj.hide.includes(`${i}_`)) {
            urlObj.hide = urlObj.hide.replace(`${i}_`, '')
        } else {
            urlObj.hide += `${i}_`
        }
    })
}

for (let i = 0; i <= 5; i++) {
    dQS(`[data-form="${i}"]`).addEventListener('mousedown', (e) => {
        if (urlObj.form.includes(`${i}_`)) {
            urlObj.form = urlObj.form.replace(`${i}_`, '')
        } else {
            urlObj.form += `${i}_`
        }
    })
}

for (let i = 1; i <= 42; i++) {
    dQS(`[data-genres="${i}"]`).addEventListener('mousedown', (e) => {
        if (urlObj.genres.includes(`${i}_`)) {
            urlObj.genres = urlObj.genres.replace(`${i}_`, '')
        } else {
            urlObj.genres += `${i}_`
        }
    })
}

// let mangaListRead = new MangaList('manga-read-cont')
// for (var i = 0; i < 10; i++) {
//     mangaListRead._add('Городская болезнь', '/assets/img/demo-manga.jpg', '/ereader.html', 'GL Alliance')
// }


function select (parent, list) {
    for (let it of dQSA(parent)) {
        it.onclick = () => {
            for (let it2 of dQSA(list)) {
                if (hasClass(it2, 'show')) {
                    removeClass(it2, 'show')
                } else {
                    addClass(it2, 'show')
                }
            }
        }
    }
    for (let it of dQSA(list + ' li')) {
        it.onmousedown = (e) => {
            for (let it2 of dQSA(`${parent} span`)) {
                it2.innerText = it.innerText
            }
            for (let it2 of dQSA(list)) {
                removeClass(it2, 'show')
            }
        }
    }
}

dQS('.add-filters').onclick = () => {
    log(urlObj)
    for (var key in urlObj) {
        if (urlObj.hasOwnProperty(key)) {
            if (/\d/.test(urlObj[key])) {
                generatorUrl += urlObj[key] + '&'
            }
        }
    }
    window.location = generatorUrl
}

dQS('.remove-filters').onclick = () => {
    window.location = generatorUrl
}
