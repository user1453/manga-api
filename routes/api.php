<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@authenticate');
    Route::post('unique-login', 'UserController@uniqueLogin');
    Route::post('unique-email', 'UserController@uniqueEmail');

//    Route::post('filter', 'FilerController@filter');

    Route::post('get-user-name', 'UserController@getName');
    Route::group(['middleware' => ['jwt.verify']], function () {
    });
    Route::prefix('manga')->group(function () {

        Route::any('popular-updates', 'MangaController@popularUpdates');
        Route::any('popular', 'MangaController@popular');
        Route::any('new', 'MangaController@newManga');
        Route::any('{id}/view', 'MangaController@views');
        Route::any('{id}/show', 'MangaController@show');
        Route::any('{id}/parts', 'MangaController@parts');
    });
    Route::prefix('part')->group(function () {
        Route::any('new', 'PartController@newParts');
    });
    Route::prefix('page')->group(function () {
        Route::any('show', 'PageController@show');
    });
    Route::prefix('team')->group(function () {
        Route::any('top', 'TeamController@top');
        Route::any('{id}/manga', 'TeamController@manga');
    });

});
