<?php

namespace App\Jobs;

use App\Models\Page;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use ZipArchive;

//use \wapmorgan\UnifiedArchive\UnifiedArchive;

class ProcessResize implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    private $count;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->count = 0;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->data['path'];
        $part = $this->data['part'];

        if ($this->extract_file($path, storage_path('app/temp/archives/' . $part->id . '/')) == false)
            return;
        $this->resize();

        if ($this->count == 0) {
            $part->status = 'pages not found';
            $part->save();
        } else {
            $part->status = 'local';
            $part->save();
            $part->publish();
        }

    }

    public function resize()
    {
        $part = $this->data['part'];
        $translator = $this->data['translator'];

        foreach (File::allFiles(Storage::disk('temp')->path('archives/' . $part->id)) as $file) {
            echo $file->getFilename() . PHP_EOL;
            try {
                $extension = $file->getExtension();
                if ($extension == 'png' || $extension == 'jpg') {
                    $filename = $this->getPlaceNumber($file);
                    $page = Page::firstOrNew(['number' => $filename, 'part_id' => $part->id]);

                    $storagePath = "manga/{$part->manga_id}/{$translator}/{$part->vol}/{$part->number}/{$filename}.jpg";

                    $page->original = "manga/{$part->manga_id}/{$translator}/{$part->vol}/{$part->number}/{$filename}.{$extension}";


                    Storage::disk('originals')->put($page->original, $file->getContents());
                    $path = realpath(Storage::disk('originals')->path($page->original));


                    $this->place(1100, $path, $filename);
                    $page->path = $storagePath;
                    $page->save();
                    $this->count++;

                    echo ' resized' . PHP_EOL;
                } else {
                    continue;
                }

            } catch (\Exception $ex) {
                echo ' resized error' . PHP_EOL;
                echo $ex->getMessage() . PHP_EOL;
            }
        }
    }

    function extract_file($file_path, $to_path = "./")
    {
        $storagePath = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
        $file_type = substr($file_path, strrpos($file_path, '.') - strlen($file_path) + 1);
        if ("zip" === $file_type) {
            $xmlZip = new ZipArchive();

            if ($xmlZip->open($storagePath . $file_path) == true) {
                $xmlZip->extractTo($to_path);

                echo "extract success";
                $part = $this->data['part'];
                $part->status = 'unpacked';
                $part->save();

            } else {
                echo "extract fail";
                $part = $this->data['part'];
                $part->status = 'unpacking error';
                $part->save();

                Storage::disk('temp')->delete($file_path);
                return false;
            }
        } elseif ("rar" == $file_type) {

            try {
                shell_exec('unrar e ' . $storagePath . $file_path . ' ' . $to_path);
            } catch (\Exception $e) {

                echo "extract fail";
                $part = $this->data['part'];
                $part->status = 'unpacking error';
                $part->save();

                Storage::disk('temp')->delete($file_path);
                return false;
            }
        }
        return true;
    }

    public function place($width, $pathr, $placeNumber)
    {
        $part = $this->data['part'];
        $translator = $this->data['translator'];
        $storagePath = "manga/{$part->manga_id}/{$translator}/{$part->vol}/{$part->number}/{$placeNumber}.jpg";

        echo PHP_EOL . $pathr . PHP_EOL;

//        $waterMarkUrl = public_path('watermark.png');

        $resize = Image::make($pathr)
            ->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })
//            ->insert($waterMarkUrl, 'bottom-right', 5, 5)
            ->encode('jpg', 75);

        $resize->extension = 'jpg';
        Storage::disk('local')->put($storagePath, $resize->__toString());

        echo 'save ' . $storagePath . PHP_EOL;
        return $storagePath;
    }


    private function getPlaceNumber($file): string
    {
        $str = $file->getFilename();
        preg_match_all('!\d+!', $str, $matches);
        return ($matches[0][0]);
    }
}
