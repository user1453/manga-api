export function editStyle (id, str) {
    let item = document.querySelector(`head style#${id}`)
    if (item === null) {
        let style = document.createElement('style')
        style.id = id
        style.html(str)
        document.querySelector(`head`)._append(style)
    } else {
        item.html(str)
    }
}
