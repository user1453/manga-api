@extends('layouts.page')

@section('title', 'Загрузка глав')

@section('main')
    <div class="wrapper m-t-10px">
        <div class="content-area">
            <div class="addparts">
                @if($lastPart)
                    <div class="part-box firstbox" data-vol="{{$lastPart->vol}}" data-part="{{$lastPart->number + 1}}">
                        <div class="section vol">
                            <p>Том</p>
                            <input type="text" value="{{$lastPart->vol}}">
                        </div>
                        <div class="section part">
                            <p>Глава</p>
                            <input type="text" value="{{$lastPart->number + 1}}">
                        </div>
                @else
                    <div class="part-box firstbox" data-vol="1" data-part="1">

                        <div class="section vol">
                            <p>Том</p>
                            <input type="text" value="1">
                        </div>
                        <div class="section part">
                            <p>Глава</p>
                            <input type="text" value="1">
                        </div>
                @endif
                    <div class="section namePart">
                        <p>Название</p>
                        <input type="text" style="width: 250px">
                    </div>
                    <div class="section select">
                        <p>Переводчик</p>
                        <select>
                            @foreach($delegates as $delegate)
                                <option value="{{$delegate->user_id}}">{{$delegate->user->login}}</option>
                            @endforeach
                        </select>
                    </div>
                    <label>
                        <span class="load-zip">
                            <svg>
                                <use xlink:href="/assets/img/sprite.svg#folder"></use>
                            </svg>
                            <span>Загрузка <span class="c-fd">zip</span> или <span class="c-fd">rar</span></span>
                        </span>
                        <input type="file">
                    </label>
                </div>
            </div>
            <div class="panel">
                <button type="button" class="add-part-btn">Добавить главу</button>
                <button type="button" class="load-btn">Начать загрузку</button>
            </div>
        </div>
    </div>


@stop

@push('css')

    <meta name="manga-id" content="{{$manga->id}}">
    <link rel="stylesheet" href="/assets/css/addparts.css">
@endpush

@push('scripts')

    <script src="/assets/js/addparts.js" charset="utf-8"></script>
@endpush
