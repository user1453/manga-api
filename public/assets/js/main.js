/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/main.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/main.sass":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/main.sass ***!
  \******************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/assets/scss/main.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./src/assets/scss/main.sass":
/*!***********************************!*\
  !*** ./src/assets/scss/main.sass ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js!../../../node_modules/postcss-loader/src??ref--5-3!../../../node_modules/sass-loader/dist/cjs.js!./main.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/main.sass\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/assets/scss/main.sass?");

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet storage = {};\n\nfor (let it of dQSA('.menu__nav__search-box__panel__params span')) {\n  it.onclick = e => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {\n      removeClass(it2, 'active');\n    }\n\n    storage.searchTypeSort = it.dataset.typeSort;\n    addClass(it, 'active');\n  };\n}\n\nfor (let it of dQSA('.menu__nav__search-box__search-input')) {\n  it.onfocus = () => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel')) {\n      addClass(it2, 'show');\n    }\n  };\n\n  it.oninput = () => {\n    if (it.value.length >= 3) {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        addClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        addClass(it2, 'show');\n      }\n    } else {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        removeClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        removeClass(it2, 'show');\n      }\n    }\n  };\n}\n\ndocument.onmousedown = e => {\n  let target = e.target;\n\n  if (!target.closest('.menu__nav__search-box__panel') && !target.closest('.menu__nav__search-box__search-input')) {\n    for (let it of dQSA('.menu__nav__search-box__panel')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/js/menu.js?");

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_scss_main_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/scss/main.sass */ \"./src/assets/scss/main.sass\");\n/* harmony import */ var _assets_scss_main_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_main_sass__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/menu.js */ \"./src/js/menu.js\");\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_menu_js__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var laravel_echo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! laravel-echo */ \"./node_modules/laravel-echo/dist/echo.js\");\n\n\n\nwindow.io = __webpack_require__(/*! socket.io-client */ \"./node_modules/socket.io-client/lib/index.js\");\nwindow.Pusher = __webpack_require__(/*! pusher-js */ \"./node_modules/pusher-js/dist/web/pusher.js\");\n\nconst log = text => console.log(text);\n\nlet random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;\n\nconst dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nclass Slider {\n  constructor(id, nav) {\n    this.id = id;\n    this.nav = nav;\n    this.numSlides = 0;\n    this.timeout = 2000;\n    this.count = 0;\n    this.interval = setInterval(() => {\n      this._hideSlide();\n\n      if (this.count == this.numSlides - 1) {\n        this.count = 0;\n      } else {\n        this.count++;\n      }\n\n      this._showSlide();\n    }, this.timeout);\n  }\n\n  _addSlide(img, link, alt = 'Слайд', title = 'Слайд') {\n    this.numSlides++;\n    document.getElementById(this.id).innerHTML += `\n        <div data-link=\"${link}\" class=\"slide opacity-0\">\n            <img src=\"${img}\" alt=\"${alt}\" title=\"${title}\">\n        </div>`;\n    document.getElementById(this.nav).innerHTML += `<span></span>`;\n  }\n\n  _hideSlide() {\n    document.querySelectorAll('#homeSlider .slide')[this.count].classList.remove('opacity-1');\n    document.querySelectorAll('#homeSlider .slide')[this.count].classList.add('opacity-0');\n    document.querySelectorAll('#homeSliderNav span')[this.count].classList.remove('active');\n  }\n\n  _showSlide() {\n    document.querySelectorAll('#homeSlider .slide')[this.count].classList.remove('opacity-0');\n    document.querySelectorAll('#homeSlider .slide')[this.count].classList.add('opacity-1');\n    document.querySelectorAll('#homeSliderNav span')[this.count].classList.add('active');\n  }\n\n  _activeSlide() {\n    this.interval;\n  }\n\n  _bindHandlers() {\n    this._activeSlide();\n\n    for (let it of document.querySelectorAll('#homeSliderNav span')) {\n      it.onclick = () => {\n        this._hideSlide();\n\n        this.count = [].indexOf.call(document.querySelectorAll('#homeSliderNav span'), it);\n\n        this._showSlide();\n\n        clearInterval(this.interval);\n        this.interval = setInterval(() => {\n          this._hideSlide();\n\n          if (this.count == this.numSlides - 1) {\n            this.count = 0;\n          } else {\n            this.count++;\n          }\n\n          this._showSlide();\n        }, this.timeout);\n      };\n    }\n  }\n\n  _init() {\n    this._showSlide();\n\n    this._bindHandlers();\n  }\n\n}\n\nclass MangaList {\n  constructor(cl) {\n    this.id = cl;\n  }\n\n  _addItemUpdatePop(title, imgUrl, url, info, translater) {\n    let html = `\n        <div class=\"manga-box margin-10px\" data-link=\"${url}\">\n            <div class=\"shadow-dark-big\" style=\"background-image: url('${imgUrl}')\">\n                <span class=\"after\">\n                    <span class=\"shadow-dark\">Читать</span>\n                </span>\n                <span class=\"translater transition-m\">${translater}</span>\n                <span class=\"info transition-m\">${info}</span>\n            </div>\n            <h3 class=\"font-14px m-t-10px transition-m\">${title}</h3>\n        </div>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemSimple(title, url) {\n    let html = `\n        <li class=\"p-10px c-fi2 b-r-10px font-13px normal transition-m\" data-link=\"${url}\">${title}</li>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemSimplePage(title, url) {\n    let html = `\n        <li class=\"p-10px c-fi2 b-r-10px font-13px normal transition-m\" data-link=\"${url}\">${title}</li>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemPopular(title, altTitle, imgUrl, url, stat) {\n    let html = `\n        <div class=\"line m-t-10px transition-m\" data-link=\"${url}\">\n            <div class=\"cover\" style=\"background-image: url(${imgUrl})\"></div>\n            <div>\n                <p class=\"font-15px bold\">${title}</p>\n                <span class=\"font-13px\">${altTitle}</span>\n                <br>\n                <span class=\"rating detach-num font-13px m-t-10px\">\n                    ${stat}\n                    <svg>\n                        <use xlink:href=\"/assets/img/sprite.svg#views\"></use>\n                    </svg>\n                </span>\n            </div>\n        </div>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemNew(title, imgUrl, url, translater) {\n    let html = `\n        <div class=\"manga-box margin-10px\" data-link=\"${url}\">\n            <div class=\"shadow-dark-big\" style=\"background-image: url('${imgUrl}')\">\n                <span class=\"after\">\n                    <span class=\"shadow-dark\">Читать</span>\n                </span>\n                <span class=\"translater transition-m\">${translater}</span>\n            </div>\n            <h3 class=\"font-14px m-t-10px transition-m\">${title}</h3>\n        </div>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n  _addItemNewParts(title, altTitle, imgUrl, url, type, tags, part) {\n    let tagsHtml = ``,\n        items = ``;\n\n    for (let it in tags) {\n      if (tags.hasOwnProperty(it)) {\n        tagsHtml += `<span class=\"font-11px uppercase tag-${it}\">${tags[it]}</span>`;\n      }\n    }\n\n    for (let it in part) {\n      if (part.hasOwnProperty(it)) {\n        items += `\n                <tr>\n                    <td class=\"hover-color-brand\" data-link=\"${part[it].url}\">${it}</td>\n                    <td class=\"hover-color-brand\"><a href=\"${part[it].pUrl}\">${part[it].name}</a></td>\n                </tr>`;\n      }\n    }\n\n    let html = `\n        <div class=\"manga-box m-t-10px\">\n            <div class=\"img\" style=\"background-image: url('${imgUrl}')\">\n                <span class=\"type\">${type}</span>\n            </div>\n            <div class=\"info\">\n                <div class=\"start-info\">\n                    <h3>\n                        <span class=\"font-18px\">${title}</span>\n                        <br>\n                        <span class=\"font-14px normal\">${altTitle}</span>\n                    </h3>\n                    <div class=\"info-box\">\n                        <div class=\"t-box\">\n                            ${tagsHtml}\n                        </div>\n                    </div>\n                </div>\n                <table class=\"table-srt m-t-10px\">\n                    <tbody class=\"font-14px\">\n                        ${items}\n                    </tbody>\n                </table>\n            </div>\n        </div>`;\n\n    for (let it of document.getElementsByClassName(this.id)) {\n      it.innerHTML += html;\n    }\n  }\n\n}\n\nclass TopTeams {\n  constructor(teams) {\n    this.teams = teams;\n  }\n\n  _sort() {\n    this.top = [];\n\n    for (let it of this.teams) {\n      this.top.push({\n        name: it.name,\n        val: it.speed + it.quality + it.volume,\n        link: it.link\n      });\n    }\n\n    this.top = this.top.sort((prev, next) => next.val - prev.val);\n  }\n\n  _addItemsHome(cl) {\n    this._sort();\n\n    for (let it of document.querySelectorAll(`.${cl}`)) {\n      for (let i = 0; i < this.top.length; i++) {\n        it.innerHTML += `\n                <div class=\"line transition-m\" data-link=\"${this.top[i].link}\">\n                    <span class=\"num bold font-14px\">${i + 1}</span>\n                    <div class=\"info\">\n                        <span>${this.top[i].name}</span>\n                        <span class=\"c-fd bold\">${this.top[i].val}</span>\n                        <svg>\n                            <use xlink:href=\"/assets/img/sprite.svg#star\"></use>\n                        </svg>\n                    </div>\n                </div>`;\n      }\n    }\n  }\n\n}\n\nwindow.Echo = new laravel_echo__WEBPACK_IMPORTED_MODULE_2__[\"default\"]({\n  broadcaster: 'pusher',\n  key: 'a6fcf07fb835fe173b5f',\n  cluster: 'eu',\n  encrypted: true\n});\n\nclass Chat {\n  constructor(id) {\n    this.date = new Date();\n    this.id = id;\n  }\n\n  _addMessage(text, name, urlImg, cl = '') {\n    let html = `\n        <tr>\n            <td class=\"ava\" style=\"background-image: url(${urlImg})\"></td>\n            <td class=\"text\">\n                <p class=\"bold font-15px\">${name}</p>\n                <span class=\"font-14px\">${text}</span>\n            </td>\n        </tr>`;\n    document.querySelectorAll(`#${this.id} .message tbody`)[0].innerHTML += html;\n  }\n\n  _bindHundlers() {\n    for (let it of document.querySelectorAll(`#${this.id} .input-box input`)) {\n      it.addEventListener('keyup', e => {\n        let val = it.value.trim();\n\n        if (e.keyCode === 13 && val.length != '') {\n          // тест поведения\n          if (val == 'Ошибка' || val == 'ошибка') {\n            document.querySelectorAll(`#${this.id} .input-box .error`)[0].style.display = 'block';\n          } else {\n            document.querySelectorAll(`#${this.id} .input-box .error`)[0].style.display = 'none';\n            let response = fetch('/chat', {\n              method: 'POST',\n              mode: \"cors\",\n              headers: {\n                'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n                'Content-Type': 'application/json'\n              },\n              body: JSON.stringify({\n                message: String(val)\n              })\n            });\n            response.then(req => req.json()).then(req => {// console.log(req);\n            });\n          }\n\n          it.value = '';\n\n          for (let it2 of document.querySelectorAll(`#${this.id} .message`)) {\n            it2.scrollTop = it2.scrollHeight;\n          }\n        }\n      });\n    }\n  }\n\n  _init() {\n    this._bindHundlers();\n\n    window.Echo.channel('chat').listen(\"Message\", ({\n      message\n    }) => {\n      this._addMessage(message.message, message.name, message.image);\n    });\n  }\n\n} // --- SLIDER ------------\n\n\nlet homeSlider = new Slider('homeSlider', 'homeSliderNav');\n\nhomeSlider._addSlide('/assets/img/slider-slide.png', '/');\n\nhomeSlider._addSlide('/assets/img/slider-slide-2.png', '/');\n\nhomeSlider._init();\n\nlet homeChat = new Chat('homeChat'); // homeChat._addMessage('Lorem Ipsum - это текст-\"рыба\", часто используемый в печати и вэб-дизайне.', 'H!dden', '/assets/img/demo-ava.jpg')\n\nhomeChat._init();\n\nlet addListUpdatePop = fetch('/api/v1/manga/popular-updates', {\n  method: 'POST'\n});\naddListUpdatePop.then(req => req.json()).then(req => {\n  let updateNewManga = new MangaList('manga-update-container');\n\n  for (let item of req) {\n    updateNewManga._addItemUpdatePop(item.rusName, item.cover, `/manga/${item.id}`, `Том ${item.vol} - Глава ${item.part}`, item.translater);\n  }\n});\nlet addListNewManga = fetch('/api/v1/manga/new', {\n  method: 'POST'\n});\naddListNewManga.then(req => req.json()).then(req => {\n  let newManga = new MangaList('new-manga-list');\n\n  for (let item of req) {\n    newManga._addItemNew(item.rusName, item.cover, `/manga/${item.id}`, item.translater);\n  }\n});\nlet addListNewMangaParts = fetch('/api/v1/part/new', {\n  method: 'POST'\n});\naddListNewMangaParts.then(req => req.json()).then(req => {\n  console.log(req);\n  let newPartsManga = new MangaList('manga-new-container');\n\n  for (let item of req) {\n    let parts = {};\n    let length = 5;\n    if (item.parts.length < 5) length = item.parts.length;\n\n    for (var i = 0; i < length; i++) {\n      // console.log(item.parts[]);\n      let part = item.parts[i];\n      parts[`Том ${part.vol} Глава ${part.part}`] = {\n        name: part.translater,\n        url: part.url,\n        pUrl: part.pUrl\n      };\n    }\n\n    newPartsManga._addItemNewParts(item.rusName, item.origName, item.cover, `/manga/${item.id}`, item.type, item.tags, parts);\n  }\n});\nlet addListPopManga = fetch('/api/v1/manga/popular', {\n  method: 'POST'\n});\naddListPopManga.then(req => req.json()).then(req => {\n  console.log(req);\n  let popularManga = new MangaList('top-manga-list');\n\n  for (let item of req) {\n    popularManga._addItemPopular(item.rusName, item.origName, item.cover, `/manga/${item.id}`, item.views);\n  }\n});\nlet simpleList = new MangaList('ereader-list');\n\nfor (let i = 0; i < 20; i++) {\n  simpleList._addItemSimple(`Том 1 - Глава ${i} : Юй Синь Ши, Чжу Сяо Оу!`, '/ereader.html');\n}\n\nlet simpleListPage = new MangaList('ereader-page-list');\n\nfor (let i = 0; i < 10; i++) {\n  simpleListPage._addItemSimple(`Страница ${i}`, '/ereader.html');\n}\n\nlet addListTopTeam = fetch('/api/v1/team/top', {\n  method: 'POST'\n});\naddListTopTeam.then(req => req.json()).then(req => {\n  console.log(req);\n  let topTeams = new TopTeams(req);\n\n  topTeams._addItemsHome('top-teams');\n}); //\n// let topTeams = new TopTeams([\n//     {//2\n//         name: 'Черный кролик',\n//         speed: 4,\n//         quality: 5,\n//         volume: 5,\n//         link: '/team.html'\n//     },\n//     {//1\n//         name: 'Gl Alliance',\n//         speed: 5,\n//         quality: 5,\n//         volume: 5,\n//         link: '/team.html'\n//     },\n//     {//3\n//         name: 'Ntro',\n//         speed: 4,\n//         quality: 4,\n//         volume: 5,\n//         link: '/team.html'\n//     },\n//     {//4\n//         name: 'TlSamurai',\n//         speed: 4,\n//         quality: 4,\n//         volume: 4,\n//         link: '/team.html'\n//     }\n// ])\n// topTeams._addItemsHome('top-teams')\n\n//# sourceURL=webpack:///./src/main.js?");

/***/ }),

/***/ 0:
/*!********************!*\
  !*** ws (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("/* (ignored) */\n\n//# sourceURL=webpack:///ws_(ignored)?");

/***/ })

/******/ });