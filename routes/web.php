<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true]);


Route::group(['prefix' => 'admin', 'middleware' =>['auth', 'can:panel access']], function () {
    Route::get('/', 'Admin\IndexController@getIndex');
    Route::get('users', 'Admin\IndexController@users')->middleware('can:edit role');
    Route::get('teams', 'Admin\IndexController@teams')->middleware('can:edit role');
    Route::post('events', 'Admin\IndexController@events');
    Route::get('roles', 'Admin\IndexController@roles');
    Route::prefix('team')->group(function () {
        Route::post('/{id}/certified', 'Admin\TeamController@certified');
        Route::post('/{id}/failure', 'Admin\TeamController@failure');
    });
    Route::prefix('manga')->group(function () {
        Route::post('/{id}/certified', 'Admin\MangaController@certified');
        Route::post('/{id}/failure', 'Admin\MangaController@failure');
    });

});

Route::post('/ajax-login', 'UserController@ajaxLogin');
Route::post('/login-rec-check', 'UserController@loginRecCheck');
Route::get('catalog', 'CatalogController@getIndex')->name('catalog');
Route::post('reg-info-check-log', 'UserController@regInfoCheckLog');
Route::post('reg-info-check-mail', 'UserController@regInfoCheckMail');
Route::post('login-rec', 'UserController@forgotPassword');

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('team')->group(function () {

        Route::get('my', 'TeamController@teamRedirect')->name('team.redirect');
        Route::get('create', 'TeamController@getCreate')->name('team.create');
        Route::post('create', 'TeamController@create');
        Route::post('rate', 'TeamController@rate');
        Route::post('check-name', 'TeamController@checkName');
    });

    Route::get('user', 'UserController@getAuthenticatedUser');
    Route::prefix('manga')->group(function () {
        Route::post('rate', 'MangaController@rate')->name('manga.rate');
        Route::get('create', 'MangaController@getCreate')->name('manga.create')->middleware('can:add manga');
        Route::post('create', 'MangaController@create')->middleware('can:add manga');
        Route::post('check-name-{type}', 'MangaController@checkName');
        Route::post('upload', 'MangaController@upload');
        Route::get('{id}/addparts', 'MangaController@addparts')->name('manga.addparts')->middleware('can:add parts');
    });
    Route::prefix('bookmark')->group(function () {
        Route::get('/', 'BookmarkController@get');
        Route::post('store', 'BookmarkController@store');
        Route::post('show/{mangaId}', 'BookmarkController@show');
    });
});

Route::prefix('profile')->group(function () {
    Route::get('/', 'ProfileController@getIndex')->name('profile');
    Route::get('/{id}', 'ProfileController@getIndex')->name('profile.page');
});
Route::prefix('ereader')->group(function () {
    Route::get('/', 'PageController@getIndex')->name('reader');
    Route::get('image/{part}/{page}', 'PageController@image')->name('reader.image');
    Route::any('list-parts', 'PageController@parts')->name('reader.parts');
    Route::any('list-pages', 'PageController@pages')->name('reader.pages');
});


Route::post('search', 'CatalogController@search');
Route::post('chat', 'ChatController@message');
Route::get('chat/clear/{name}', 'ChatController@clear');
Route::get('team/{id}', 'TeamController@getIndex')->name('team.page');
Route::get('manga-clear', 'MangaController@clear');
Route::get('manga-rand', 'MangaController@rand');
Route::get('manga/{id}', 'MangaController@getIndex')->name('manga.page');
Route::get('/', 'IndexController@getIndex')->name('home');
Route::get('faq', function () {
    return view('pages.faq');
});

