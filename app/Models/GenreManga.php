<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GenreManga extends Model
{
    protected $table = 'genre_manga';
    protected $guarded = [];
    //
}
