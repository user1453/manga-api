const { storage } = require('../api/storage')

let prefix = storage.system.classPrefix

export function ntf() {
    for (let item of document.querySelectorAll(`.${prefix}ntf`)) {
        let cl = item.classList
        let txt = item.innerHTML
        let href = item.attr('href') || undefined
        let time = item.attr('time') || undefined
        let top = 0

        if (time === undefined || time === '') {
            time = 5
        }

        if (href !== undefined) {
            item.addEventListener('click', () => {
                document.location.href = href
            })
        }

        for (let item of document.querySelectorAll(`.${prefix}ntf`)) {
            top += item.clientHeight
            item.style.top = top + 'px'
            top += 10
        }

        new Promise((reject) => {
            setTimeout(function () {
                reject()
            }, time * 1000)
        }).then(() => {
            item.remove()
        })
    }
}

export function addNtf (html, stat, position) {
    let ntf = document.createElement('div')
    ntf.addClass(`${prefix}ntf ${prefix}ntf-${stat} ntf-fade-in-top ${position}`)
    ntf.html(html)
    document.body._append(ntf)
}
