<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MangaPart extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $part = $this->resource;

        return [
            'id' => $part->id,
            'number' => $part->number,
            'vol' => $part->vol,
            'name' => $part->name,
            'translater' => $part->user->login,
            'mangaId' => $part->manga_id,
            'date' => date('d:m:Y', strtotime($part->created_at)),
        ];
    }
}
