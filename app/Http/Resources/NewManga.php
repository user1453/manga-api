<?php

namespace App\Http\Resources;

use App\Models\Manga;
use App\Models\Part;
use Illuminate\Http\Resources\Json\JsonResource;

class NewManga extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $manga = $this->resource;

        return [
            'id' => $manga->id,
            'rusName' => $manga->rus_name,
            'cover' => $manga->cover,
            'translater' => Manga::translators($manga->id)
        ];
    }
}
