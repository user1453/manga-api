@extends('layouts.page')

@section('title', 'teikei.me')

@section('main')

    <div class="wrapper m-t-10px">
        <div class="content-area">
            <div class="lists shadow-dark p-10px b-li margin-10px">
                <div class="main-filter">
                    <button class="{{$sort != 'rate'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'rate'])}}">По рейтингу</button>
                    <button class="{{$sort != 'name'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'name'])}}">По названию</button>
                    <button class="{{$sort != 'views'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'views'])}}">По просмотрам</button>
                    <button class="{{$sort != 'created_at'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'created_at'])}}">По дате добавления</button>
                    <button class="{{$sort != 'updated_at'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'updated_at'])}}">По дате обновления</button>
                    <button class="{{$sort != 'count'?: 'active'}}" data-url="{{route('catalog', $input + ['sort' => 'count'])}}">По количеству глав</button>
                </div>
                <div class="help-filter">
                    <button class="{{$order != 'desc'?: 'active'}}" data-url="{{route('catalog', $inputExOrder + ['order' => 'desc'])}}">По убыванию</button>
                    <button class="{{$order != 'asc'?: 'active'}}" data-url="{{route('catalog', $inputExOrder + ['order' => 'asc'])}}">По возрастанию</button>
                </div>
            </div>
            <section>
                <div class="shadow-dark p-10px b-li margin-10px center manga-read-cont">
                    @foreach($mangas as $manga)
                        <div class="manga-box margin-10px" data-link="/manga/{{$manga->id}}">
                            <div class="shadow-dark-big" style="background-image: url({{$manga->cover}})">
                            <span class="after">
                                <span class="shadow-dark">Читать</span>
                            </span>
                                <span
                                    class="translater transition-m">{{\App\Models\Manga::translators($manga->id)}}</span>
                            </div>
                            <h3 class="font-14px m-t-10px transition-m">{{$manga->origin_name}}</h3>
                        </div>
                    @endforeach
                </div>
                <div class="filters p-10px b-li margin-10px shadow-dark">
                    <div class="btn-box">
                        <button type="button" class="add-filters">Применить</button>
                        <button type="button" class="remove-filters">Сбросить</button>
                    </div>
                    <section class="m-t-10px">
                        <p class="font-14px">Год выпуска</p>
                        <input type="text" id="relYear"
                               value="{{$year}}">
                    </section>
                    <section class="m-t-10px">
                        <ul class="simple-ul">
                            <li class="checkbox m-t-10px" data-hide="0">
                                <span class="input"></span>
                                <span class="text">скрыть мангу в закладках</span>
                            </li>
                            <li class="checkbox m-t-10px" data-hide="1">
                                <span class="input"></span>
                                <span class="text">показывать мангу только из закладок</span>
                            </li>
                        </ul>
                    </section>
                    <section class="m-t-10px">
                        <p class="font-14px">Категория</p>
                        <ul class="simple-ul">
                            @foreach($categories as $category)
                                <li class="checkbox m-t-10px" data-cat="{{$category->id}}">
                                    <span class="input"></span>
                                    <span class="text">{{$category->name}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </section>
                    <section class="m-t-10px">
                        <p class="font-14px">Статус перевода</p>
                        <ul class="simple-ul">
                            <li class="checkbox m-t-10px" data-statTr="0">
                                <span class="input"></span>
                                <span class="text">продолжается</span>
                            </li>
                            <li class="checkbox m-t-10px" data-statTr="1">
                                <span class="input"></span>
                                <span class="text">завершен</span>
                            </li>
                            <li class="checkbox m-t-10px" data-statTr="2">
                                <span class="input"></span>
                                <span class="text">заморожен</span>
                            </li>
                        </ul>
                    </section>
                    <section class="m-t-10px">
                        <p class="font-14px">Формат выпуска</p>
                        <ul class="simple-ul">
                            <li class="checkbox m-t-10px" data-form="0">
                                <span class="input"></span>
                                <span class="text">4-кома (Ёнкома)</span>
                            </li>
                            <li class="checkbox m-t-10px" data-form="1">
                                <span class="input"></span>
                                <span class="text">сборник</span>
                            </li>
                            <li class="checkbox m-t-10px" data-form="2">
                                <span class="input"></span>
                                <span class="text">додзинси</span>
                            </li>
                            <li class="checkbox m-t-10px" data-form="3">
                                <span class="input"></span>
                                <span class="text">В цвете</span>
                            </li>
                            <li class="checkbox m-t-10px" data-form="4">
                                <span class="input"></span>
                                <span class="text">сингл</span>
                            </li>
                            <li class="checkbox m-t-10px" data-form="5">
                                <span class="input"></span>
                                <span class="text">веб</span>
                            </li>
                        </ul>
                    </section>
                    <section class="m-t-10px">
                        <p class="font-14px">Жанры</p>
                        <ul class="simple-ul">
                            @foreach($genres as $genre)
                                <li class="checkbox m-t-10px" data-genres="{{$genre ->id}}">
                                    <span class="input"></span>
                                    <span class="text">{{$genre->name}}</span>
                                </li>
                            @endforeach
                        </ul>
                    </section>
                </div>
            </section>
        </div>
    </div>
    <div class="left-0 right-0 top-0 bottom-0 substrate fixed hide">
        <div class="absolute left-0 right-0 top-0 bottom-0 center">
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="sign-in">
                <input type="text" placeholder="Логин или e-mail" class="m-t-10px" data-auth-input="log">
                <br>
                <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-input="pass">
                <br>
                <div class="m-t-10px">
                    <span class="error font-13px m-t-10px hide" data-auth-check="log">Неверный логин</span>
                    <span class="error font-13px m-t-10px hide" data-auth-check="pass">Неверный пароль</span>
                </div>
                <div class="btn-box">
                    <button type="button" class="btn-stand font-14px m-t-10px send-form">Вход</button>
                    <div class="font-14px m-t-10px">
                        <div class="checkbox">
                            <span class="input"></span>
                            <span class="text">запомнить меня</span>
                        </div>
                    </div>
                </div>
                <div class="center">
                    <hr>
                </div>
                <div class="btn-box">
                    <button type="button" class="btn-help font-13px m-t-10px" data-auth="recovery">Забыли пароль?
                    </button>
                    <button type="button" class="btn-help font-13px m-t-10px" data-auth="reg">Регистрация</button>
                </div>
            </div>
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="reg">
                <div class="center">
                    <h2>Регистрация</h2>
                </div>
                <div class="m-t-20px">
                    <div>
                        <input type="text" placeholder="Логин" class="m-t-20px" data-auth-check="login"
                               data-auth-input="log">
                        <span class="error font-13px m-t-10px hide"
                              data-auth-check="login">Упс! Такой уже существует</span>
                    </div>
                    <div>
                        <input type="text" placeholder="E-mail" class="m-t-10px" data-auth-check="e-mail"
                               data-auth-input="e-mail">
                        <span class="error font-13px m-t-10px hide"
                              data-auth-check="e-mail">Упс! Такой уже существует</span>
                    </div>
                    <div>
                        <input type="text" placeholder="Пароль" class="m-t-10px" data-auth-check="pass"
                               data-auth-input="pass">
                        <span class="error font-13px m-t-10px hide" data-auth-check="pass">Пароль может содержать только латиницу, цифры и _</span>
                    </div>
                    <div>
                        <input type="text" placeholder="Повторите пароль" class="m-t-10px" data-auth-check="pass-check"
                               data-auth-input="pass-check">
                        <span class="error font-13px m-t-10px hide"
                              data-auth-check="pass-check">Пароли не совпадают</span>
                    </div>
                    <div class="btn-box m-t-20px">
                        <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Регистрация
                        </button>
                        <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="sign-in">У меня
                            есть аккаунт
                        </button>
                    </div>
                </div>
            </div>
            <div class="modal b-li b-r-10px auth hide" data-auth-modal="recovery">
                <div class="center">
                    <h2 class="font-18px">Восстановление пароля</h2>
                </div>
                <div class="m-t-20px rec-w">
                    <div>
                        <input type="text" placeholder="Логин или e-mail" class="m-t-20px" data-auth-input="log"
                               data-auth-check="login-rec">
                        <span class="error font-13px m-t-10px hide"
                              data-auth-check="login-rec">Пользователь не найден</span>
                    </div>
                    <div class="btn-box m-t-20px">
                        <button type="button" class="btn-stand font-14px m-t-10px send-form" disabled>Восстановить
                        </button>
                        <button type="button" class="btn-help font-13px m-t-10px m-l-30px" data-auth="reg">Регистрация
                        </button>
                    </div>
                </div>
                <div class="rec-c hide">
                    <p class="c-se font-14px m-t-20px text-center">Письмо с инструкциями отправлено на ваш электронный
                        адрес</p>
                </div>
            </div>
        </div>
    </div>
@stop

@push('css')

    <link rel="stylesheet" href="/assets/css/catalog.css">
@endpush

@push('scripts')

    <script src="/assets/js/catalog.js" charset="utf-8"></script>
@endpush
