<?php

namespace App\Http\Resources;

use App\Models\Part;
use Illuminate\Http\Resources\Json\JsonResource;

class PartManga extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $manga = $this->resource;
        return [
            'id' => $manga->id,
            'rusName' => $manga->rus_name,
            'origName' => $manga->origin_name,
            'cover' => $manga->cover,
            'tags' => [],
            'type' => $manga->type,
            'parts' => LastPart::collection($manga->lastparts)
        ];
    }
}
