@php
    if (isset($approved) and $approved == true) {
        $comments = $model->approvedComments;
    } else {
        $comments = $model->comments;
    }
@endphp


<div class="p-10px margin-10px shadow-dark b-li b-r-10px comment-box" id="manga-comments">
    <div class="func-comm">
        @auth
            @include('comments::_form')
        @elseif(Config::get('comments.guest_commenting') == true)
            @include('comments::_form', [
                'guest_commenting' => true
            ])
        @else
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Требуется авторизация</h5>
                    <p class="card-text">Вы должны войти в систему, чтобы оставить комментарий.</p>
                </div>
            </div>
        @endauth

        @php
            $comments = $comments->sortByDesc('created_at');

            if (isset($perPage)) {
                $page = request()->query('page', 1) - 1;

                $parentComments = $comments->where('child_id', '');

                $slicedParentComments = $parentComments->slice($page * $perPage, $perPage);

                $m = Config::get('comments.model'); // This has to be done like this, otherwise it will complain.
                $modelKeyName = (new $m)->getKeyName(); // This defaults to 'id' if not changed.

                $slicedParentCommentsIds = $slicedParentComments->pluck($modelKeyName)->toArray();

                // Remove parent Comments from comments.
                $comments = $comments->where('child_id', '!=', '');

                $grouped_comments = new \Illuminate\Pagination\LengthAwarePaginator(
                    $slicedParentComments->merge($comments)->groupBy('child_id'),
                    $parentComments->count(),
                    $perPage
                );

                $grouped_comments->withPath(request()->path());
            } else {
                $grouped_comments = $comments->groupBy('child_id');
            }
        @endphp
        <div class="m-t-10px comm-box" data-send-id="main">
        @foreach($grouped_comments as $comment_id => $comments)
            {{-- Process parent nodes --}}
            @if($comment_id == '')
                @foreach($comments as $comment)
                    @include('comments::_comment', [
                        'comment' => $comment,
                        'grouped_comments' => $grouped_comments
                    ])
                @endforeach
            @endif
        @endforeach
        </div>
    </div>
</div>
@isset ($perPage)
    {{ $grouped_comments->links() }}
@endisset


