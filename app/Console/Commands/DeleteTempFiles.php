<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;

class DeleteTempFiles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmp:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear temporary storage';
    protected $lifeTime;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Setup temporary storage life time
        $this->lifeTime = config('filesystem.disks.temp.lifetime', 600);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::disk('temp')->allFiles();

        // Get a Carbon instance for the current date and time.
        $now = Carbon::now();

        // Iterate through all files in directory
        foreach ($files as $file)
        {
            // Get a Carbon instance for the file modified date and time.
            $modTimestamp = Storage::disk('temp')->lastModified($file);
            $modeDate = Carbon::createFromTimestamp($modTimestamp);

            // Calculate date difference
            $length = $modeDate->diffInSeconds($now);

            // Check if the file is old enough to delete it
            if ($length > $this->lifeTime) {
                Storage::disk('temp')->delete($file);
            }
        }
    }
}
