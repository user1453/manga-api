export function customPrompt (title, fn = null) {
    let p = prompt(title)

    if (fn !== null) {
        fn(p)
    }
}
