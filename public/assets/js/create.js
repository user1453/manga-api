/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"create": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/create.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/create.sass":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/create.sass ***!
  \********************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n\n//# sourceURL=webpack:///./src/assets/scss/create.sass?./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src??ref--5-3!./node_modules/sass-loader/dist/cjs.js");

/***/ }),

/***/ "./src/assets/scss/create.sass":
/*!*************************************!*\
  !*** ./src/assets/scss/create.sass ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var api = __webpack_require__(/*! ../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ \"./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js\");\n            var content = __webpack_require__(/*! !../../../node_modules/mini-css-extract-plugin/dist/loader.js!../../../node_modules/css-loader/dist/cjs.js!../../../node_modules/postcss-loader/src??ref--5-3!../../../node_modules/sass-loader/dist/cjs.js!./create.sass */ \"./node_modules/mini-css-extract-plugin/dist/loader.js!./node_modules/css-loader/dist/cjs.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js!./src/assets/scss/create.sass\");\n\n            content = content.__esModule ? content.default : content;\n\n            if (typeof content === 'string') {\n              content = [[module.i, content, '']];\n            }\n\nvar options = {};\n\noptions.insert = \"head\";\noptions.singleton = false;\n\nvar update = api(content, options);\n\n\n\nmodule.exports = content.locals || {};\n\n//# sourceURL=webpack:///./src/assets/scss/create.sass?");

/***/ }),

/***/ "./src/create.js":
/*!***********************!*\
  !*** ./src/create.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _assets_scss_create_sass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./assets/scss/create.sass */ \"./src/assets/scss/create.sass\");\n/* harmony import */ var _assets_scss_create_sass__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_assets_scss_create_sass__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/menu.js */ \"./src/js/menu.js\");\n/* harmony import */ var _js_menu_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_menu_js__WEBPACK_IMPORTED_MODULE_1__);\n\n\n\nconst log = text => console.log(text);\n\nlet random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;\n\nconst dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => {\n  if (el != undefined) {\n    el.classList.add(el2);\n  }\n},\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet stepsParams = {\n  count: 1,\n  steps: dQS('[data-steps-count]').dataset.stepsCount\n};\nlet typeCreating = dQS('[data-creat-type]').dataset.creatType;\nlet deligates = [];\nstepsParams = new Proxy(stepsParams, {\n  set(target, prop, val) {\n    if (prop == 'count') {\n      if (val != target.steps + 1) {\n        target[prop] = val;\n        removeClass(dQS('#name'), 'error-input');\n\n        for (let it of dQSA(`[class*=\"step-\"]`)) {\n          addClass(it, 'hide');\n        }\n\n        removeClass(dQS(`.step-${target[prop]}`), 'hide');\n\n        for (let it of dQSA(`.progress span`)) {\n          removeClass(it, 'active');\n        }\n\n        if (target[prop] > 1) {\n          removeClass(dQS(`.prev-step`), 'hide');\n        } else {\n          addClass(dQS(`.prev-step`), 'hide');\n        }\n\n        addClass(dQSA(`.progress span`)[target[prop] - 1], 'complete');\n        addClass(dQSA(`.progress span`)[target[prop] - 1], 'active');\n      }\n\n      if (val == target.steps) {\n        if (typeCreating == 'manga') {\n          let fData = new FormData();\n          fData.append('cover', dQS('[data-id=\"0\"]').files[0]);\n          fData.append('background', dQS('[data-id=\"1\"]').files[0]);\n          let genres = '';\n\n          for (let item of dQSA('#genres .checkbox.active .text')) {\n            genres += item.innerText + ',';\n          }\n\n          let form = '';\n\n          for (let item of dQSA('#form .checkbox.active .text')) {\n            form += item.innerText + ',';\n          }\n\n          fData.append('origName', dQS('#name').value);\n          fData.append('rusName', dQS('#rusName').value);\n          fData.append('engName', dQS('#engName').value);\n          fData.append('altName', dQS('#altName').value);\n          fData.append('description', dQS('#description').value);\n          fData.append('type', dQS('#type').value);\n          fData.append('relDate', dQS('#relDate').value);\n          fData.append('author', dQS('#author').value);\n          fData.append('painter', dQS('#painter').value);\n          fData.append('relHead', dQS('#relHead').value);\n          fData.append('genres', genres);\n          fData.append('form', form);\n          fData.append('statTranslate', dQS('#statTranslate').value);\n          fData.append('yo', dQS('#yo').value);\n          let response = fetch('/manga/create', {\n            method: 'POST',\n            mode: \"cors\",\n            headers: {\n              'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content')\n            },\n            body: fData\n          });\n          response.then(req => req.json()).then(req => {\n            if (req.status == 1) {\n              addClass(dQS('.loader-send'), 'hide');\n              dQS('.status-send').innerText = 'Заявка на модерации';\n            }\n          });\n        } else if (typeCreating == 'team') {\n          let obj = {\n            name: dQS('#name').value,\n            dateStart: dQS('#dateStart').value,\n            headLink: dQS('#headLink').value,\n            deligates: deligates,\n            discordLink: dQS('#discordLink').value,\n            vkLink: dQS('#vkLink').value,\n            description: dQS('#description').value\n          };\n          let response = fetch('/team/create', {\n            method: 'POST',\n            mode: \"cors\",\n            headers: {\n              'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n              'Content-Type': 'application/json'\n            },\n            body: JSON.stringify(obj)\n          });\n          response.then(req => req.json()).then(req => {\n            if (req.status == 1) {\n              addClass(dQS('.loader-send'), 'hide');\n              dQS('.status-send').innerText = 'Заявка на модерации';\n            }\n          });\n        }\n\n        for (let it of dQSA('.next-step, .prev-step')) {\n          addClass(it, 'hide');\n        }\n      }\n\n      return true;\n    }\n  }\n\n});\n\ndQS('.next-step').onclick = e => {\n  stepsParams.count++;\n};\n\ndQS('.prev-step').onclick = e => {\n  stepsParams.count--;\n};\n\ndQS('#name').onchange = e => {\n  let target = e.target,\n      val = target.value;\n\n  if (typeCreating == 'manga') {\n    let response = fetch('/manga/check-name-origin', {\n      method: 'POST',\n      mode: \"cors\",\n      headers: {\n        'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n        'Content-Type': 'application/json'\n      },\n      body: JSON.stringify({\n        name: dQS('#name').value\n      })\n    });\n    response.then(req => req.json()).then(req => {\n      log(req);\n\n      if (req.status == 'empty') {\n        addClass(dQS('#nameError'), 'hide');\n      } else {\n        removeClass(dQS('#nameError'), 'hide');\n      }\n    });\n  } else if (typeCreating == 'team') {\n    let response = fetch('/team/check-name', {\n      method: 'POST',\n      mode: \"cors\",\n      headers: {\n        'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n        'Content-Type': 'application/json'\n      },\n      body: JSON.stringify({\n        name: dQS('#name').value\n      })\n    });\n    response.then(req => req.json()).then(req => {\n      log(req);\n\n      if (req.status == 'empty') {\n        addClass(dQS('#nameError'), 'hide');\n      } else {\n        removeClass(dQS('#nameError'), 'hide');\n      }\n    });\n  }\n};\n\nif (dQS('#rusName') != null) {\n  dQS('#rusName').onchange = e => {\n    let target = e.target,\n        val = target.value;\n\n    if (typeCreating == 'manga') {\n      let response = fetch('/manga/check-name-rus', {\n        method: 'POST',\n        mode: \"cors\",\n        headers: {\n          'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n          'Content-Type': 'application/json'\n        },\n        body: JSON.stringify({\n          name: dQS('#rusName').value\n        })\n      });\n      response.then(req => req.json()).then(req => {\n        if (req.status == 'empty') {\n          addClass(dQS('#nameRusError'), 'hide');\n        } else {\n          removeClass(dQS('#nameRusError'), 'hide');\n        }\n      });\n    }\n  };\n}\n\nif (dQS('#engName') != null) {\n  dQS('#engName').onchange = e => {\n    let target = e.target,\n        val = target.value;\n\n    if (typeCreating == 'manga') {\n      let response = fetch('/manga/check-name-en', {\n        method: 'POST',\n        mode: \"cors\",\n        headers: {\n          'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n          'Content-Type': 'application/json'\n        },\n        body: JSON.stringify({\n          name: dQS('#engName').value\n        })\n      });\n      response.then(req => req.json()).then(req => {\n        if (req.status == 'empty') {\n          addClass(dQS('#nameEngError'), 'hide');\n        } else {\n          removeClass(dQS('#nameEngError'), 'hide');\n        }\n      });\n    }\n  };\n}\n\nif (dQS('.addDeligate') !== null) {\n  dQS('.addDeligate').onclick = e => {\n    let target = e.target;\n    let html = ``;\n\n    if (dQS('.deligator-1').value.trim().length != 0 && dQS('.deligator-2').value.trim().length != 0) {\n      let response = fetch('/api/v1/get-user-name', {\n        method: 'POST',\n        mode: \"cors\",\n        headers: {\n          'X-CSRF-TOKEN': dQS('[name=\"csrf-token\"]').getAttribute('content'),\n          'Content-Type': 'application/json'\n        },\n        body: JSON.stringify({\n          login: dQS('.deligator-2').value\n        })\n      });\n      response.then(req => req.json()).then(req => {\n        dQS('.delegates').innerHTML += `<span data-log-rem=\"${req}\">\n                    ${req}\n                    <svg>\n                        <use xlink:href=\"/assets/img/sprite.svg#remove\"></use>\n                    </svg>\n                </span>`;\n        deligates.push({\n          log: req,\n          deligate: dQS('.deligator-1').value\n        });\n\n        for (let it of dQSA('.delegates span')) {\n          it.onclick = e => {\n            let target = e.target;\n            let log = it.dataset.logRem;\n            deligates.find((item, i, arr) => {\n              if (item.log == log) {\n                arr.splice(i, 1);\n              }\n            });\n            it.remove();\n            console.log(deligates);\n          };\n        }\n\n        dQS('.deligator-1').value = '';\n        dQS('.deligator-2').value = '';\n      });\n    }\n  };\n}\n\nif (dQSA('.file-load') !== null) {\n  for (let it of dQSA('.file-load input')) {\n    it.onchange = e => {\n      let reader = new FileReader();\n\n      reader.onload = function (e) {\n        for (let it2 of dQSA(`#l-img-${it.dataset.id}`)) {\n          it2.setAttribute('src', e.target.result);\n        }\n      };\n\n      reader.readAsDataURL(it.files[0]);\n    };\n  }\n}\n\nfunction checkbox(el) {\n  for (let it of dQSA(`${el}`)) {\n    it.onclick = e => {\n      if (hasClass(it, 'active')) {\n        removeClass(it, 'active');\n        it.removeAttribute('data-selected');\n      } else {\n        addClass(it, 'active');\n        it.setAttribute('data-selected', 'true');\n      }\n    };\n  }\n}\n\ncheckbox('.checkbox');\n\n//# sourceURL=webpack:///./src/create.js?");

/***/ }),

/***/ "./src/js/menu.js":
/*!************************!*\
  !*** ./src/js/menu.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("const dGetClass = el => document.getElementsByClassName(el),\n      dGetTag = el => document.getElementsByTagName(el),\n      dGetId = el => document.getElementById(el),\n      dQS = el => document.querySelector(el),\n      dQSA = el => document.querySelectorAll(el),\n      removeClass = (el, el2) => el.classList.remove(el2),\n      addClass = (el, el2) => el.classList.add(el2),\n      replaceClass = (el, clr, cla) => {\n  el.classList.remove(clr);\n  el.classList.add(cla);\n},\n      hasClass = (el, el2) => el.classList.contains(el2);\n\nlet storage = {};\n\nfor (let it of dQSA('.menu__nav__search-box__panel__params span')) {\n  it.onclick = e => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel__params span')) {\n      removeClass(it2, 'active');\n    }\n\n    storage.searchTypeSort = it.dataset.typeSort;\n    addClass(it, 'active');\n  };\n}\n\nfor (let it of dQSA('.menu__nav__search-box__search-input')) {\n  it.onfocus = () => {\n    for (let it2 of dQSA('.menu__nav__search-box__panel')) {\n      addClass(it2, 'show');\n    }\n  };\n\n  it.oninput = () => {\n    if (it.value.length >= 3) {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        addClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        addClass(it2, 'show');\n      }\n    } else {\n      for (let it2 of dQSA('.menu__nav__search-box__search-process')) {\n        removeClass(it2, 'search-process-anim');\n      }\n\n      for (let it2 of dQSA('.menu__nav__search-box__panel__result')) {\n        removeClass(it2, 'show');\n      }\n    }\n  };\n}\n\ndocument.onmousedown = e => {\n  let target = e.target;\n\n  if (!target.closest('.menu__nav__search-box__panel') && !target.closest('.menu__nav__search-box__search-input')) {\n    for (let it of dQSA('.menu__nav__search-box__panel')) {\n      removeClass(it, 'show');\n    }\n  }\n};\n\n//# sourceURL=webpack:///./src/js/menu.js?");

/***/ })

/******/ });