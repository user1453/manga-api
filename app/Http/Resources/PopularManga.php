<?php

namespace App\Http\Resources;

use App\Models\Part;
use Illuminate\Http\Resources\Json\JsonResource;

class PopularManga extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $manga = $this->resource;
        $part = Part::where('manga_id', '=', $manga->id)->with('user')->orderBy('created_at', 'desc')->first();

        return [
            'id' => $manga->id,
            'rusName' => $manga->rus_name,
            'origName' => $manga->origin_name,
            'cover' => $manga->cover,
            'vol' => $part->vol,
            'part' => $part->number,
            'views' => $manga->views,
            'translater' => $part->user->login
        ];
    }
}
