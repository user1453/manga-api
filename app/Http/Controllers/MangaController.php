<?php

namespace App\Http\Controllers;

use App\Http\Resources\MangaPartCollection;
use App\Http\Resources\NewMangaCollection;
use App\Http\Resources\PopularMangaCollection;
use App\Http\Resources\ShowManga;
use App\Http\Resources\StatusResponse;
use App\Jobs\ProcessResize;
use App\Models\Bookmark;
use App\Models\Delegate;
use App\Models\Manga;
use App\Models\Page;
use App\Models\Part;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class MangaController extends Controller
{
    public function getIndex(Request $request, $id)
    {
        $translatorParts = Part::with('user')
            ->where('manga_id', $id)
            ->distinct('user_id')
            ->get();
        $translators = [];
        foreach ($translatorParts as $part) {
            $translators[] = $part->user->login;
        }
        $parts = Part::certified()
            ->orderBy('number', 'desc')
            ->where('manga_id', $id)
            ->get();

        $listName = '';
        if (Auth::check()) {
            $bookmark = Bookmark::where('manga_id', $id)->where('user_id', Auth::user()->id)->first();
            if ($bookmark != null)
                switch ($bookmark->list) {
                    case 'like':
                        $listName = 'Любимое';
                        break;
                    case 'fors':
                        $listName = 'Брошено';
                        break;
                    case 'i-read':
                        $listName = 'Читаю';
                        break;
                    case 'read':
                        $listName = 'Прочитано';
                        break;
                    case 'plans':
                        $listName = 'В планах';
                        break;
                    default:
                        break;
                }
        }

        $bookmarksCount = Cache::rememberForever('bookmarks:count#' . $id, function () use ($id) {
            return Bookmark::where('manga_id', $id)->count();
        });

        $manga = Manga::with(['categories', 'genres'])->find($id);
        if ($manga == null)
            abort(404);
        $firstPart = Part::firstPart($id);
        $genres = $manga->relationsToArray()['genres'];
        $genreIds = [];
        if ($genres != null)
            foreach ($genres as $genre) {
                $genreIds[] = $genre['id'];
            }

        $recommends = Manga::whereHas('genres', function (Builder $query) use ($genreIds, $manga) {
            $query->whereIn('genres.id', $genreIds);
        })->where('id', '!=', $manga->id)
            ->take(2)->get();

        if (!$request->session()->has('manga_' . $id)) {
            session(['manga_' . $id => true]);
            $manga->increment('views');
        }

        return view('manga.index', [
            'manga' => $manga,
            'translators' => implode(', ', $translators),
            'parts' => $parts,
            'list' => $listName,
            'bookmarksCount' => $bookmarksCount,
            'firstPart' => $firstPart,
            'recommends' => $recommends,
        ]);
    }

    public function getCreate()
    {
        if (!Auth::user()->hasDelegate())
            abort(404);
        return view('manga.create');
    }

    public function upload(Request $request)
    {
        if (!Auth::user()->hasDelegate())
            abort(404);

        $mangaId = $request->get('id');

        foreach ($request->get('part') as $input) {

            $translator = User::where('id', $input['translater'])->first();
            if ($translator == null)
                return new StatusResponse('error');

            $part = new Part;
            $part->manga_id = $mangaId;
            $part->number = $input['part'];
            $part->name = $input['name'];
            $part->vol = $input['vol'];
            $part->status = 'extracting';
            $part->user_id = Auth::user()->id;
            $part->certified = true; // TODO: удалить
            $extension = $request->file('file_' . $input['part'])->getClientOriginalExtension();

            if ($extension != 'zip' & $extension != 'rar')
                return response(new StatusResponse('no valid file'), 200);


            $archive = $request->file('file_' . $input['part']);
            $fileName = time() . "file_{$input['part']}." . $archive->getClientOriginalExtension();
            $path = $request->file('file_' . $input['part'])->storeAs(
                'archives',
                $fileName,
                'temp'
            );

            $part->save();

            ProcessResize::dispatchNow(['path' => $path, 'part' => $part, 'mangaId' => $mangaId, 'translator' => $translator->login]);
        }

        return response(new StatusResponse(1), 200);
    }


    public function create(Request $request)
    {
        if (!Auth::user()->hasDelegate())
            abort(404);

        $input = [
            'rus_name' => strip_tags($request->get('rusName')),
            'en_name' => strip_tags($request->get('engName')),
            'origin_name' => strip_tags($request->get('origName')),
            'alt_name' => strip_tags($request->get('altName')),
            'description' => strip_tags($request->get('description')),
            'type' => strip_tags($request->get('type')),
            'date_release' => $request->get('relDate'),
            'author' => strip_tags($request->get('author')),
            'painter' => strip_tags($request->get('painter')),
            'publisher' => strip_tags($request->get('relHead')),
            'genres' => strip_tags($request->get('genres')),
            'format' => strip_tags($request->get('form')),
            'translate_status' => strip_tags($request->get('statTranslate')),
            'age_limit' => strip_tags($request->get('yo')),
            'user_id' => Auth::user()->id,
        ];

        $validator = Validator::make($input, [
            'rus_name' => 'required|string|max:255|unique:mangas',
            'en_name' => 'required|string|max:255|unique:mangas',
            'origin_name' => 'required|string|max:255|unique:mangas',
            'alt_name' => 'required|string|max:255|unique:mangas',
            'date_release' => 'required',
            'author' => 'required|string|max:255',
            'type' => 'required|string|max:255',
            'painter' => 'required|string|max:255',
            'publisher' => 'required|string|max:255',
            'format' => 'required|string|max:255',
            'translate_status' => 'required|string|max:255',
            'age_limit' => 'required|string|max:255',
            'description' => 'string',
            'cover' => 'image',
            'background' => 'image',
        ]);

        if ($validator->fails()) {
            return response(new StatusResponse($validator->errors()), 200);
        }

        $files = [
            'cover' => $request->file('cover'),
            'background' => $request->file('background'),
        ];
        $fileValidator = Validator::make($files, [
            'cover' => 'image',
            'background' => 'image',
        ]);

        if ($fileValidator->fails()) {
            return response(new StatusResponse($fileValidator->errors()), 200);
        }

        $manga = new Manga;
        $manga->fill($input);
        $manga->save();

        Storage::putFileAs("/public/mangas/{$manga->id}/originals", $files['background'], 'background.' . $files['background']->extension());

        $path = storage_path("app/public/mangas/{$manga->id}/originals/background." . $files['background']->extension());
        $pathr = "public/mangas/{$manga->id}/resized/background.jpg";
        $this->resize($path, 1820, $pathr);


        Storage::putFileAs("public/mangas/{$manga->id}/originals", $files['cover'], 'cover.' . $files['cover']->extension());

        $path = storage_path("app/public/mangas/{$manga->id}/originals/cover." . $files['cover']->extension());
        $pathr = "public/mangas/{$manga->id}/resized/cover.jpg";


        $this->resize($path, 300, $pathr);


        $manga->background = \url(Storage::url("public/mangas/{$manga->id}/resized/" . 'background.jpg'));
        $manga->cover = \url(Storage::url("public/mangas/{$manga->id}/resized/" . 'cover.jpg'));
        $manga->save();

        Cache::forget('manga.lastupdates');
        return response(new StatusResponse(1), 200);
    }

    public function update(Request $request){
        $user = Auth::user();

//        if($user)


    }


    private function resize($name, $height, $storagePath)
    {
        $resize = Image::make($name)
            ->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('jpg', 75);
        $resize->extension = 'jpg';
        Storage::put($storagePath, $resize->__toString());
    }

    public function newManga()
    {
        $value = Cache::remember('manga.lastupdates', 600, function () {
            return New NewMangaCollection(Manga::where('certified', true)
                ->orderby('created_at', 'desc')
                ->take(6)
                ->get());
        });
        return response($value, 200);
    }

    public function popularUpdates()
    {
        $value = Cache::remember('manga.popularupdates', 600, function () {
            return new PopularMangaCollection(Manga::where('views', '>', 0)
                ->where('certified', true)
                ->whereHas('parts')
                ->orderby('views')
                ->take(6)
                ->get());
        });
        return response($value, 200);
    }

    public function popular()
    {
        $value = Cache::remember('manga.popular', 600, function () {
            return new PopularMangaCollection(Manga::where('views', '>', 0)
                ->where('certified', true)
                ->whereHas('parts')
                ->orderby('views')
                ->take(6)
                ->get());
        });
        return response($value, 200);
    }

    public function views($mangaId)
    {
        $manga = Manga::where('id', $mangaId)->first();
        $manga->views = $manga->views + 1;
        $manga->save();
        return response(new StatusResponse(1), 200);
    }

    public function rate(Request $request)
    {
        $manga = Manga::where('rus_name', $request->get('name'))->first();
        $rate = $request->get('rating');
        if ($rate > 5)
            $rate = 5;
        if ($rate < 1)
            $rate = 1;

        $manga->rateOnce($rate);
        return response(new StatusResponse(1), 200);
    }

    public function show(Request $request, $id)
    {
        return new ShowManga(Manga::find($id));
    }

    public function parts(Request $request, $id)
    {
        return response(new MangaPartCollection(Part::where('manga_id', $id)
            ->with('user')
            ->orderby('number', 'desc')
            ->orderby('created_at')
            ->get()), 200);
    }

    public function addParts(Request $request, $id)
    {
        if (!Auth::user()->hasDelegate())
            abort(403);

        $manga = Manga::find($id);
        $teamId = Delegate::where('user_id', Auth::user()->id)->first()->team_id;
        $lastPart = Part::where('manga_id', $manga->id)->orderBy('number', 'desc')->first();

        $delegates = Delegate::where('team_id', $teamId)->with('user')->get();
        return view('manga.addparts', ['manga' => $manga, 'delegates' => $delegates, 'lastPart' => $lastPart]);
    }

    public function checkName(Request $request, $type)
    {
        $input = [
            $type . '_name' => $request->get('name')
        ];
        $validator = Validator::make($input, [
            $type . '_name' => 'required|string|max:255|unique:mangas',
        ]);
        if ($validator->fails()) {
            return response(new StatusResponse(0), 200);
        } else
            return response(new StatusResponse('empty'), 200);
    }

    public function clear()
    {
        Page::where('id', '>', 0)->delete();
        Part::where('id', '>', 0)->delete();
    }

    public function rand()
    {
        return redirect(route('manga.page', ['id' => Manga::certified()->inRandomOrder()->first()->id]));
    }
}
