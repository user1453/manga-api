import './assets/scss/create.sass'
import './js/menu.js'

const log = (text) => console.log(text)
let random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const
dGetClass = (el) => document.getElementsByClassName(el),
dGetTag = (el) => document.getElementsByTagName(el),
dGetId = (el) => document.getElementById(el),
dQS = (el) => document.querySelector(el),
dQSA = (el) => document.querySelectorAll(el),
removeClass = (el, el2) => el.classList.remove(el2),
addClass = (el, el2) => {
    if (el != undefined) {
        el.classList.add(el2)
    }
},
replaceClass = (el, clr, cla) => {
    el.classList.remove(clr)
    el.classList.add(cla)
},
hasClass = (el, el2) => el.classList.contains(el2)

let stepsParams = {
    count: 1,
    steps: dQS('[data-steps-count]').dataset.stepsCount
}

let typeCreating = dQS('[data-creat-type]').dataset.creatType
let deligates = []

stepsParams = new Proxy(stepsParams, {
    set(target, prop, val) {
        if (prop == 'count') {
            if (val != target.steps + 1) {
                target[prop] = val
                removeClass(dQS('#name'), 'error-input')
                for (let it of dQSA(`[class*="step-"]`)) {
                    addClass(it, 'hide')
                }
                removeClass(dQS(`.step-${target[prop]}`), 'hide')
                for (let it of dQSA(`.progress span`)) {
                    removeClass(it, 'active')
                }
                if (target[prop] > 1) {
                    removeClass(dQS(`.prev-step`), 'hide')
                } else {
                    addClass(dQS(`.prev-step`), 'hide')
                }
                addClass(dQSA(`.progress span`)[target[prop]-1], 'complete')
                addClass(dQSA(`.progress span`)[target[prop]-1], 'active')
            }
            if (val == target.steps) {
                if (typeCreating == 'manga') {

                    let fData = new FormData

                    fData.append('cover', dQS('[data-id="0"]').files[0])
                    fData.append('background', dQS('[data-id="1"]').files[0])

                    let genres = ''
                    for (let item of dQSA('#genres .checkbox.active .text')) {
                        genres += item.innerText + ','
                    }

                    let form = ''
                    for (let item of dQSA('#form .checkbox.active .text')) {
                        form += item.innerText + ','
                    }

                    fData.append('origName', dQS('#name').value)
                    fData.append('rusName', dQS('#rusName').value)
                    fData.append('engName', dQS('#engName').value)
                    fData.append('altName', dQS('#altName').value)
                    fData.append('description', dQS('#description').value)
                    fData.append('type', dQS('#type').value)
                    fData.append('relDate', dQS('#relDate').value)
                    fData.append('author', dQS('#author').value)
                    fData.append('painter', dQS('#painter').value)
                    fData.append('relHead', dQS('#relHead').value)
                    fData.append('genres', genres)
                    fData.append('form', form)
                    fData.append('statTranslate', dQS('#statTranslate').value)
                    fData.append('yo', dQS('#yo').value)

                    let response = fetch('/manga/create', {
                        method: 'POST',
                        mode: "cors",
                        headers: {
                            'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content')
                        },
                        body: fData,
                    })

                    response
                    .then(req => req.json())
                    .then(req => {
                        if (req.status == 1) {
                            addClass(dQS('.loader-send'), 'hide')
                            dQS('.status-send').innerText = 'Заявка на модерации'
                        }
                    })
                } else if (typeCreating == 'team') {

                    let obj = {
                        name: dQS('#name').value,
                        dateStart: dQS('#dateStart').value,
                        headLink: dQS('#headLink').value,
                        deligates: deligates,
                        discordLink: dQS('#discordLink').value,
                        vkLink: dQS('#vkLink').value,
                        description: dQS('#description').value,
                    }

                    let response = fetch('/team/create', {
                        method: 'POST',
                        mode: "cors",
                        headers: {
                            'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify(obj),
                    })

                    response
                    .then(req => req.json())
                    .then(req => {
                        if (req.status == 1) {
                            addClass(dQS('.loader-send'), 'hide')
                            dQS('.status-send').innerText = 'Заявка на модерации'
                        }
                    })
                }
                for (let it of dQSA('.next-step, .prev-step')) {
                    addClass(it, 'hide')
                }
            }
            return true
        }
    }
})

dQS('.next-step').onclick = (e) => {
    stepsParams.count++
}

dQS('.prev-step').onclick = (e) => {
    stepsParams.count--
}

dQS('#name').onchange = (e) => {
    let target = e.target,
        val = target.value

    if (typeCreating == 'manga') {
        let response = fetch('/manga/check-name-origin', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({name: dQS('#name').value}),
        })

        response
        .then(req => req.json())
        .then(req => {
            log(req)
            if (req.status == 'empty') {
                addClass(dQS('#nameError'), 'hide')
            } else {
                removeClass(dQS('#nameError'), 'hide')
            }
        })
    } else if (typeCreating == 'team') {
        let response = fetch('/team/check-name', {
            method: 'POST',
            mode: "cors",
            headers: {
                'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({name: dQS('#name').value}),
        })

        response
        .then(req => req.json())
        .then(req => {
            log(req)
            if (req.status == 'empty') {
                addClass(dQS('#nameError'), 'hide')
            } else {
                removeClass(dQS('#nameError'), 'hide')
            }
        })
    }
}

if (dQS('#rusName') != null) {
    dQS('#rusName').onchange = (e) => {
        let target = e.target,
            val = target.value

        if (typeCreating == 'manga') {
            let response = fetch('/manga/check-name-rus', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({name: dQS('#rusName').value}),
            })

            response
            .then(req => req.json())
            .then(req => {
                if (req.status == 'empty') {
                    addClass(dQS('#nameRusError'), 'hide')
                } else {
                    removeClass(dQS('#nameRusError'), 'hide')
                }
            })
        }
    }
}

if (dQS('#engName') != null) {
    dQS('#engName').onchange = (e) => {
        let target = e.target,
            val = target.value

        if (typeCreating == 'manga') {
            let response = fetch('/manga/check-name-en', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({name: dQS('#engName').value}),
            })

            response
            .then(req => req.json())
            .then(req => {
                if (req.status == 'empty') {
                    addClass(dQS('#nameEngError'), 'hide')
                } else {
                    removeClass(dQS('#nameEngError'), 'hide')
                }
            })
        }
    }
}

if (dQS('.addDeligate') !== null) {
    dQS('.addDeligate').onclick = (e) => {
        let target = e.target
        let html = ``

        if (dQS('.deligator-1').value.trim().length != 0 && dQS('.deligator-2').value.trim().length != 0) {
            let response = fetch('/api/v1/get-user-name', {
                method: 'POST',
                mode: "cors",
                headers: {
                    'X-CSRF-TOKEN': dQS('[name="csrf-token"]').getAttribute('content'),
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({login: dQS('.deligator-2').value}),
            })

            response
            .then(req => req.json())
            .then(req => {
                dQS('.delegates').innerHTML += `<span data-log-rem="${req}">
                    ${req}
                    <svg>
                        <use xlink:href="/assets/img/sprite.svg#remove"></use>
                    </svg>
                </span>`
                deligates.push({log: req, deligate: dQS('.deligator-1').value})
                for (let it of dQSA('.delegates span')) {
                    it.onclick = (e) => {
                        let target = e.target
                        let log = it.dataset.logRem
                        deligates.find((item, i, arr) => {
                            if (item.log == log) {
                                arr.splice(i, 1)
                            }
                        })
                        it.remove()
                        console.log(deligates);
                    }
                }
                dQS('.deligator-1').value = ''
                dQS('.deligator-2').value = ''
            })
        }
    }
}


if (dQSA('.file-load') !== null) {
    for (let it of dQSA('.file-load input')) {
        it.onchange = (e) => {
            let reader = new FileReader()
            reader.onload = function (e) {
                for (let it2 of dQSA(`#l-img-${it.dataset.id}`)) {
                    it2.setAttribute('src', e.target.result)
                }
            }
            reader.readAsDataURL(it.files[0])
        }
    }
}

function checkbox (el) {
    for (let it of dQSA(`${el}`)) {
        it.onclick = (e) => {
            if (hasClass(it, 'active')) {
                removeClass(it, 'active')
                it.removeAttribute('data-selected')
            } else {
                addClass(it, 'active')
                it.setAttribute('data-selected', 'true')
            }
        }
    }
}

checkbox('.checkbox')
