const merge = require('webpack-merge');
const webpack = require('webpack');
const baseWebpackConfig = require('./webpack.base.conf');
const buildWebpackConfig = merge(baseWebpackConfig, {
    // mode: 'production',
    mode: 'development',
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[file.map]'
        }),
    ]
});
module.exports = new Promise((resolve, reject) => {
    resolve(buildWebpackConfig)
});
