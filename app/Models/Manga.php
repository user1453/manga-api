<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Laravelista\Comments\Commentable;
use willvincent\Rateable\Rateable;

/**
 * @property integer id
 */
class Manga extends Model
{
    protected $guarded = [];
    use Commentable;
    use Rateable;

    public function lastUpdatePart()
    {
        Part::first();
    }

    public function parts()
    {
        return $this->hasMany(Part::class);
    }

    public function lastparts()
    {
        return $this->hasMany(Part::class)->orderBy('created_at', 'desc')->take(6)->with('user');
    }

    public static function translators($id)
    {
        $value = Cache::remember('manga.translator#' . $id, 600, function () use ($id) {
            $translators = Part::certified()
                ->where('manga_id', $id)
                ->leftJoin('users', 'users.id', '=', 'parts.user_id')->groupBy('users.login')->select('users.login')->get();
            $translator = '';
            if ($translators->count() > 0) {
                $translator = $translators->first()->login;
                if ($translators->count() > 1) {
                    $count = $translators->count() - 1;
                    $translator .= " (+{$count})";
                }
            }
            return $translator;
        });

        return $value;
    }

    public function publish()
    {
        Manga::where('id', $this->id)->update(['certified' => true]);

        CategoryManga::where('manga_id', $this->id)->delete();
        $cat = Category::where('name', $this->type)->first();
        if ($cat != null)
            CategoryManga::create(['manga_id' => $this->id, 'category_id' => $cat->id]);

        $genres = explode(',', $this->genres);
        $genres = array_diff($genres, array(''));

        GenreManga::where('manga_id', $this->id)->delete();

        foreach ($genres as $genreName) {
            $genre = Genre::where('name', $genreName)->first();
            if ($genre == null)
                continue;

            GenreManga::create(['manga_id' => $this->id, 'genre_id' => $genre->id]);
        }
    }

    /**
     * @return Builder
     */
    public static function certified()
    {
        return self::where('mangas.certified', true);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function categoryManga()
    {
        return $this->hasMany(CategoryManga::class);
    }

    public function boockmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function getGesnreAttribute()
    {
        return implode(', ' , $this->genres()->pluck('name'));
    }
}
